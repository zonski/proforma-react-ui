/**
 * Checks if the browser has enabled a supported version of the Do Not Track (dnt) setting
 * @returns {boolean} - Returns true if the browser does not use a supported dnt setting or if Do Not Track is disabled
 */
export const checkCanIgnoreDoNotTrack = () => {
  const hasNavigatorDNT = 'doNotTrack' in window.navigator;
  const hasWindowDNT = 'doNotTrack' in window;

  const dnt = (
    (hasNavigatorDNT && window.navigator.doNotTrack)
    || (hasWindowDNT && window.doNotTrack)
    || '0'
  );

  const dntIsEnabled = (
    dnt === '1' || dnt === 'yes'
  );

  return !dntIsEnabled;
};
