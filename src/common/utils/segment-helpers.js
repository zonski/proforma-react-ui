// Utils
import { sendDebugMsg } from './errorHandlingUtils';

export const commonFieldsOverwrite = {
  path: global.location.pathname,
  referrer: '',
  search: '',
  title: '',
  url: global.location.host,
};

export const optionsOverwrite = {
  context: {
    ip: '0.0.0.0',
  },
  page: commonFieldsOverwrite,
};


/**
 * Used to track events triggered by a user
 * @param {string} eventName - Name of the event being tracked
 * @param {object} payload - Flat object structure containing all the data associated with the event being tracked
 */
export const sendTrackEvent = (eventName, payload) => {
  const hasTrackMethod = !!global.analytics && !!global.analytics.track;

  if (hasTrackMethod && eventName) {
    global.analytics.track(eventName, payload, optionsOverwrite);
  }
};

/**
 * Used to associate track events with a specific registered user
 * @param {string} userIdHash - Hashed representation of either the User Key (server) or Account Id (clound)
 */
export const sendIdentifyEvent = (userIdHash) => {
  const hasIdentifyMethod = !!global.analytics && !!global.analytics.identify;

  if (hasIdentifyMethod && userIdHash) {
    global.analytics.identify(userIdHash, {}, optionsOverwrite);
  }
};

/**
 * Used for sending a group event that associates individual users by the shared SEN number
 * @param {string} sen - Represents the group name to associate with events from the identified user
 */
export const sendGroupEvent = (sen) => {
  const hasGroupMethod = !!global.analytics && !!global.analytics.group;

  if (hasGroupMethod && sen) {
    global.analytics.group(sen, {}, optionsOverwrite);
  }
};

/**
 * Used for sending a page event when the app is first mounted
 * @param {string} pageCategory - App container name
 * @param {string} pageName - Route where the app is mounted
 */
export const sendPageEvent = (pageCategory, pageName) => {
  const hasPageMethod = !!global.analytics && !!global.analytics.page;

  if (hasPageMethod && pageCategory) {
    global.analytics.page(pageCategory, pageName, commonFieldsOverwrite, optionsOverwrite);
    return;
  }

  if (hasPageMethod) {
    global.analytics.page(pageName, commonFieldsOverwrite, optionsOverwrite);
  }
};

/**
 * Takes the original payload from a segment track event and sanitises the data before passing it to analytics.track
 * @param {string} eventName - Name of the event that will appear in segment
 * @param {object} payload - JSON object to be sent along with the tracking event
 */
export const cleanSegmentTrackPayload = (eventName, payload) => {
  const newPayload = {
    // Proforma properties
    ...payload.formName && { formName: payload.formName },
    ...payload.templateName && { templateName: payload.templateName },
    ...payload.projectKey && { projectKey: payload.projectKey },

    // Appcues properties
    ...payload.flowName && { flowName: payload.flowName },
  };

  global.proFormaBuilderUi.analyticsTracker(eventName, newPayload, optionsOverwrite);
};

/**
 * Replaces the default analytics.track method with a custom one that sanitises the payload before sending it onto the real track method
 */
export const addAnalyticsTrackInterceptor = () => {
  if (global.analytics.track) {
    global.proFormaBuilderUi.analyticsTracker = global.analytics.track;
    global.analytics.track = cleanSegmentTrackPayload;
  }
};

/**
 * Initialises segment fetching the analytics.js code from the remote source before sending 3 core events: identify, group & page
 * @param {string} segmentWriteKey - Determines where segment event payloads are sent
 * @param {function} Container - The React container where the app is being mounted
 * @param {string} userIdHash - Hashed representation of either the User Key (server) or Account Id (clound)
 * @param {string} sen - Represents the group name to associate with events from the identified user
 * @param {boolean} appcues - Controls if appcues should be included in the integrations list established during the analytics.load() call
 */
export const setupSegment = (segmentWriteKey, Container, userIdHash, sen, appcues) => {
  // Extract the pageCategory the Container class
  const pageCategory = typeof Container === 'function' && Container.name;

  // Extract the pageName from the UI route
  const pageName = global.location.pathname;

  // No segmentWriteKey provided so bail out early
  if (!segmentWriteKey) {
    return;
  }

  /*
        === Found a segmentWriteKey so continue with setup ===

        Create a queue, but don't obliterate an existing one!
        Treats analytics as both an object (for helper methods) and array (holding a queue of helpers to be called)
    */
  global.analytics = global.analytics || [];
  const { analytics } = global;

  // If the real analytics.js is already on the page return.
  if (analytics.initialize) {
    return;
  }

  // If the snippet was invoked already show an error.
  if (analytics.invoked) {
    sendDebugMsg({
      level: 'error',
      msg: 'Segment snippet included twice.',
      error: new Error('Segment snippet included twice.'),
    });

    return;
  }

  /*
        Invoked flag, to make sure the snippet is never invoked twice.
    */
  analytics.invoked = true;

  // A list of the methods in Analytics.js to stub.
  analytics.methods = [
    'trackSubmit',
    'trackClick',
    'trackLink',
    'trackForm',
    'pageview',
    'identify',
    'reset',
    'group',
    'track',
    'ready',
    'alias',
    'debug',
    'page',
    'once',
    'off',
    'on',
  ];

  /*
        Define a factory to create stubs. These are placeholders
        for methods in Analytics.js so that you never have to wait
        for it to load to actually record data. The `method` is
        stored as the first argument, so we can replay the data.
    */
  analytics.methods.forEach((methodName) => {
    analytics[methodName] = function anonymous(...args) {
      args.unshift(methodName);
      analytics.push(args);
    };
  });

  /*
        Define a method to load Analytics.js from our CDN,
        and that will be sure to only ever load it once.
    */
  analytics.load = (key, options) => {
    // Create an async script element based on your key.
    const script = global.document.createElement('script');
    script.type = 'text/javascript';
    script.async = true;
    script.src = `https://cdn.segment.com/analytics.js/v1/${key}/analytics.min.js`;

    script.onload = addAnalyticsTrackInterceptor;

    // Insert our script next to the first script element.
    const first = global.document.getElementsByTagName('script')[0];
    first.parentNode.insertBefore(script, first);

    /* eslint-disable no-underscore-dangle */
    analytics._loadOptions = options;
    /* eslint-enable no-underscore-dangle */
  };

  // Add a version to keep track of what's in the wild.
  analytics.SNIPPET_VERSION = '4.1.0';

  /*
        Load Analytics.js with your key, which will automatically
        load the tools you've enabled for your account. Boosh!
    */
  analytics.load(segmentWriteKey, {
    integrations: {
      All: false,
      'Segment.io': true,
      Appcues: !!appcues,
    },
  });

  // Identify the user
  sendIdentifyEvent(userIdHash);

  // Associate the user with a group by SEN
  sendGroupEvent(sen);

  // Send a page event
  sendPageEvent(pageCategory, pageName);
};

export const trackEventNameLookup = {
  changeIssueForm: 'Change Issue Form',
  createBlankForm: 'Create Blank Form',
  createCopyForm: 'Create Copy Form',
  createIssueWithForm: 'Create Issue With Form',
  createIssueWithFormFailed: 'Create Issue With Form Failed',
  createPortalForm: 'Create Portal Form',
  createTemplateForm: 'Create Template Form',
  deleteFormDesign: 'Delete Form Design',
  disableAllProjects: 'Disable All Projects',
  disableProject: 'Disable Project',
  downloadFormDesign: 'Download Form Design',
  editFormDesign: 'Edit Form Design',
  enableAllProjects: 'Enable All Projects',
  enableProject: 'Enable Project',
  installTemplate: 'Install Template',
  makeExternalForm: 'Make External Form',
  makeInternalForm: 'Make Internal Form',
  preferredIssueForm: 'Preferred Issue Form',
  previewTemplateForm: 'Preview Template Form',
  saveEditModeForm: 'Save Edit Mode Form',
  showForm: 'Show Form',
  viewTemplateDetails: 'View Template Details',
};
