// Sentry
import * as Raven from 'raven-js';
import sentryIgnoreErrorsList from './sentryIgnoreErrorsList';

// Analytics Utils
import { checkCanIgnoreDoNotTrack } from './analyticsUtils';
import { apiErrorTypeLookup } from './global-constants';
import { string } from 'prop-types';

// Models
export type DebugObjLevel = 'error' | 'warn' | 'info';
export enum DebugLvl {
  all = 'all',
  'warn+error' = 'warn+error',
  error = 'error',
  warn = 'warn',
  info = 'info',
  none = '',
}

export interface DebugObj {
  level: DebugObjLevel;
  msg: string;
  error: Error | string;
}

export interface ProFormaUtils {
  debugMsg?: (debugObj: DebugObj) => void;
}

declare global {
  interface Window {
    proFormaUtils?: ProFormaUtils;
  }
}

/**
 * Takes a variable and checks that its constructor (base class) is an "Object"
 * instead of being a "Date", "Null", "Array", etc.
 * @param variable - Should have "Object" as the constructor
 * @returns - Only returns true if "Object" is the variable's constructor
 */
export const isRealObject = variable => (
  typeof variable === 'object' &&
  Object.prototype.toString.call(variable).slice(8, -1) === 'Object'
);

/**
 * Used to submit handled Sentry reports triggered by debugMsg calls
 * @param {any} msg - Data associated with the error that has occurred (typically string)
 * @param {string} level - Represents the severity of the message (error, warn or info)
 * @param {string} callStack - Describes the functions that were called leading up to the event
 */
export const submitSentryData = (
  msg: string,
  level: DebugObjLevel,
  callStack: string,
  sen: string,
  analyticsEnabled?: boolean,
) => {
  if (checkCanIgnoreDoNotTrack() && analyticsEnabled) {
    Raven.captureException(new Error(msg), {
      level,
      release: process.env['REACT_APP_VERSION_NO'] || 'VERSION_NO was undefined',
      tags: {
        environment: process.env.NODE_ENV,
        sen,
      },
      extra: {
        callStack,
      },
    });
  }
};

/**
 * Used to provide a stack trace from the point in code where it was executed
 * @returns - A stringified list of the call stack separated by '\n'
 */
export const getStackTrace = (error: DebugObj['error'], depth: number): string => {
  // Only create a new Error object if the original one is undefined - use '' for ie9 <
  const stackStr = (typeof error === 'object') ? (error.stack || '') : (new Error().stack || '');
  const rawStackList = stackStr.split('\n');

  const stackList = rawStackList.slice(0, depth + 1);

  const stackTrace = stackList.filter(item => item.trim()).join('\n');

  return `\nCall Stack:\n${stackTrace}\n`;
};

/**
 * Creates a utility function for logging and debugging the application
 */
export const createDebugMsgFn = (
  sen: string,
  serverSettingsDebugLvl: DebugLvl,
  analyticsEnabled?: boolean,
) => {
  const debugMsg = (debugObj: DebugObj) => {
    // Make sure the console object exists
    if (typeof console === 'undefined') {
      return;
    }

    const msgLevel = debugObj.level.toLowerCase();
    const debugLvl = serverSettingsDebugLvl.toLowerCase();

    const meetsInfoReqs = (
      debugLvl === DebugLvl.all ||
      debugLvl === DebugLvl.info
    );

    const meetsWarnReqs = (
      debugLvl === DebugLvl.all ||
      debugLvl === DebugLvl['warn+error'] ||
      debugLvl === DebugLvl.warn
    );

    const meetsErrorReqs = (
      debugLvl === DebugLvl.all ||
      debugLvl === DebugLvl['warn+error'] ||
      debugLvl === DebugLvl.error
    );

    if (meetsInfoReqs && msgLevel === 'info') {
      console.log(debugObj.msg);
    }

    if (meetsWarnReqs && msgLevel === 'warn') {
      console.warn(debugObj.msg);
      console.warn(getStackTrace(debugObj.error, 2));
    }

    if (meetsErrorReqs && msgLevel === 'error') {
      console.error(debugObj.msg);
      console.error(getStackTrace(debugObj.error, 4));
    }

    // Always send warn and error debugMsgs to sentry
    if (msgLevel === 'warn') {
      submitSentryData(
        debugObj.msg,
        'warn',
        getStackTrace(debugObj.error, 2),
        sen,
        analyticsEnabled,
      );
    }

    if (msgLevel === 'error') {
      submitSentryData(
        debugObj.msg,
        'error',
        getStackTrace(debugObj.error, 4),
        sen,
        analyticsEnabled,
      );
    }
  };

  return debugMsg;
};

/**
 * Generates and throws an error message based on the errorDetails
 * @param {object} errorDetails - JSON Body returned when the an API error occurs - non-JSON response body === {boolean} false
 * @param {object} errorResponse - Error response object used for extracting the status & statusText or as a generic fallback (i.e. js error)
 */
export const throwNewApiError = (errorDetails, errorResponse) => {
  const errorTypeIsFunction = (
    !!errorDetails &&
    typeof apiErrorTypeLookup[errorDetails.error] === 'function'
  );

  const hasValidErrorContext = (
    !!errorDetails &&
    isRealObject(apiErrorTypeLookup[errorDetails.error]) &&
    typeof errorDetails.context === 'string' &&
    typeof apiErrorTypeLookup[errorDetails.error][errorDetails.context] === 'function'
  );

  // Error type matches a nested contextual message returned by a function
  if (!errorTypeIsFunction && hasValidErrorContext) {
    throw new Error(apiErrorTypeLookup[errorDetails.error][errorDetails.context](errorResponse.status, errorDetails.contextId));
  }

  // Error type matches a basic string message returned by a function
  if (errorTypeIsFunction && !hasValidErrorContext) {
    throw new Error(apiErrorTypeLookup[errorDetails.error](errorResponse.status));
  }

  // Unhandled or incompatible errorDetails were provided so attempting to use the fallback msg
  if (errorResponse.status && errorResponse.statusText) {
    throw new Error(`Request rejected with status: ${errorResponse.status} - ${errorResponse.statusText}`);
  }

  // Most likely a Javascript client side error
  throw errorResponse;
};

/**
 * Establish sentry for catching unhandled exceptions and make global utils available
 * Requirements for a global util:
 * - Has dependency on 1+ serverSettings
 * - All serverSettings must not be identical across all modules shown on a given page
 */
export const setupGlobalErrorHandling = (
  sen?: string,
  debugLvl?: DebugLvl,
  analyticsEnabled?: boolean,
) => {
  // Enables the catching of unhandled errors / exceptions that may be thrown
  Raven.config('https://ec788d0a115a444ba71b41263ddf3527@sentry.io/275198', {
    release: process.env['REACT_APP_VERSION_NO'] || 'VERSION_NO was undefined',
    environment: process.env.NODE_ENV,
    ...typeof sen !== 'undefined' && {
      tags: {
        sen,
      },
    },
    ignoreErrors: sentryIgnoreErrorsList,
  }).install();

  // Update or Create the global proFormaUtils window property
  window.proFormaUtils = {
    debugMsg: createDebugMsgFn(
      sen || '',
      debugLvl || DebugLvl.none,
      !!analyticsEnabled,
    ),
  };
};

export const sendDebugMsg = (debugObj: DebugObj) => {
  if (window.proFormaUtils && typeof window.proFormaUtils.debugMsg === 'function') {
    window.proFormaUtils.debugMsg(debugObj);
  }
};
