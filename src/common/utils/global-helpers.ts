
/**
 * Resolves or Rejects a promise when the global.flagName is set to True and then deletes the global flag before clearing the interval
 * @param {function} exitCmd - the resolve or reject param from a Promise
 * @param {string} flagName - References a boolean property attached to the global / window object that when set will trigger the exitCmd
 * @param {number} pollFrequency - Interval time in milliseconds to poll for the specified global flag
 * @param {Any} payload - Optional param when defined will be passed in as the body of the exitCmd
 */
export const createFlagControlledPromise = (
  exitCmd: (payload?: any) => any,
  flagName: string,
  pollFrequency: number,
  payload?: any,
) => {
  const intervalCb = () => {
    if (global[flagName] && payload) {
      exitCmd(payload);
      delete global[flagName];
      clearInterval(pollForFlag);
      return;
    }

    if (global[flagName]) {
      exitCmd();
      delete global[flagName];
      clearInterval(pollForFlag);
    }
  };

  const pollForFlag = setInterval(intervalCb, pollFrequency);
};

// TODO: Split these out into dedicated Util modules - temp disabled extra functionality for initial merge

// /**
//  * Responsible for turning on / off various features that may not be fully supported yet (for conflicts, localFlags overwrite backendFlags)
//  * @param {object} localFlags - Contains feature flag properties with boolean values defined locally (not intended for prod use)
//  * @param {object} backendFlags - Contains feature flag properties with boolean values defined on the backend passed in via serverSettings
//  */
// export const setFeatureFlags = (localFlags, backendFlags) => {
//   if (global.proFormaBuilderUi && global.proFormaBuilderUi.featureFlags) {
//     // Update existing feature flags
//     global.proFormaBuilderUi.featureFlags = {
//       ...global.proFormaBuilderUi.featureFlags,
//       ...isRealObject(backendFlags) && backendFlags,
//       ...isRealObject(localFlags) && localFlags,
//     };

//     return;
//   }

//   // No flags exist so set them
//   global.proFormaBuilderUi.featureFlags = {
//     ...isRealObject(backendFlags) && backendFlags,
//     ...isRealObject(localFlags) && localFlags,
//   };
// };
