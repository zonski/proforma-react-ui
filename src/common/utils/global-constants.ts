// These are constants used project wide for things like error handling in API requests

export const apiErrorTypeLookup = {
  client: statusCode => `${statusCode} Oops! Something went wrong. Try again.`,
  clientWithDetails: {
    'err.entity.issue.create': statusCode => `${statusCode} We were unable to create the issue. Check with your Jira Administrator.`,
  },
  jwt: {
    'err.jwt.parse': statusCode => `${statusCode} We can't verify your Jira connection. Try refreshing your browser.`,
    'err.jwt.signed': statusCode => `${statusCode} We can't verify your Jira connection. Try refreshing your browser.`,
    'err.jwt.verify': statusCode => `${statusCode} We can't verify your Jira connection. Try refreshing your browser.`,
  },
  moduleAccess: {
    'err.access.path': statusCode => `${statusCode} Oops! Something went wrong. Try again.`,
  },
  notLoggedIn: statusCode => `${statusCode} Oops, looks like you're not logged into Jira`,
  license: statusCode => `${statusCode} Verify ProForma License on the Manage Add-ons page in Administration (a.k.a Jira) settings.`,
  forbidden: statusCode => `${statusCode} Sorry, you don't have permission to do this. Check with your Jira Administrator.`,
  notFound: {
    'err.entity.form': statusCode => `${statusCode} We can't find the form you're looking for. Try again.`,
    'err.entity.issue': statusCode => `${statusCode} We can't find the issue you're looking for. Try again.`,
    'err.entity.project': statusCode => `${statusCode} We can't find the project you're looking for. Try again.`,
    'err.entity.servicedesk': statusCode => `${statusCode} We can't find the Service Desk you're looking for. Try again.`,
    'err.entity.requesttype': statusCode => `${statusCode} We can't find the Request Type you're looking for. Try again.`,
  },
  timeout: statusCode => `${statusCode} That took too long to complete. Try again and Save as you go.`,
  server: statusCode => `${statusCode} Oops! Something went wrong. Try again.`,
  liteExceeded: () => (
    'Want to build more than 3 forms? '
    + 'ProForma Lite is limited to three form designs in Jira at any given time. '
    + 'Upgrade to ProForma to design and build an unlimited number of forms.'
  ),
};

export const licenseLevelLookup = {
  full: 'full',
  lite: 'lite',
  unlicensed: 'unlicensed',
};

// This should be an empty object when committing code
export const localFeatureFlags = {};
