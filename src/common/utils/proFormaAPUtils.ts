/**
* @prop targetNoticeType (required) - Ensure the dialog.close event comes from the matching popup
* @prop title - Describes what the text in the notice title area should be
* @prop msg - Describes the body of the notice popup
* @prop alertStatusType - Controls what type of alert popup is being shown
* @prop confirmationName - The text to be shown on the confirmation button of the notice popup
 */
export interface CustomNoticeData {
  noticeType: string;
  title?: string;
  msg?: string;
  alertStatusType?: string;
  confirmationName?: string;
}

export interface DialogCreatePayload {
  payloadType: 'create';
  customData: CustomNoticeData;
  key: string;
  chrome: boolean;
  size: string;
  closeOnEscape: boolean;
}

export interface DialogClosePayload {
  payloadType: 'close';
  noticeType: string;
  status: boolean;
}

export enum ProFormaAPUtilsFnNames {
  once = 'once',
  create = 'create',
  close = 'close',
  getCustomData = 'getCustomData',
}

export enum ProFormaAPOnceEventOptions {
  onClose = 'dialog.close',
}

export interface ProFormaAPEvents {
  once: (event: string, callbackFn: any) => void;
}

export interface ProFormaAPDialog {
  create: (payload) => void;
  close: (payload) => void;
  getCustomData: (callbackFn: any) => void;
}

export interface ProFormaAP {
  dialog: ProFormaAPDialog;
  events: ProFormaAPEvents;
}

declare global {
  interface Window {
    proFormaAP?: ProFormaAP;
  }
}

export interface ProFormaAPUtilsProps {
  fnName: ProFormaAPUtilsFnNames;
  payload?: DialogCreatePayload | DialogClosePayload;
  event?: ProFormaAPOnceEventOptions;
  callback?: (data: any) => void;
}

export const proFormaAPUtils = ({
  fnName,
  payload,
  event,
  callback,
}: ProFormaAPUtilsProps) => {
  if (!window.proFormaAP) {
    return;
  }

  if (fnName === ProFormaAPUtilsFnNames.close && payload && payload.payloadType === 'close') {
    window.proFormaAP.dialog.close(payload);
    return;
  }

  if (fnName === ProFormaAPUtilsFnNames.create && payload && payload.payloadType === 'create') {
    window.proFormaAP.dialog.create(payload);
    return;
  }

  if (fnName === ProFormaAPUtilsFnNames.getCustomData && callback) {
    window.proFormaAP.dialog.getCustomData(callback);
    return;
  }

  if (fnName === ProFormaAPUtilsFnNames.once && event === ProFormaAPOnceEventOptions.onClose && callback) {
    window.proFormaAP.events.once(event, callback);
    return;
  }
};
