interface AppCues {
  show: (appCuesId: string) => void;
}

declare global {
  interface Window {
    AppCues?: AppCues;
  }
}

export const callAppcuesShow = (appcuesId: string) => {
  if (window.AppCues) {
    window.AppCues.show(appcuesId);
  }
};
