import { Project } from '../models/Project';
import { PagedResults } from '../models/PagedResults';

export interface ProjectApi {

  search(pageSize: number, cursor?: string): Promise<PagedResults<Project>>;

  isProjectEnabled(key): Promise<boolean>;

  setProjectEnabled(key, enabled: boolean): Promise<void>;

  setAllProjectsEnabled(enabled: boolean): Promise<void>;
}
