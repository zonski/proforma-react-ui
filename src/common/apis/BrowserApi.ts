export type licenseLevel = 'full' | 'lite' | 'unlicensed';

export interface BrowserApi {

  getProjectKey(): string | undefined;

  getlicenseLevel(): licenseLevel;

  getFlag(flag: string): any;

  createUrl(relativePath: string): string;

  createAssetUrl(relativePath: string): string

  goToUrl(fullPath: string);

  downloadFile(relativePath: string, nameToSaveAs: string);

  showDialog(key: string, data): Promise<any>;
}
