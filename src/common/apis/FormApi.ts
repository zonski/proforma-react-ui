import { PagedResults } from '../models/PagedResults';
import { Form } from '../models/Form';

export interface FormApi {

  search(pageSize: number,
         projectKey?: string,
         cursor?: string,
         sortKey?: string,
         sortOrder?: string): Promise<PagedResults<Form>>;

  getXlsxDownloadUrl(projectKey, formId): string;

  deleteForm(projectKey, formId): Promise<void>;
}
