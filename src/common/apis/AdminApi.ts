import { ConnectionSummary } from '../models/ConnectionSummary';

export interface AdminApi {

  isIssueFormsEnabled(): Promise<boolean>;

  setIssueFormsEnabled(enabled: boolean): Promise<boolean>;

  getConnectionSummaries(): Promise<ConnectionSummary[]>

}
