import { ProjectApi } from '../ProjectApi';
import { Project } from '../../models/Project';
import { MockSearchHelper } from './MockSearchHelper';
import { MockData } from './data/MockData';

export class MockProjectApi implements ProjectApi {

  private mockSearch: MockSearchHelper<Project>;

  constructor(private mockData: MockData) {
    this.mockSearch = new MockSearchHelper();
  }

  search(pageSize: number, cursor?: string) {
    return this.mockSearch.search(() => this.mockData.projects, pageSize, cursor);
  }

  isProjectEnabled(key): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      setTimeout(() => {
        resolve(this.mockData.getProject(key).enabled);
      }, 1000);
    });
  }

  setProjectEnabled(key, enabled: boolean): Promise<void> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        this.mockData.getProject(key).enabled = enabled;
        resolve();
      }, 1000);
    });
  }

  setAllProjectsEnabled(enabled: boolean): Promise<void> {
    return new Promise((resolve, reject) => {
      setTimeout(() => {
        this.mockData.projects.forEach(p => p.enabled = enabled);
        resolve();
      }, 1000);
    });
  }
}
