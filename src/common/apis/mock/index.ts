import { MockAdminApi } from "./MockAdminApi";
import { MockFormApi } from './MockFormApi';
import { MockProjectApi } from './MockProjectApi';
import { MockBrowserApi } from './MockBrowserApi';
import { MockData } from './data/MockData';

export function createMockApis(mockData: MockData) {
  return {
    browserApi: new MockBrowserApi(),
    adminApi: new MockAdminApi(mockData),
    projectApi: new MockProjectApi(mockData),
    formApi: new MockFormApi(mockData)
  };
}
