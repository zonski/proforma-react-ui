import { FormApi } from '../FormApi';
import { MockSearchHelper } from './MockSearchHelper';
import { Form } from '../../models/Form';
import { MockData } from './data/MockData';

export class MockFormApi implements FormApi {

  private mockSearch: MockSearchHelper<Form>;

  constructor(private mockData: MockData) {
    this.mockSearch = new MockSearchHelper(false);
  }

  search(pageSize: number,
         projectKey?: string,
         cursor?: string,
         sortKey?: string,
         sortOrder?: string) {
    return this.mockSearch.search(() => this.mockData.forms.filter(f => !projectKey || f.key === projectKey),
      pageSize, cursor, sortKey, sortOrder);
  }

  getXlsxDownloadUrl(projectKey, formId): string {
    return "https://docs.collectiveaccess.org/images/6/68/Sample_data.xlsx";
  }

  deleteForm(projectKey, formId): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      setTimeout(() => {
        this.mockData.deleteForm(formId);
        resolve();
      }, 1000);
    });
  }
}
