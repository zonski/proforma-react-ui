import { MockData, ProjectWithEnabledState } from './MockData';
import { RequestType } from '../../../models/Form';

export const loadTypicalMockData = (mockData: MockData) => {
  console.log('Loading typical mock data');
  createConnections(mockData);
  createProjects(mockData);
  createForms(mockData);
  return mockData;
};

//-----------------------------------------------------------------------------
// Connections

const createConnections = (mockData: MockData) => {
  mockData.connections.push(
    { "id": 1, "name": "Internal database", "source": "https://internaldatabase.example.com/api/query", "status": "ok" }
  );

  mockData.connections.push(
    { "id": 4, "name": "Product list API", "source": "https://api.example.net/rest/2.0/products", "status": "failing" }
  );
};

//-----------------------------------------------------------------------------
// Projects

const createProjects = (mockData: MockData) => {
  for (let i = 0; i < 100; i++) {
    let project: ProjectWithEnabledState = {
      key: 'PRJ_' + (i+1),
      name: "Test Project " + (i+1),
      projectTypeKey: (i % 2 == 0 ? 'software' : 'servicedesk'),
      projectTypeName: (i % 2 == 0 ? 'Software' : 'Service Desk'),
      smallAvatarUrl: 'https://proformajax.atlassian.net/secure/projectavatar?size=small&avatarId=10324',
      enabled: i % 2 == 0
    };
    mockData.projects.push(project);
  }
};

//-----------------------------------------------------------------------------
// Forms

let nextFormId = 0;

const createForms = (mockData: MockData) => {
  mockData.projects.forEach(project => {
    for (let i = 0; i < 100; i++) {
      mockData.forms.push({
        id: nextFormId++,
        name: `Form ${i+1} in Proj ${project.key}`,
        key: project.key,
        project: project.name,
        updated: {
          friendly: '15 Mar 2019'
        },
        requesttypes: createRequestTypes(i),
        editUrl: `url-to-edit-form-FRM_${i+1}`
      });
    }
  });
};

const createRequestTypes = (index) => {
  let types: RequestType[] = [];
  for (let i = 0; i < index % 5; i++) {
    types.push({
        id: 'rt' + index + "." + i,
        name: getTypeName(index),
        portal: index % 2 == 0,
        hidden: index % 4 == 0,
        newIssue: index % 3 == 0
      }
    );
  }
  return types;
};

const getTypeName = (index) => {
  const names = ['Story', 'Bug', 'Epic', 'Task'];
  return names[index % names.length];
};

