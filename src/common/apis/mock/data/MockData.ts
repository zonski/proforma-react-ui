import { Project } from '../../../models/Project';
import { ConnectionSummary } from '../../../models/ConnectionSummary';
import { Form } from '../../../models/Form';

export class MockData {

  issueFormsEnabled: boolean = false;

  connections: ConnectionSummary[] = [];
  projects: ProjectWithEnabledState[] = [];
  forms: Form[] = [];

  getProject(key: string): ProjectWithEnabledState {
    let p = this.projects.find(p => p.key === key);
    if (!p) {
      throw `No project for key ${key}`;
    }
    return p;
  }

  getForm(id: number): Form {
    let f = this.forms.find(f => f.id === id);
    if (!f) {
      throw `No project for ID ${id}`;
    }
    return f;
  }

  deleteForm(id: number) {
    this.forms = this.forms.filter(f => f.id !== id);
  }

  clear() {
    console.log('Clearing mock data');
    this.issueFormsEnabled = false;
    this.connections.length = 0;
    this.projects.length = 0;
    this.forms.length = 0;
  }
}

export interface ProjectWithEnabledState extends Project {
  enabled: boolean;
}
