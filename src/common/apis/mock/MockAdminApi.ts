import { ConnectionSummary } from '../../models/ConnectionSummary';
import { AdminApi } from "../AdminApi";
import { MockData } from './data/MockData';

export class MockAdminApi implements AdminApi {

  constructor(private mockData: MockData) {
  }

  isIssueFormsEnabled(): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      setTimeout(() => {
        resolve(this.mockData.issueFormsEnabled);
      }, 1000);
    });
  }


  setIssueFormsEnabled(enabled: boolean): Promise<boolean> {
    return new Promise<boolean>((resolve, reject) => {
      setTimeout(() => {
        this.mockData.issueFormsEnabled = enabled;
        resolve(this.mockData.issueFormsEnabled);
      }, 1000);
    });
  }

  getConnectionSummaries(): Promise<ConnectionSummary[]> {
    return new Promise<ConnectionSummary[]>((resolve, reject) => {
      setTimeout(() => {
        resolve(this.mockData.connections);
      }, 1000);
    });
  }

}
