import { BrowserApi, licenseLevel } from '../BrowserApi';

export class MockBrowserApi implements BrowserApi {

  projectKey: string | undefined = "TEST";
  flags = {};

  getProjectKey(): string | undefined {
    return this.projectKey;
  }

  getlicenseLevel(): licenseLevel {
    return 'lite';
  }

  getFlag(flag: string): any {
    return this.flags[flag];
  }

  createUrl(relativePath: string): string {
    return relativePath;
  }

  createAssetUrl(relativePath: string): string {
    return `${process.env.PUBLIC_URL}${relativePath}`;
  }

  goToUrl(fullPath: string) {
    window.location.href = fullPath;
  }

  downloadFile(fullPath: string, name?: string) {
    const downloadLink = document.createElement('a');

    downloadLink.href = fullPath;
    if (name) {
      downloadLink.setAttribute('download', name);
    }

    document.body.appendChild(downloadLink);
    downloadLink.click();
    document.body.removeChild(downloadLink);
  }

  showDialog(key: string, data): Promise<void> {
    return new Promise<void>((resolve, reject) => {
      alert(`dialog for key ${key}`);
      resolve();
    });
  }
}
