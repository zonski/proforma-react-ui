import { PagedResults } from '../../models/PagedResults';
import { Form } from '../../models/Form';
import { ApiUtil } from './index';
import { FormApi } from '../FormApi';

export class LiveFormApi implements FormApi {

  constructor(private util: ApiUtil) {
  }

  search(pageSize: number,
         projectKey?: string,
         cursor?: string,
         sortKey?: string,
         sortOrder?: string): Promise<PagedResults<Form>> {

    return this.util.get(`/api/1/projects/${projectKey ? projectKey : '*'}/forms?pageSize=${pageSize}`
        + this.util.param('cursor', cursor)
        + this.util.param('sortKey', sortKey)
        + this.util.param('sortOrder', sortOrder)
    );
  }

  getXlsxDownloadUrl(projectKey, formId): string {
    return `/api/1/projects/${projectKey}/forms/${formId}/spreadsheet`;
  }

  deleteForm(projectKey, formId): Promise<void> {
    return this.util.delete(`/api/1/projects/${projectKey}/forms/${formId}`);
  }
}
