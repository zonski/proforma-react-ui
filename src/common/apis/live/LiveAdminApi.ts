import { ConnectionSummary } from '../../models/ConnectionSummary';
import { AdminApi } from "../AdminApi";
import { ApiUtil } from "./index";

export class LiveAdminApi implements AdminApi {

  constructor(private util: ApiUtil) {
  }

  isIssueFormsEnabled(): Promise<boolean> {
    return this.util.get(`/api/1/admin/settings/globallinks`).then(result =>
      result.enabled || false
    );
  }


  setIssueFormsEnabled(enabled: boolean): Promise<boolean> {
    return this.util.put(
      `/api/1/admin/settings/globallinks`,
      {enabled: enabled}).then(result =>
      result.enabled || false
    );
  }


  getConnectionSummaries(): Promise<ConnectionSummary[]> {
    return this.util.get(`/api/1/admin/connection`)
      .then(result =>
        result || []
    );
  }

}
