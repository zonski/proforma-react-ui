import { BrowserApi, licenseLevel } from '../BrowserApi';
import { ApiUtil } from './index';

export class LiveBrowserApi implements BrowserApi {

  constructor(private util: ApiUtil) {
  }

  getProjectKey(): string | undefined {
    return this.util.serverSettings.projectKey;
  }

  getlicenseLevel(): licenseLevel {
    return this.util.serverSettings.licenseLevel;
  }

  getFlag(flag: string): any {
    return this.util.serverSettings.flags[flag];
  }

  createUrl(relativePath: string): string {
    const {
      jiraBaseUrl,
      pagePrefix
    } = this.util.serverSettings;
    relativePath = relativePath.charAt(0) === '/' ? relativePath : `/${relativePath}`;
    return `${jiraBaseUrl}${pagePrefix}${relativePath}`;
  }

  createAssetUrl(relativePath: string): string {
    const {
      assetPath
    } = this.util.serverSettings;
    relativePath = relativePath.charAt(0) === '/' ? relativePath : `/${relativePath}`;
    return `${assetPath}${relativePath}`;
  }

  goToUrl(fullPath: string) {
    window.open(fullPath, '_top', 'noopener');
  }

  downloadFile(relativePath: string, nameToSaveAs: string) {

    const {
      token,
      apiBaseUrl
    } = this.util.serverSettings;

    const preFlight: RequestInit = {
      method: 'GET',
      credentials: "same-origin",
      headers: {
        'Content-Type': 'application/vnd.openxmlformats-officedocument.spreadsheetml.sheet',
        Authorization: `Bearer ${token}`,
      },
    };

    return fetch(apiBaseUrl + relativePath, preFlight)
      .then(response => {
        if (response.ok) {
          return response.blob();
        }
        throw response;
      })
      .then((blob) => {
        const fileUrl = window.URL.createObjectURL(blob);

        if (window.navigator.msSaveBlob) {
          window.navigator.msSaveBlob(blob, nameToSaveAs);
          return;
        }

        const downloadLink = window.document.createElement('a');

        downloadLink.href = fileUrl;
        downloadLink.setAttribute('download', nameToSaveAs);

        window.document.body.appendChild(downloadLink);
        downloadLink.click();
        window.document.body.removeChild(downloadLink);
      });
  }

  showDialog(key: string, data): Promise<any> {
    return new Promise<void>((resolve, reject) => {
      const {
        atlasPlugins
      } = this.util.serverSettings;

      atlasPlugins.dialog.create({
        key: key,
        chrome: false,
        size: 'large',
        closeOnEscape: false,
        customData: data
      });

      atlasPlugins.events.once('dialog.close', (confirmData) => {
        resolve(confirmData);
      });
    });
  }
}
