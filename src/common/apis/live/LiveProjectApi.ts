import { PagedResults } from '../../models/PagedResults';
import { Project } from '../../models/Project';
import { ProjectApi } from '../ProjectApi';
import { ApiUtil } from './index';

export class LiveProjectApi implements ProjectApi {

  constructor(private util: ApiUtil) {
  }

  search(pageSize: number, cursor?: string): Promise<PagedResults<Project>> {
    return this.util.get(`/api/1/admin/projects?pageSize=${pageSize}`
        + this.util.param('cursor', cursor));
  }

  isProjectEnabled(key): Promise<boolean> {
    return this.util.get(`/api/1/admin/projects/${key}`)
      .then(project => project.enabled);
  }

  setProjectEnabled(key, enabled: boolean): Promise<void> {
    return this.util.putWithNoResult(`/api/1/admin/projects/${key}`, {
      enabled: enabled
    });
  }

  setAllProjectsEnabled(enabled: boolean): Promise<void> {
    return this.util.putWithNoResult(`/api/1/admin/projects/*`, {
      enabled: enabled
    });
  }
}
