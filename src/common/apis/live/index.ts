import { LiveProjectApi } from './LiveProjectApi';
import { LiveAdminApi } from "./LiveAdminApi";
import { LiveFormApi } from './LiveFormApi';
import { LiveBrowserApi } from './LiveBrowserApi';
import { licenseLevel } from '../BrowserApi';
import { DebugLvl } from '../../utils/errorHandlingUtils';

export type ResponseType = 'json' | 'blob' | 'text' | 'none';

export interface PermissionLevel {
  submit?: boolean;
  unlock?: boolean;
  reopen?: boolean;
  save?: boolean;
  create?: boolean;
  delete?: boolean;
  download?: boolean;
  addForm?: boolean;

  // Determines if a user can toggle a form between internal and external
  toggleAccess?: boolean;
}

export interface ServerSettings {
  analyticsEnabled?: boolean;
  apiBaseUrl: string;
  appcues?: string;
  assetPath: string;
  flags: any;
  atlasPlugins;
  buildUrl?: string;
  closeUrl?: string;
  debugLvl?: DebugLvl;
  jiraBaseUrl: string;
  licenseLevel: licenseLevel;
  locale: string;
  pagePrefix: string;
  permissionLevel: PermissionLevel;
  projectFormId?: string;
  projectKey?: string;
  segmentWriteKey?: string;
  sen?: string;
  token: string;
  tz: string;
  userIdHash?: string;
}

export function createLiveApis(serverSettings: ServerSettings) {
  const util = new ApiUtil(serverSettings);
  return {
    browserApi: new LiveBrowserApi(util),
    adminApi: new LiveAdminApi(util),
    projectApi: new LiveProjectApi(util),
    formApi: new LiveFormApi(util)
  };
}

export class ApiUtil {

  serverSettings: ServerSettings;

  constructor(serverSettings: ServerSettings) {
    this.serverSettings = serverSettings;
  }

  get(path: string): Promise<any> {
    return this.fetchApi('GET', path);
  }

  put(path: string, body?): Promise<any> {
    return this.fetchApi('PUT', path, body);
  }

  putWithNoResult(path: string, body?): Promise<any> {
    return this.fetchApi('PUT', path, body, 'none');
  }

  post(path: string, body?): Promise<any> {
    return this.fetchApi('POST', path, body);
  }

  delete(path: string, body?): Promise<any> {
    return this.fetchApi('DELETE', path, body);
  }

  param(name, value, joiner = '&') {
    return !(value === null || value === undefined)
      ? `${joiner}${name}=${value}`
      : '';
  }

  fetchApi(method: string,
    path: string,
    body?: any,
    responseType: ResponseType = 'json'): Promise<any> {

    const {
      apiBaseUrl,
      token
    } = this.serverSettings;

    return fetch(
      apiBaseUrl + path,
      {
        method,
        credentials: 'same-origin',
        headers: {
          ...token && { Authorization: `Bearer ${token}` },
          'Content-Type': 'application/json',
        },
        ...body && { body: JSON.stringify(body) },
      }
    ).then(response => {
      if (response.status === 204) {
        return '';
      } else {
        switch (responseType) {
          case 'json': return response.json();
          case 'blob': return response.blob();
          case 'text': return response.text();
          case 'none': return '';
          default: return response;
        }
      }
    });
  }
}
