import * as React from 'react';
import { inject, observer } from 'mobx-react';
import EditorRemoveIcon from '@atlaskit/icon/glyph/editor/remove';
import Spinner from '@atlaskit/spinner';
import Button, { ButtonGroup } from '@atlaskit/button';
import Tooltip from '@atlaskit/tooltip';

export const RowActions = inject('apis')(observer((props) => (
  <ButtonGroup>
    <Tooltip content="Edit project form">
      <Button spacing="compact"
              onClick={() => editForm(props.apis, props.form)}>
        Edit
      </Button>
    </Tooltip>

    {props.apis.browserApi.getFlag('SpreadsheetDownload') &&
      <Tooltip content="Download as *.xlsx spreadsheet">
        <Button spacing="compact"
                onClick={() => downloadXlsx(props.apis, props.form)}>
          Download
        </Button>
      </Tooltip>
    }

    <Tooltip content="Delete project form">
      <DeleteButton {...props}/>
    </Tooltip>

  </ButtonGroup>
)));

const DeleteButton = observer((props) => {
  if (props.form.deleting) {
    return (<Spinner size="small"/>);
  } else {
    return (
      <Button spacing="compact"
              iconBefore={(<EditorRemoveIcon label='Delete project form'/>)}
              onClick={() => deleteForm(props.apis, props.form, props.search)}>
        {props.form.deleting}
      </Button>
    );
  }
});


const editForm = (apis, form) => {
  apis.browserApi.goToUrl(form.editUrl);
};

const downloadXlsx = (apis, form) => {
  let downloadUrl = apis.formApi.getXlsxDownloadUrl(form.key, form.id);
  apis.browserApi.downloadFile(downloadUrl, `${form.id}-proforma-form-spreadsheet.xlsx`);
};

const deleteForm = (apis, form, search) => {
  const deleteConfirmText = `Are you sure you want to delete the ${form.name} form? This cannot be undone.`;
  apis.browserApi.showDialog('notice', {
    title: 'Confirm Deleting Form',
    msg: deleteConfirmText,
    confirmationName: 'Delete',
    noticeType: 'deleteProjectFormConfirm'
  })
    .then((confirmation) => {
      if (confirmation.status) {
        form.deleteForm().then(() => search.refresh());
      }
    });
};


