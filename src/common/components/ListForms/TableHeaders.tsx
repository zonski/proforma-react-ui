export const createTableHeaders = (showProjectColumn: boolean = true) => {

  let columns: any[] = [];

  columns.push({
    key: 'form',
    content: 'Form',
    isSortable: false,
    width: 30
  });

  if (showProjectColumn) {
    columns.push({
      key: 'project',
      content: 'Project',
      isSortable: true,
      width: 25
    });
  }

  columns.push({
    key: 'issueType',
    content: 'Request/Issue Type',
    isSortable: false,
    width: 40
  });

  columns.push({
    key: 'updated',
    content: 'Last Updated',
    isSortable: false,
    width: 15
  });

  columns.push({
    key: 'actions',
    content: '',
    isSortable: false,
    width: 35
  });

  return {cells: columns};
};