import * as React from 'react';
import { inject } from 'mobx-react';
import styled from 'styled-components';

export const ListFormsFooter = inject('apis')((props) => (
  <Wrapper>
    <div>
      <h2>Build a form</h2>
      <Message>
        ProForma makes it easy to build forms in Jira to collect the information you and your teams need.
        Click <strong>Create Form</strong> to get started or refer to the
        help <a target="_blank" rel="noopener" href="https://docs.thinktilt.com/">guide</a>.
      </Message>
    </div>
    {
      props.licenseLevel !== 'lite' ? '' : (
        <LiteLicenceMessage>
          ProForma Lite Limitation: You can only build three forms with ProForma Lite.
          <a target="_top" rel="noopener" href="https://marketplace.atlassian.com/apps/1215833">Install the full ProForma</a>
          to build as many forms as you need.
        </LiteLicenceMessage>)
    }
    <FooterImage src={props.apis.browserApi.createAssetUrl('/images/proforma-ui-forms-list-footer-img.png')} />
  </Wrapper>
));

const Wrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: center;
  width: 65%;
  margin: 2rem auto;
  margin-top: 4rem;
`;

const Message = styled.p`
  width: 100%;
  margin: 0.8rem 0;
  > strong {
    margin: 0 0.4rem;
  }
`;

const LiteLicenceMessage = styled.div`
  width: 100%;
  margin: 0.8rem 0;
  font-weight: 700;
`;

const FooterImage = styled.img`
  display: block;
  width: 40rem;
  margin: 1rem auto 0;
`;

