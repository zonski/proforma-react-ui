import * as React from 'react';
import { inject, observer } from 'mobx-react';
import '../../../common/styles/CommonModuleStyles.css';
import { DynamicTableStateless } from '@atlaskit/dynamic-table';
import Tooltip from '@atlaskit/tooltip';
import EditorPanelIcon from '@atlaskit/icon/glyph/editor/panel';
import Lozenge from '@atlaskit/lozenge';
import styled from 'styled-components';
import { RowActions } from './RowActions';
import { FormSearchStore } from '../../../common/stores/domain/FormDomain-store';
import { PaginatedTable } from '../../../common/components/PaginatedTable';
import { FormStore } from '../../../common/stores/domain/Form-store';
import { createTableHeaders } from './TableHeaders';
import { ListFormsFooter } from './ListFormsFooter';

@inject('stores')
@observer
export default class ListForms extends React.Component<{ projectKey?: string, showProjectColumn?: boolean }> {

  private search: FormSearchStore;
  private projectKey: string | undefined;
  private showProjectColumn: boolean = false;

  constructor(props) {
    super(props);
    this.projectKey = props.projectKey;
    this.search = props.stores.formDomainStore.startSearch(this.projectKey);
    this.showProjectColumn = props.showProjectColumn;
  }

  render() {
    return (
      <div>
        <PaginatedTable
          store={this.search}
          columns={createTableHeaders(this.showProjectColumn)}
          renderRow={(form) => createRow(form, this.search, { showProjectColumn: this.showProjectColumn })}
        />
        <ListFormsFooter/>
      </div>
    );
  }
}

function createRow(form: FormStore, search: FormSearchStore, settings) {

  let columns: any[] = [];

  columns.push({
    key: 'form',
      content: form.name + (form.deleted ? ' deleted' : ''),
  });

  if (settings.showProjectColumn) {
    columns.push({
      key: 'project',
      content: form.projectName,
    });
  }

  columns.push({
    key: 'issueType',
      content: (!form.requestTypes ? '' : form.requestTypes.map(t => (<RequestTypeRow key={t.id} type={t}/>) )),
  });

  columns.push({
    key: 'updated',
      content: form.lastUpdatedFriendly,
  });

  columns.push({
    key: 'actions',
      content: (<RowActions form={form} search={search}/>),
  });

  return ({
    key: `row-${form.id}`,
    cells: columns
  });
}

const RequestTypeRowDiv = styled.div`
  display: flex;
  justify-content: space-between;
  margin: 0.25rem 0;

  > span:first-child {
      margin-right: 0.4rem;
  }
`;

const RequestTypePilDiv = styled.div`
  display: flex;
  align-items: center;
  justify-content: flex-end;
  width: 13rem;
  min-width: 13rem;
`;

const RequestTypeRow = ({type}) => (
  <RequestTypeRowDiv>
    <span>{type.name}</span>
    <RequestTypePilDiv>
      { type.portal && !type.hidden && <PortalPill/> }
      { type.portal && type.hidden && <PortalHiddenPill/> }
      { type.newIssue && <NewIssuePill/> }
    </RequestTypePilDiv>
  </RequestTypeRowDiv>
);

export const PortalPill = () => (
  <Lozenge appearance="success">PORTAL</Lozenge>
);

const HiddenPillSpan = styled.span`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

export const PortalHiddenPill = () => (
  <Tooltip content="This request type is currently hidden from the portal, add it to a group to be visible on the portal.">
    <HiddenPillSpan>
      <Lozenge appearance="removed">PORTAL - HIDDEN</Lozenge>
      <EditorPanelIcon label="Edit"/>
    </HiddenPillSpan>
  </Tooltip>
);

export const NewIssuePill = () => (
  <Lozenge appearance="success">NEW ISSUE</Lozenge>
);

