export interface FormSettingsInfo {
  name: string;
  portalRequestTypeIds: number[];
  issueRequestTypeIds: number[];
  newIssueIssueTypeIds: number[];
  newIssueRequestTypeIds: number[];
  portalLock: boolean;
}
