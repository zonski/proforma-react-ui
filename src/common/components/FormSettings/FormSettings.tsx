import * as React from 'react';
import { observer, inject } from 'mobx-react';
import { action, computed, IReactionDisposer, observable, reaction } from 'mobx';

// Atlaskit Components
import InlineEdit, { SingleLineTextInput } from '@atlaskit/inline-edit';
import Select from '@atlaskit/select';
import Tag from '@atlaskit/tag';
import TagGroup from '@atlaskit/tag-group';
import { HelperMessage } from '@atlaskit/form';

// Styles
import { SettingsPanel } from '../../../modulesFB/shared-styles';
import {
  FormSettingsWrapper,
} from './FormSettings-styles';

// Models
import {
  ReferenceData,
  ConfigureApis,
  ConfigureStore,
} from '../../../modulesFB/FormBuilderApp/components/Configure/Configure-models';
import { FormSettingsInfo } from './FormSettings-models';

interface FormSettingsProps {
  configureApis: ConfigureApis;
  configureStore: ConfigureStore;
  formSettingsInfo: FormSettingsInfo;
  refData: ReferenceData;
  jwtToken?: string;
}

interface PortalLockOption {
  label: string;
  value: boolean;
}

interface RiTypeOption {
  label: string;
  value: string;
}

@inject('configureStore')
@observer
export default class FormSettings extends React.Component<FormSettingsProps, {}> {
  @observable
  private scratchConfiguration?: FormSettingsInfo;

  private disposeScratch: IReactionDisposer;

  private portalLockOptions = [{ label: 'Lock form contents', value: true }, { label: 'Do not lock form contents', value: false }];

  @computed get recommendedFormOptions() {
    const availableRefData = this.props.refData || { issueTypes: [] };
    const hasReqestTypes = (availableRefData.requestTypes && !!availableRefData.requestTypes.length);

    // Duplicated map fn so typescript can track which type is being used for typeOptions
    return hasReqestTypes ? (
      availableRefData.requestTypes.map(type => ({
        label: type.name,
        value: type.id,
      }))
    ) : (
        availableRefData.issueTypes.map(type => ({
          label: type.name,
          value: type.id,
        }))
      );
  }

  @computed get requestTypeOptions() {
    return this.props.refData
      ? this.props.refData.requestTypes.map(requestType => ({
        label: requestType.name,
        value: requestType.id,
      }))
      : [];
  }

  @computed get riTypeOptions() {
    return this.props.refData
      ? [
        ...this.props.refData.issueTypes.filter(issueType => !issueType.subtask).map(issueType => ({
          label: issueType.name,
          value: `i${issueType.id}`,
        })),
        ...this.props.refData.requestTypes.map(requestType => ({
          label: requestType.name,
          value: `r${requestType.id}`,
        })),
      ]
      : [];
  }

  @computed get selectedPortalRequestTypeIds() {
    if (this.scratchConfiguration) {
      const scratchConfiguration: FormSettingsInfo = this.scratchConfiguration;
      return this.requestTypeOptions.filter(requestType => scratchConfiguration.portalRequestTypeIds.includes(parseInt(requestType.value, 10)));
    }
    return [];
  }

  @computed get selectedIssueRequestTypeIds() {
    if (this.scratchConfiguration) {
      const scratchConfiguration: FormSettingsInfo = this.scratchConfiguration;
      return this.recommendedFormOptions.filter(requestType => scratchConfiguration.issueRequestTypeIds.includes(parseInt(requestType.value, 10)));
    }
    return [];
  }

  @computed get selectedNewIssueRiTypeIds() {
    if (this.scratchConfiguration) {
      const scratchConfiguration: FormSettingsInfo = this.scratchConfiguration;
      return this.riTypeOptions.filter(requestType =>
        requestType.value.charAt(0) === 'i'
          ? scratchConfiguration.newIssueIssueTypeIds.includes(parseInt(requestType.value.substring(1), 10))
          : scratchConfiguration.newIssueRequestTypeIds.includes(parseInt(requestType.value.substring(1), 10)),
      );
    }
    return [];
  }

  /**
   * Generates an onChange handler for the given property.
   * @param property The name of the property on the scratchConfiguration to be updated by onChange events
   */
  private generateOnChange(property: string) {
    return (e: Event) => {
      if (e.target && this.scratchConfiguration) {
        this.scratchConfiguration[property] = e.target['value'];
      }
    };
  }

  private generateMultiOnChange(property: string) {
    return (e: RiTypeOption[]) => {
      if (e && this.scratchConfiguration) {
        this.scratchConfiguration[property] = e.map(option => parseInt(option.value, 10));
      }
    };
  }

  @action
  onNameChange = (e: Event) => {
    if (e.target && this.scratchConfiguration) {
      this.scratchConfiguration.name = e.target['value'];
    }
  }

  @action
  onPortalLockChange = (option: PortalLockOption) => {
    if (option && this.scratchConfiguration) {
      this.scratchConfiguration.portalLock = option.value;
    }
  }

  @action
  onNewIssueRiTypeChange = (options: RiTypeOption[]) => {
    if (this.scratchConfiguration) {
      const newScratchConfigTypeIds = options.reduce(
        (newScratchTypeIds, riTypeOption) => {
          if (riTypeOption.value[0] === 'i') {
            newScratchTypeIds.issueTypeIdsList.push(parseInt(riTypeOption.value.slice(1), 10));
            return newScratchTypeIds;
          }

          newScratchTypeIds.requestTypeIdsList.push(parseInt(riTypeOption.value.slice(1), 10));
          return newScratchTypeIds;
        },
        {
          requestTypeIdsList: [],
          issueTypeIdsList: [],
        } as {
          requestTypeIdsList: number[];
          issueTypeIdsList: number[];
        },
      );

      this.scratchConfiguration.newIssueIssueTypeIds = newScratchConfigTypeIds.issueTypeIdsList;
      this.scratchConfiguration.newIssueRequestTypeIds = newScratchConfigTypeIds.requestTypeIdsList;
    }
  }

  private handleConfirmCreateIssueChanges = () => {
    this.generateOnConfirm(['newIssueIssueTypeIds', 'newIssueRequestTypeIds'])();
  }
  private handleCancelCreateIssueChanges = () => {
    this.generateOnCancel('newIssueIssueTypeIds')();
    this.generateOnCancel('newIssueRequestTypeIds')();
  }

  private generateOnConfirm(properties: string[]) {
    return () => {
      if (this.scratchConfiguration) {
        const newFormSettingsData = {
          ...this.props.configureStore.formSettingsData,
          ...properties.reduce(
            (propertyCollection, property) => {
              const targetProperty = this.scratchConfiguration && this.scratchConfiguration[property];

              if (typeof targetProperty !== 'undefined') {
                propertyCollection[property] = targetProperty;
              }
              return propertyCollection;
            },
            {},
          ),
        };

        this.props.configureStore.updateFormSettingsData(newFormSettingsData);

        // Update backend settings
        this.props.configureApis.postFormSettings(newFormSettingsData, this.props.jwtToken);
      }
    };
  }

  private generateOnCancel(property: string) {
    return () => {
      if (this.scratchConfiguration) {
        this.scratchConfiguration[property] = this.props.formSettingsInfo[property];
      }
    };
  }

  private generateRecommendedFormsView = () => {
    const recommendedItems = this.selectedIssueRequestTypeIds.map(requestType => (
      <Tag text={requestType.label} key={requestType.value} />
    ));

    return recommendedItems.length ? (
      recommendedItems
    ) : (
        <span>Not recommended</span>
      );
  }

  constructor(props: Readonly<FormSettingsProps>) {
    super(props);
    this.scratchConfiguration = this.props.formSettingsInfo;
    this.disposeScratch = reaction(
      () => this.props.formSettingsInfo,
      (formSettingsInfo) => {
        this.scratchConfiguration = { ...formSettingsInfo };
      },
    );
  }

  componentWillUnmount() {
    // Stop observing the formSettingsInfo so the scratch formSettingsInfo can be disposed
    this.disposeScratch();
  }

  render() {
    const { formSettingsInfo } = this.props;
    const scratchConfiguration = this.scratchConfiguration;

    return (
      <SettingsPanel>
        <h1>Form Settings</h1>
        {formSettingsInfo && scratchConfiguration && (
          <FormSettingsWrapper>
            <InlineEdit
              label="Name"
              editView={
                <SingleLineTextInput
                  isEditing={true}
                  isInitiallySelected={true}
                  value={scratchConfiguration.name || ''}
                  onChange={this.generateOnChange('name')}
                />
              }
              readView={
                <SingleLineTextInput
                  isEditing={false}
                  value={formSettingsInfo.name || ''}
                />
              }
              onConfirm={this.generateOnConfirm(['name'])}
              onCancel={this.generateOnCancel('name')}
            />

            <InlineEdit
              label="On submission"
              disableEditViewFieldBase={true}
              editView={
                <Select
                  options={this.portalLockOptions}
                  defaultValue={formSettingsInfo.portalLock ? this.portalLockOptions[0] : this.portalLockOptions[1]}
                  defaultMenuIsOpen={true}
                  autoFocus={true}
                  onChange={this.onPortalLockChange}
                />
              }
              readView={
                formSettingsInfo.portalLock ? (
                  <div className="view-select-padding">Lock form contents</div>
                ) : (
                    <div className="view-select-padding">Do not lock form contents</div>
                  )
              }
              onConfirm={this.generateOnConfirm(['portalLock'])}
              onCancel={this.generateOnCancel('portalLock')}
            />

            {
              !!this.requestTypeOptions.length &&
              <>
                <InlineEdit
                  label="Jira Service Desk customer portal"
                  disableEditViewFieldBase={true}
                  editView={
                    <Select
                      options={this.requestTypeOptions}
                      defaultValue={this.selectedPortalRequestTypeIds}
                      isMulti={true}
                      defaultMenuIsOpen={true}
                      autoFocus={true}
                      onChange={this.generateMultiOnChange('portalRequestTypeIds')}
                    />
                  }
                  readView={
                    <TagGroup>
                      {this.selectedPortalRequestTypeIds.map(requestType => (
                        <Tag text={requestType.label} key={requestType.value} />
                      ))}
                      {this.selectedPortalRequestTypeIds.length === 0 && <div>Not shown on Service Desk portal</div>}
                    </TagGroup>
                  }
                  onConfirm={this.generateOnConfirm(['portalRequestTypeIds'])}
                  onCancel={this.generateOnCancel('portalRequestTypeIds')}
                />
                <HelperMessage>Display this form when creating a request type via the Jira Service Desk customer portal.</HelperMessage>
              </>
            }

            <InlineEdit
              label="Recommended form"
              disableEditViewFieldBase={true}
              editView={
                <Select
                  options={this.recommendedFormOptions}
                  defaultValue={this.selectedIssueRequestTypeIds}
                  isMulti={true}
                  defaultMenuIsOpen={true}
                  autoFocus={true}
                  onChange={this.generateMultiOnChange('issueRequestTypeIds')}
                />
              }
              readView={
                <TagGroup>
                  {this.generateRecommendedFormsView()}
                </TagGroup>
              }
              onConfirm={this.generateOnConfirm(['issueRequestTypeIds'])}
              onCancel={this.generateOnCancel('issueRequestTypeIds')}
            />
            <HelperMessage>Include this form on a shortlist of forms that are recommended for this issue/request type.</HelperMessage>

            <InlineEdit
              label="Create issue"
              disableEditViewFieldBase={true}
              editView={
                <Select
                  options={this.riTypeOptions}
                  defaultValue={this.selectedNewIssueRiTypeIds}
                  isMulti={true}
                  defaultMenuIsOpen={true}
                  autoFocus={true}
                  onChange={this.onNewIssueRiTypeChange}
                />
              }
              readView={
                <TagGroup>
                  {this.selectedNewIssueRiTypeIds.map(requestType => (
                    <Tag text={requestType.label} key={requestType.value} />
                  ))}
                  {this.selectedNewIssueRiTypeIds.length === 0 && <div>Not used</div>}
                </TagGroup>
              }
              onConfirm={this.handleConfirmCreateIssueChanges}
              onCancel={this.handleCancelCreateIssueChanges}
            />
            <HelperMessage>Use this form to create issues within Jira, via the Issue Forms button.</HelperMessage>
          </FormSettingsWrapper>
        )}
      </SettingsPanel>
    );
  }
}
