// Atlaskit components
import { colors } from '@atlaskit/theme';

import styled from 'styled-components';

export const FormSettingsWrapper = styled.div`
  > div > div:last-child > div:first-child {
    border: dashed 1px ${colors.N40};
    margin-left: 8px;
  }
`;
