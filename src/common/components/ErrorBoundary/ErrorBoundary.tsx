// NPM Modules
import * as React from 'react';

// Atlaskit Components
import { default as AtlaskitBtn } from '@atlaskit/button';

// Utils
import { sendDebugMsg } from '../../utils/errorHandlingUtils';

// Modules
import {
  ErrorBoundaryProps,
  ErrorBoundaryState,
} from './ErrorBoundary-interfaces';

// Styles
import './ErrorBoundary.css';

export default class ErrorBoundary extends React.Component<ErrorBoundaryProps, ErrorBoundaryState> {
  static defaultProps = {
    showImmediately: false,
  };

  constructor(props) {
    super(props);

    this.state = {
      hasError: false,
    };
  }

  componentDidCatch(error) {
    sendDebugMsg({
      error,
      level: 'error',
      msg: 'Error boundary was triggered, see below for more details:',
    });

    this.setState({
      hasError: true,
    });
  }

  render() {
    const { hasError } = this.state;
    const {
      children,
      showImmediately,
    } = this.props;

    if (hasError || showImmediately) {
      return (
        <p className="proforma-ui__error-boundary">
          <span>A technical failure has occurred. Please contact</span>
          <AtlaskitBtn
            className="proforma-ui__error-boundary-link"
            spacing="compact"
            appearance="link"
            target="_blank"
            href="https://status.thinktilt.com/"
          >
            ThinkTilt Support
          </AtlaskitBtn>
          <span>for more information.</span>
        </p>
      );
    }

    return children;
  }
}
