import { ReactNode } from 'react';

export interface ErrorBoundaryProps {
  children: ReactNode;
  showImmediately?: boolean;
}

export interface ErrorBoundaryState {
  hasError: boolean;
}
