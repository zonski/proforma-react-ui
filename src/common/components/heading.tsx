import React from 'react';
import styled from 'styled-components';
import { ProformaLogoIcon } from './icons';

const HeadingWrapper = styled.div`
  display: flex;
  justify-content: flex-start;
  align-items: center;
  > h1 {
    margin: 0
  }
`;

const Toolbar = styled.div`
  margin-left: auto;
  text-align: right;
`;

export const PageHeading: React.SFC<{children?, actions?}> = (props) => (
  <HeadingWrapper>
    <ProformaLogoIcon/>
    <h1>{props.children ? props.children : ''}</h1>
    {props.actions ? <Toolbar>{(props.actions)}</Toolbar> : ''}
  </HeadingWrapper>
);