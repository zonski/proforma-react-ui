// NPM Modules
import * as React from 'react';

// Atlaskit Components
import Spinner from '@atlaskit/spinner';

// Error Boundary
import ErrorBoundary from '../ErrorBoundary/ErrorBoundary';

// Styles
import './LoadingSpinner.css';

// Models
interface LoadingSpinnerProps {
  message: string;
}

const LoadingSpinner: React.FC<LoadingSpinnerProps> = ({ message }) => (
  <ErrorBoundary>
    <div className="pf-builder__loading-spinner">
      <Spinner />
      <p className="pf-builder__loading-spinner-msg">{message}</p>
    </div>
  </ErrorBoundary>
);

export default LoadingSpinner;
