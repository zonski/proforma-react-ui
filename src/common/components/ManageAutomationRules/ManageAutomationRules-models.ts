import {
  AutomationRule,
  ReferenceData,
} from '../../../modulesFB/FormBuilderApp/components/Configure/Configure-models';

// Restructured Automation Rule Models
export enum TriggerTypes {
  submit = 'submit',
  transition = 'transition',
  workflowValidator = 'workflowValidator',
}

export interface AtlaskitBasicSelectOptionsListItem {
  label: string;
  value: any;
}

export interface PreventTransitionUniqueActionProps {
  isNotSubmitted: boolean;
  fromStatus?: string;
  toStatus?: string;
}

export type AtlaskitSelectValidationState = 'default' | 'error' | 'success';

export interface LookupAutomationRuleActionName {
  1: string;
  2: string;
  3: string;
}

export interface LookupAutomationRuleTriggerLabel {
  submit: string;
  transition: string;
  workflowValidator: string;
}

export enum RuleConditionTypes {
  allFormsSubmitted = 'allFormsSubmitted',
  matchingIssueStatus = 'matchingIssueStatus',
  previousIssueStatus = 'previousIssueStatus',
  isNotSubmitted = 'isNotSubmitted',
  toStatus = 'toStatus',
  fromStatus = 'fromStatus',
}

export interface LookupAutomationRuleConditionLabel {
  allFormsSubmitted: string;
  matchingIssueStatus: string;
  previousIssueStatus: string;
  isNotSubmitted: string;
  toStatus: string;
  fromStatus: string;
}

export interface StoreAutomationRuleRiTypeInfo {
  statusOptions: AtlaskitBasicSelectOptionsListItem[];
  issueTypeId?: string;
  label?: string;
  requestTypeId?: string;
  riTypeIconUrl?: string;
  value?: string;
}

export interface StoreRuleTriggerSubmit {
  label: string;
  value: TriggerTypes.submit;
}

export interface StoreRuleTriggerTransition {
  label: string;
  value: TriggerTypes.transition;
  status?: AtlaskitBasicSelectOptionsListItem;
}

export interface StoreRuleTriggerWorkflowValidator {
  label: string;
  value: TriggerTypes.workflowValidator;
}

export type StoreRuleTrigger = (
  StoreRuleTriggerSubmit |
  StoreRuleTriggerTransition |
  StoreRuleTriggerWorkflowValidator
);

export interface StoreRuleConditionAllSubmitted {
  label: string;
  value: RuleConditionTypes.allFormsSubmitted;
  otherFormsSubmitted: boolean;
}

export interface StoreRuleConditionMatchingStatus {
  label: string;
  value: RuleConditionTypes.matchingIssueStatus;
  status?: AtlaskitBasicSelectOptionsListItem;
}

export interface StoreRuleConditionPreviousStatus {
  label: string;
  value: RuleConditionTypes.previousIssueStatus;
  status?: AtlaskitBasicSelectOptionsListItem;
}

export interface StoreRuleConditionIsNotSubmitted {
  label: string;
  value: RuleConditionTypes.isNotSubmitted;
  isNotSubmitted: boolean;
}

export interface StoreRuleConditionFromStatus {
  label: string;
  value: RuleConditionTypes.fromStatus;
  status?: AtlaskitBasicSelectOptionsListItem;
}

export interface StoreRuleConditionToStatus {
  label: string;
  value: RuleConditionTypes.toStatus;
  status?: AtlaskitBasicSelectOptionsListItem;
}

export interface StoreRuleConditionBlank {
  label: string;
  value: '';
}

export type StoreRuleCondition = (
  StoreRuleConditionAllSubmitted |
  StoreRuleConditionMatchingStatus |
  StoreRuleConditionPreviousStatus |
  StoreRuleConditionIsNotSubmitted |
  StoreRuleConditionFromStatus |
  StoreRuleConditionToStatus |
  StoreRuleConditionBlank
);

export interface StoreRuleActionTransition {
  label: string;
  value: '1';
  status?: AtlaskitBasicSelectOptionsListItem;
}

export interface StoreRuleActionAddForm {
  label: string;
  value: '2';
  addForm?: AtlaskitBasicSelectOptionsListItem;
  author: string;
  doNotDuplicate: boolean;
  external?: boolean;
}

export interface StoreRuleActionPreventTransition {
  label: string;
  value: '3';
}

export type StoreRuleAction = (
  StoreRuleActionTransition |
  StoreRuleActionAddForm |
  StoreRuleActionPreventTransition
);

export interface StoreRefData {
  allTypes: StoreAutomationRuleRiTypeInfo;
  issueTypes: StoreAutomationRuleRiTypeInfo[];
  requestTypes: StoreAutomationRuleRiTypeInfo[];
  statuses: AtlaskitBasicSelectOptionsListItem[];
}

// Validated Automation Rule Models
export enum ValidationErrorTypes {
  incompleteCondition = 'incompleteCondition',
  invalidActionStatus = 'invalidActionStatus',
  invalidConditionStatus = 'invalidConditionStatus',
  invalidTriggerStatus = 'invalidTriggerStatus',
  noAction = 'noAction',
  noActionStatus = 'noActionStatus',
  noConditionStatus = 'noConditionStatus',
  noTrigger = 'noTrigger',
  noTriggerStatus = 'noTriggerStatus',
}

export interface ValidationError {
  errorType: ValidationErrorTypes;
  errorMsg: string;
}

export interface ValidAutomationRuleTriggerTransition extends StoreRuleTriggerTransition {
  status: AtlaskitBasicSelectOptionsListItem;
}

export type ValidAutomationRuleTrigger = (
  StoreRuleTriggerSubmit |
  ValidAutomationRuleTriggerTransition |
  StoreRuleTriggerWorkflowValidator
);

export interface ValidRuleConditionMatchingStatus extends StoreRuleConditionMatchingStatus {
  status: AtlaskitBasicSelectOptionsListItem;
}

export interface ValidRuleConditionPreviousStatus extends StoreRuleConditionPreviousStatus {
  status: AtlaskitBasicSelectOptionsListItem;
}

export interface ValidRuleConditionFromStatus extends StoreRuleConditionFromStatus {
  status: AtlaskitBasicSelectOptionsListItem;
}

export interface ValidRuleConditionToStatus extends StoreRuleConditionToStatus {
  status: AtlaskitBasicSelectOptionsListItem;
}


export type ValidRuleCondition = (
  StoreRuleConditionAllSubmitted |
  ValidRuleConditionMatchingStatus |
  ValidRuleConditionPreviousStatus |
  StoreRuleConditionIsNotSubmitted |
  ValidRuleConditionFromStatus |
  ValidRuleConditionToStatus
);

export interface ValidAutomationRuleActionAddForm extends StoreRuleActionAddForm {
  addForm: AtlaskitBasicSelectOptionsListItem;
}

export interface ValidAutomationRuleActionTransition extends StoreRuleActionTransition {
  status: AtlaskitBasicSelectOptionsListItem;
}

export type ValidAutomationRuleAction = (
  ValidAutomationRuleActionAddForm |
  ValidAutomationRuleActionTransition |
  StoreRuleActionPreventTransition
);

export interface ValidAutomationRule {
  id: string;
  formId: number;
  riTypeInfo: StoreAutomationRuleRiTypeInfo;
  trigger: ValidAutomationRuleTrigger;
  conditions: ValidRuleCondition[];
  action: ValidAutomationRuleAction;
}

// Store Models
export interface AutomationRuleMicroStore {
  // State
  conditions: StoreRuleCondition[];
  formId: number;
  id: string;
  isViewDroplistOpen: boolean;
  isSaving: boolean;
  hasUnsavedChanges: boolean;
  validationErrors: ValidationError[];
  riTypeInfo: StoreAutomationRuleRiTypeInfo;
  action?: StoreRuleAction;
  trigger?: StoreRuleTrigger;

  // Actions
  updateTrigger: (newTrigger?: StoreRuleTrigger) => void;
  updateRiTypeInfo: (newRiTypeInfo: StoreAutomationRuleRiTypeInfo) => void;
  updateConditions: (newConditions: StoreRuleCondition[]) => void;
  updateAction: (newAction: StoreRuleAction) => void;
  updateIsViewDroplistOpen: (newStatus: boolean) => void;
  updateIsSaving: (newStatus: boolean) => void;
  updateRuleId: (newId: string) => void;
  updateValidationErrors: (errorList: ValidationError[]) => void;
  updateHasUnsavedChanges: (newStatus: boolean) => void;
}

export interface ManageAutomationRulesStore {
  // State
  activeRuleId: string;
  isLoadingRules: boolean;
  projectFormName: string;
  projectFormId: string;
  refData: StoreRefData;
  ruleMicroStoreIds: string[];
  tempRuleMicroStore: AutomationRuleMicroStore;
  triggerOptions: StoreRuleTrigger[];
  rawAutomationRules: AutomationRule[];
  rawReferenceData: ReferenceData;

  // Actions
  updateTempRuleMicroStore: (newTempRuleMicroStore: AutomationRuleMicroStore) => void;
  updateActiveRuleId: (newRuleId: string) => void;
  updateRefData: (structuredReferenceData: StoreRefData) => void;
  updateTriggerOptions: (newTriggerOptions: StoreRuleTrigger[]) => void;
  updateRuleMicroStoreIds: (newRuleMicroStoreIds: string[]) => void;
  updateIsLoadingRules: (newStatus: boolean) => void;
  updateRawAutomationRules: (newAutomationRulesData: AutomationRule[]) => void;
  updateRawReferenceData: (newReferenceData: ReferenceData) => void;
}
