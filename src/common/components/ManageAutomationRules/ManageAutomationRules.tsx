// NPM Modules
import * as React from 'react';
import { inject, observer } from 'mobx-react';

// Error Boundary
import ErrorBoundary from '../ErrorBoundary/ErrorBoundary';

// Models
import {
  ConfigureApis,
} from '../../../modulesFB/FormBuilderApp/components/Configure/Configure-models';
import { ManageAutomationRulesStore } from './ManageAutomationRules-models';

// Components
import LoadingSpinner from '../LoadingSpinner/LoadingSpinner';
import {
  ViewAutomationRules,
  DesignAutomationRule,
} from './ManageAutomationRules-sfcs';

// Styles
import { SettingsPanel } from '../../../modulesFB/shared-styles';
import './ManageAutomationRules.css';

export interface ManageAutomationRulesProps {
  isEditOn: boolean;
  updateIsEditOnCallback: (newStatus: boolean) => void;
  configureApis: ConfigureApis;
  manageAutomationRulesStore: ManageAutomationRulesStore;
  doNotTrack: boolean;
  segmentWriteKey?: string;
  jwtToken?: string;
}

class ManageAutomationRules extends React.Component<ManageAutomationRulesProps, {}> {
  render() {
    // Destructure store to reduce boilerplate
    const {
      isEditOn,
      updateIsEditOnCallback,
      manageAutomationRulesStore,
      manageAutomationRulesStore: {
        activeRuleId,
        isLoadingRules,
        refData,
        ruleMicroStoreIds,
        triggerOptions,
      },
      configureApis,
      doNotTrack,
      segmentWriteKey,
      jwtToken,
    } = this.props;

    const canShowAppcues = (
      !doNotTrack &&
      !!segmentWriteKey
    );

    return (
      <ErrorBoundary>
        <SettingsPanel>
          {
            isLoadingRules &&
            <LoadingSpinner message="Loading Automation Rules..." />
          }
          {
            !isLoadingRules &&
            !isEditOn &&
            <ViewAutomationRules
              store={manageAutomationRulesStore}
              ruleMicroStoreIds={ruleMicroStoreIds}
              updateIsEditOnCallback={updateIsEditOnCallback}
              configureApis={configureApis}
              canShowAppcues={canShowAppcues}
              jwtToken={jwtToken}
            />
          }
          {
            !isLoadingRules &&
            isEditOn &&
            <DesignAutomationRule
              store={manageAutomationRulesStore}
              refData={refData}
              activeRuleMicroStoreId={activeRuleId}
              triggerOptions={triggerOptions}
              configureApis={configureApis}
              updateIsEditOnCallback={updateIsEditOnCallback}
              jwtToken={jwtToken}
            />
          }
        </SettingsPanel>
      </ErrorBoundary>
    );
  }
}

export default inject('manageAutomationRulesStore')(observer(ManageAutomationRules));
