import {
  AtlaskitBasicSelectOptionsListItem,
  LookupAutomationRuleActionName,
  LookupAutomationRuleConditionLabel,
  LookupAutomationRuleTriggerLabel,
} from './ManageAutomationRules-models';

export const automationRuleActionNamesLookup: LookupAutomationRuleActionName = {
  1: 'transition',
  2: 'add form',
  3: 'prevent transition',
};

export const automationRuleTriggerLabelLookup: LookupAutomationRuleTriggerLabel = {
  submit: 'this form is submitted',
  transition: 'the status changes to',
  workflowValidator: 'a Jira workflow validation is performed',
};

export const automationRuleConditionLabelLookup: LookupAutomationRuleConditionLabel = {
  allFormsSubmitted: 'all forms are submitted',
  matchingIssueStatus: 'the issue status is',
  previousIssueStatus: 'the issue status was',
  fromStatus: 'the issue is changing from',
  toStatus: 'the issue is changing to',
  isNotSubmitted: 'this form is not attached to the issue and is not submitted',
};

export const ruleSentencePhrases = {
  riType: {
    singleRiType: 'For ',
  },
  condition: {
    allSubmitted: 'all other forms are submitted',
    notSubmitted: 'this form is not attached to the issue and not submitted',
  },
  action: {
    transitionTo: 'change the issue status to',
  },
};

export const doNotDuplicateOptions: AtlaskitBasicSelectOptionsListItem[] = [
  {
    label: 'if this form is not already attached',
    value: 'prevent',
  },
  {
    label: 'and allow duplicates of this form to be added',
    value: '',
  },
];

export const internalExternalOptions: AtlaskitBasicSelectOptionsListItem[] = [
  {
    label: 'and set form to INTERNAL',
    value: false,
  },
  {
    label: 'and set form to EXTERNAL',
    value: true,
  },
];
