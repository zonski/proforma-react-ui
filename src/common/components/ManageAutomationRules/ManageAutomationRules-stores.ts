import { action, observable } from 'mobx';
// Utils
import { sendDebugMsg } from '../../utils/errorHandlingUtils';
// Component Constants
import {
  automationRuleActionNamesLookup,
  automationRuleConditionLabelLookup,
  automationRuleTriggerLabelLookup,
} from './ManageAutomationRules-constants';
// Models
import {
  AtlaskitBasicSelectOptionsListItem,
  AutomationRuleMicroStore,
  LookupAutomationRuleActionName,
  ManageAutomationRulesStore,
  RuleConditionTypes,
  StoreAutomationRuleRiTypeInfo,
  StoreRefData,
  StoreRuleAction,
  StoreRuleCondition,
  StoreRuleTrigger,
  TriggerTypes,
  ValidationError,
} from './ManageAutomationRules-models';

import {
  ActionId,
  AutomationRule,
  AutomationRuleAction,
  ReferenceData,
} from '../../../modulesFB/FormBuilderApp/components/Configure/Configure-models';

import { createEmptyRuleMicroStore, updateManageAutmationRulesStoreData } from './ManageAutomationRules-logic';

export class AutomationRuleLiveMicroStore implements AutomationRuleMicroStore {
  public formId: number;

  @observable
  public id: string;

  @observable
  public isViewDroplistOpen: boolean;

  @observable
  public isSaving: boolean;

  @observable
  public hasUnsavedChanges: boolean;

  @observable
  public validationErrors: ValidationError[];

  @observable
  public riTypeInfo: StoreAutomationRuleRiTypeInfo;

  @observable
  public conditions: StoreRuleCondition[];

  @observable
  public trigger?: StoreRuleTrigger;

  @observable
  public action?: StoreRuleAction;

  /**
   * Used to extract the type data from a list of available types
   */
  private structureTypeData(typesList: StoreAutomationRuleRiTypeInfo[], typeId: string) {
    if (!typesList.length) {
      return undefined;
    }

    const [typeData] = typesList.filter(typeItem => typeItem.value === typeId);

    if (!typeData) {
      sendDebugMsg({
        level: 'error',
        msg: `No matching type data was found for the provided typeId: ${typeId}`,
        error: new Error('The riTypeId did not have a matching type, check automation rule data and reference data are not broken'),
      });

      return undefined;
    }

    return typeData;
  }

  /**
   * Used to extract any request / issue type data for the specified rule
   */
  private extractRiTypeData(
    issueTypes: StoreAutomationRuleRiTypeInfo[],
    requestTypes: StoreAutomationRuleRiTypeInfo[],
    issueTypeId?: string,
    requestTypeId?: string,
  ) {
    if (typeof issueTypeId === 'undefined' && typeof requestTypeId === 'undefined') {
      // No request / issue type data to extract for this rule
      return undefined;
    }

    if (typeof requestTypeId !== 'undefined') {
      // RequestType data gets priority over issueType data if a requestTypeId was provided
      return this.structureTypeData(requestTypes, requestTypeId);
    }

    if (typeof issueTypeId !== 'undefined') {
      return this.structureTypeData(issueTypes, issueTypeId);
    }
  }

  private extractStatusName(statusList: AtlaskitBasicSelectOptionsListItem[], statusId?: string) {
    if (typeof statusId === 'undefined' || !statusList.length) {
      return undefined;
    }

    // Status ID exists so find a matching name to be returned
    const [statusData] = statusList.filter(status => status.value === statusId);

    if (!statusData) {
      sendDebugMsg({
        level: 'error',
        msg: `No matching status data was found for the provided statusId: ${statusId}`,
        error: new Error('The statusId did not have a matching data set, check automation rule data and reference data are not broken'),
      });

      return undefined;
    }

    return statusData.label;
  }

  /**
   * Combines a trigger item with reference labels for display in the UI
   */
  private insertTriggerRefData(
    triggerType: string,
    status?: string,
    statusName?: string,
  ) {
    if (triggerType === TriggerTypes.submit) {
      const submitTrigger: StoreRuleTrigger = {
        label: automationRuleTriggerLabelLookup.submit,
        value: TriggerTypes.submit,
      };

      return submitTrigger;
    }

    if (triggerType === TriggerTypes.transition && status) {
      const transitionTrigger: StoreRuleTrigger = {
        label: automationRuleTriggerLabelLookup.transition,
        value: TriggerTypes.transition,
        status: statusName ? {
          label: statusName,
          value: status,
        } : undefined,
      };

      return transitionTrigger;
    }

    if (triggerType === TriggerTypes.workflowValidator) {
      const transitionTrigger: StoreRuleTrigger = {
        label: automationRuleTriggerLabelLookup.workflowValidator,
        value: TriggerTypes.workflowValidator,
      };

      return transitionTrigger;
    }

    // Unknown triggerType likely for a new rule
    return undefined;
  }

  /**
   * Combines a action item with reference labels to ids for display in the UI
   */
  private insertActionRefData(
    action: AutomationRuleAction,
    statuses: AtlaskitBasicSelectOptionsListItem[],
    actionNamesLookup: LookupAutomationRuleActionName,
    projectFormName: string,
  ) {
    if (action.id === ActionId.transition) {
      // Return the enriched version of the Transition Action Object
      const statusName = this.extractStatusName(statuses, action.to);

      const transitionAction: StoreRuleAction = {
        label: actionNamesLookup[action.id],
        value: '1',
        status: {
          label: statusName || `Unknown Status Id(${action.to})`,
          value: action.to,
        },
      };

      return transitionAction;
    }

    if (action.id === ActionId.addform) {
      // Return the enriched version of the AddForm Action Object
      const addFormAction: StoreRuleAction = {
        label: actionNamesLookup[action.id],
        value: '2',
        doNotDuplicate: action.doNotDuplicate,
        external: action.external,
        author: action.author,
        addForm: {
          label: projectFormName,
          value: action.formId.toString(),
        },
      };

      return addFormAction;
    }

    if (action.id === ActionId.preventTransition) {
      const addFormAction: StoreRuleAction = {
        label: actionNamesLookup[action.id],
        value: '3',
      };

      return addFormAction;
    }

    // Unknown or invalid action Id is provided so treat as blank
    return undefined;
  }

  constructor(
    ruleData: AutomationRule,
    refData: StoreRefData,
    projectFormName: string,
    projectFormId: string,
  ) {
    // Destructure ruleData
    const {
      id,
      trigger,
      conditions: {
        formId,
        otherFormsSubmitted,
        status,
        status2,
        issueTypeId,
        requestTypeId,
      },
      action,
    } = ruleData;

    // Destructure refData
    const {
      allTypes,
      statuses,
      issueTypes,
      requestTypes,
    } = refData;

    // Extract ref values
    const riTypeData = this.extractRiTypeData(issueTypes, requestTypes, issueTypeId, requestTypeId);

    // Extract the Condition statusName ref from refData.statuses
    const conditionStatusName = this.extractStatusName(statuses, status);

    // Extract the Action toStatus - statusName ref from refData.statuses
    const actionToStatusName = action.id === ActionId.preventTransition && this.extractStatusName(statuses, action.toStatus);

    // Extract the Action fromStatus - statusName ref from refData.statuses
    const actionFromStatusName = action.id === ActionId.preventTransition && this.extractStatusName(statuses, action.fromStatus);

    // Extract the Trigger statusName ref from refData.statuses
    const triggerStatusName = this.extractStatusName(statuses, status2);

    // Assign initial state
    this.id = id;
    this.formId = formId || Number(projectFormId);
    this.riTypeInfo = (typeof riTypeData !== 'undefined') ? {
      statusOptions: riTypeData.statusOptions,
      ...riTypeData,
    } : allTypes;

    this.trigger = this.insertTriggerRefData(
      trigger.type,
      status2,
      triggerStatusName,
    );

    this.conditions = [];

    if (otherFormsSubmitted) {
      this.conditions.push({
        otherFormsSubmitted,
        label: automationRuleConditionLabelLookup.allFormsSubmitted,
        value: RuleConditionTypes.allFormsSubmitted,
      });
    }

    if (action.id === ActionId.preventTransition && action.isNotSubmitted) {
      this.conditions.push({
        isNotSubmitted: action.isNotSubmitted,
        label: automationRuleConditionLabelLookup.isNotSubmitted,
        value: RuleConditionTypes.isNotSubmitted,
      });
    }

    if (conditionStatusName && trigger.type === TriggerTypes.submit) {
      this.conditions.push({
        label: automationRuleConditionLabelLookup.matchingIssueStatus,
        value: RuleConditionTypes.matchingIssueStatus,
        status: status ? {
          label: conditionStatusName,
          value: status,
        } : undefined,
      });
    }

    if (conditionStatusName && trigger.type === TriggerTypes.transition) {
      this.conditions.push({
        label: automationRuleConditionLabelLookup.previousIssueStatus,
        value: RuleConditionTypes.previousIssueStatus,
        status: status ? {
          label: conditionStatusName,
          value: status,
        } : undefined,
      });
    }

    if (actionFromStatusName && action.id === ActionId.preventTransition && action.fromStatus) {
      this.conditions.push({
        label: automationRuleConditionLabelLookup.fromStatus,
        value: RuleConditionTypes.fromStatus,
        status: {
          label: actionFromStatusName,
          value: action.fromStatus,
        },
      });
    }

    if (actionToStatusName && action.id === ActionId.preventTransition && action.toStatus) {
      this.conditions.push({
        label: automationRuleConditionLabelLookup.toStatus,
        value: RuleConditionTypes.toStatus,
        status: {
          label: actionToStatusName,
          value: action.toStatus,
        },
      });
    }

    this.action = this.insertActionRefData(
      action,
      statuses,
      automationRuleActionNamesLookup,
      projectFormName,
    );

    // Configure initial UI settings
    this.isViewDroplistOpen = false;
    this.isSaving = false;
    this.validationErrors = [];
    this.hasUnsavedChanges = false;
  }

  @action
  public updateTrigger = (newTrigger?: StoreRuleTrigger) => {
    this.trigger = newTrigger;

    if (!this.hasUnsavedChanges) {
      this.updateHasUnsavedChanges(true);
    }
  }

  @action
  public updateRiTypeInfo = (newRiTypeInfo: StoreAutomationRuleRiTypeInfo) => {
    this.riTypeInfo = newRiTypeInfo;

    if (!this.hasUnsavedChanges) {
      this.updateHasUnsavedChanges(true);
    }
  }

  @action
  public updateConditions = (newConditions: StoreRuleCondition[]) => {
    this.conditions = newConditions;

    if (!this.hasUnsavedChanges) {
      this.updateHasUnsavedChanges(true);
    }
  }

  @action
  public updateAction = (newAction: StoreRuleAction) => {
    this.action = newAction;

    if (!this.hasUnsavedChanges) {
      this.updateHasUnsavedChanges(true);
    }
  }

  // UI Actions
  @action
  public updateIsViewDroplistOpen = (newStatus: boolean) => {
    this.isViewDroplistOpen = newStatus;
  }

  @action
  public updateIsSaving = (newStatus: boolean) => {
    this.isSaving = newStatus;
  }

  @action
  public updateRuleId = (newId: string) => {
    this.id = newId;
  }

  @action
  public updateValidationErrors = (errorList: ValidationError[]) => {
    this.validationErrors = errorList;
  }

  @action
  public updateHasUnsavedChanges = (newStatus: boolean) => {
    this.hasUnsavedChanges = newStatus;
  }
}

export class ManageAutomationRulesLiveStore implements ManageAutomationRulesStore {
  public projectFormName: string;
  public projectFormId: string;

  @observable
  public activeRuleId: string;

  @observable
  public isLoadingRules: boolean;

  @observable
  public refData: StoreRefData;

  @observable
  public ruleMicroStoreIds: string[];

  @observable
  public tempRuleMicroStore: AutomationRuleMicroStore;

  @observable
  public triggerOptions: StoreRuleTrigger[];

  @observable
  public rawAutomationRules: AutomationRule[];

  @observable
  public rawReferenceData: ReferenceData;

  constructor(
    projectFormId: string,
    projectFormName: string,
    rawReferenceData: ReferenceData,
    rawAutomationRules: AutomationRule[],
  ) {
    this.activeRuleId = 'tempRuleMicroStore';
    this.isLoadingRules = true;
    this.ruleMicroStoreIds = [];
    this.triggerOptions = [];
    this.projectFormName = projectFormName;
    this.projectFormId = projectFormId;

    // Muffle the screams of a limited Typescript compiler failing to see these values are upadted via methods in this constructor
    this.tempRuleMicroStore = {} as AutomationRuleMicroStore;
    this.rawAutomationRules = [];
    this.rawReferenceData = {} as ReferenceData;

    this.refData = {
      allTypes: { statusOptions: [] },
      issueTypes: [],
      requestTypes: [],
      statuses: [],
    };

    // Set initial value of tempRuleMicroStore to an empty store
    this.updateTempRuleMicroStore(
      createEmptyRuleMicroStore(
        this.refData,
        this.projectFormName,
        this.projectFormId,
      )
    );

    // Update store rules with data from the backend
    updateManageAutmationRulesStoreData(
      this,
      rawAutomationRules,
      rawReferenceData,
      projectFormName,
      projectFormId,
    );
  }

  @action
  public updateTempRuleMicroStore = (newTempRuleMicroStore: AutomationRuleMicroStore) => {
    this.tempRuleMicroStore = newTempRuleMicroStore;
  }

  @action
  public updateActiveRuleId = (newRuleId: string) => {
    this.activeRuleId = newRuleId;
  }

  @action
  public updateRefData = (structuredReferenceData: StoreRefData) => {
    this.refData = structuredReferenceData;
  }

  @action
  public updateTriggerOptions = (newTriggerOptions: StoreRuleTrigger[]) => {
    this.triggerOptions = newTriggerOptions;
  }

  @action
  public updateRuleMicroStoreIds = (newRuleMicroStoreIds: string[]) => {
    this.ruleMicroStoreIds = newRuleMicroStoreIds;
  }

  @action
  public updateIsLoadingRules = (newStatus: boolean) => {
    this.isLoadingRules = newStatus;
  }

  @action
  public updateRawAutomationRules = (newAutomationRulesData: AutomationRule[]) => {
    this.rawAutomationRules = newAutomationRulesData;
  }

  @action
  public updateRawReferenceData = (newReferenceData: ReferenceData) => {
    this.rawReferenceData = newReferenceData;
  }
}
