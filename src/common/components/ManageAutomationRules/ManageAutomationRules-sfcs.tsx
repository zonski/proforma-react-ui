// NPM Modules
import * as React from 'react';
import { observer } from 'mobx-react';
// Atlaskit Components
import { default as AtlaskitBtn } from '@atlaskit/button';
import { default as Select } from '@atlaskit/select';
import { default as Droplist } from '@atlaskit/droplist';
import { default as Item } from '@atlaskit/item';
import { ErrorMessage } from '@atlaskit/form';
// Atlaskit Icons
import { default as EditorMoreIcon } from '@atlaskit/icon/glyph/editor/more';
import { default as TrashIcon } from '@atlaskit/icon/glyph/trash';
// Utils
import { callAppcuesShow } from '../../utils/AppcuesUtils';
// Error Boundary
import ErrorBoundary from '../ErrorBoundary/ErrorBoundary';
// Styles
import { AtlaskitBtnGroup } from '../../../modulesFB/shared-styles';
// Constants
import {
  automationRuleActionNamesLookup,
  automationRuleConditionLabelLookup,
  doNotDuplicateOptions,
  internalExternalOptions,
  ruleSentencePhrases,
} from './ManageAutomationRules-constants';
// Logic
import * as logic from './ManageAutomationRules-logic';
// Models
import {
  AtlaskitBasicSelectOptionsListItem,
  AtlaskitSelectValidationState,
  AutomationRuleMicroStore,
  ManageAutomationRulesStore,
  RuleConditionTypes,
  StoreAutomationRuleRiTypeInfo,
  StoreRefData,
  StoreRuleAction,
  StoreRuleActionAddForm,
  StoreRuleActionTransition,
  StoreRuleCondition,
  StoreRuleConditionFromStatus,
  StoreRuleConditionMatchingStatus,
  StoreRuleConditionPreviousStatus,
  StoreRuleConditionToStatus,
  StoreRuleTrigger,
  TriggerTypes,
  ValidationError,
  ValidationErrorTypes,
} from './ManageAutomationRules-models';

import { ConfigureApis, } from '../../../modulesFB/FormBuilderApp/components/Configure/Configure-models';

interface AutomationRuleHeadingProps {
  showExamplesBtn?: boolean;
  examplesBtnOnClick?: () => void;
  addRuleOnClick?: () => void;
}

export const AutomationRuleHeading = ({
  showExamplesBtn,
  examplesBtnOnClick,
  addRuleOnClick,
}: AutomationRuleHeadingProps) => (
    <ErrorBoundary>
      <div className="pf-builder__automation-rules-heading">
        <h2>Form Automation</h2>
        <AtlaskitBtnGroup>
          {
            false && showExamplesBtn &&
            typeof examplesBtnOnClick !== 'undefined' &&
            <AtlaskitBtn
              onClick={examplesBtnOnClick}>
              Examples
          </AtlaskitBtn>
          }
          {
            typeof addRuleOnClick !== 'undefined' &&
            <AtlaskitBtn onClick={addRuleOnClick}>
              Add Rule
          </AtlaskitBtn>
          }
        </AtlaskitBtnGroup>
      </div>
      {
        typeof addRuleOnClick !== 'undefined' &&
        <>
          <p>
            Form automation rules do repetitive tasks for you.
            Build rules to automatically change the status when a form is submitted, add a form,
            or prevent status changes unless a form is submitted.
        </p>
          <h4>Rules for this form</h4>
        </>
      }
    </ErrorBoundary>
  );

export const WorkflowValidatorInfo = () => (
  <ErrorBoundary>
    <p>There are three types of ProForma validators that can be added to your Jira workflows. These must be configured by a Jira administrator.</p>
    <p>The three ProForma validators that can be added are:</p>
    <ul>
        <li>At least one form is attached to an issue</li>
        <li>All forms attached to an issue are submitted </li>
        <li>A specific form is attached (and submitted)</li>
    </ul>
    <p>The third validator must also be <strong>configured here</strong>, so that Jira knows which specific form to check.</p>
    <AtlaskitBtn
      appearance="link"
      href="https://thinktilt.atlassian.net/wiki/x/QICfG"
      target="_blank"
    >
      Learn more about workflow validation
    </AtlaskitBtn>
  </ErrorBoundary >
);

interface AutomationRuleTypeInfoProps {
  issueTypeId?: string;
  requestTypeId?: string;
  riTypeName?: string;
}

export const AutomationRuleTypeInfo = ({ issueTypeId, requestTypeId, riTypeName }: AutomationRuleTypeInfoProps) => (
  <ErrorBoundary>
    {
      (typeof issueTypeId !== 'undefined' || typeof requestTypeId !== 'undefined') &&
      <>
        <>{ruleSentencePhrases.riType.singleRiType}</>
        <strong>{riTypeName} </strong>
        <>{(typeof requestTypeId !== 'undefined') ? 'requests, ' : 'issues, '}</>
      </>
    }
  </ErrorBoundary>
);

interface AutomationRuleTriggerInfoProps {
  appliesToAllTypes: boolean;
  trigger?: StoreRuleTrigger;
}

export const AutomationRuleTriggerInfo = ({
  trigger,
  appliesToAllTypes,
}: AutomationRuleTriggerInfoProps) => {
  return (
    <ErrorBoundary>
      <>{appliesToAllTypes ? 'When ' : 'when '} {trigger && trigger.label}</>
      {
        trigger &&
        trigger.value === 'transition' &&
        <strong> {trigger.status && trigger.status.label}</strong>
      }
      <>, </>
    </ErrorBoundary>
  );
};

interface AutomationRuleConditionInfoProps {
  conditionItemData: StoreRuleCondition;
  isFirst: boolean;
  isLast?: boolean;
}

export const AutomationRuleConditionInfo = ({
  conditionItemData,
  isFirst,
  isLast,
}: AutomationRuleConditionInfoProps) => {
  // Type guard to distinguish if a condition will have a status property
  const conditionNeedsStatus = (item: StoreRuleCondition): item is
    StoreRuleConditionMatchingStatus |
    StoreRuleConditionPreviousStatus |
    StoreRuleConditionFromStatus |
    StoreRuleConditionToStatus => (
      item.value === RuleConditionTypes.matchingIssueStatus ||
      item.value === RuleConditionTypes.previousIssueStatus ||
      item.value === RuleConditionTypes.fromStatus ||
      item.value === RuleConditionTypes.toStatus
    );

  const createTransitionDesc = (condition) => {
    if (!conditionNeedsStatus(condition)) {
      return '';
    }

    const prefix = 'the issue status';

    if (condition.value === RuleConditionTypes.previousIssueStatus) {
      return `${prefix} was`;
    }

    if (condition.value === RuleConditionTypes.matchingIssueStatus) {
      return `${prefix} is`;
    }

    if (condition.value === RuleConditionTypes.fromStatus) {
      return `${prefix} is changing from`;
    }

    if (condition.value === RuleConditionTypes.toStatus) {
      return `${prefix} is changing to`;
    }

    // Transition needs a status but no match was found.. should never happen but typescript needed this
    return '';
  };

  const transitionDescription = createTransitionDesc(conditionItemData);

  return (
    <ErrorBoundary>
      <>{isFirst ? 'if ' : ', and '}</>
      {
        conditionItemData.value === RuleConditionTypes.allFormsSubmitted &&
        <>
          {ruleSentencePhrases.condition.allSubmitted}
        </>
      }
      {
        conditionItemData.value === RuleConditionTypes.isNotSubmitted &&
        <>
          {ruleSentencePhrases.condition.notSubmitted}
        </>
      }
      {
        !!transitionDescription.length &&
        <>
          <>
            {transitionDescription}
          </>
          {
            conditionNeedsStatus(conditionItemData) &&
            conditionItemData.status &&
            <ErrorBoundary>
              <strong> {conditionItemData.status.label}</strong>
            </ErrorBoundary>
          }
        </>
      }
      {
        isLast &&
        <>, </>
      }
    </ErrorBoundary>
  );
};

interface AutomationRuleActionInfoProps {
  actionItemData: StoreRuleAction;
  hasIsNotSubmittedCondition: boolean;
  isFirst: boolean;
  isLast: boolean;
}

export const AutomationRuleActionInfo = ({
  actionItemData,
  hasIsNotSubmittedCondition,
  isFirst,
  isLast,
}: AutomationRuleActionInfoProps) => {
  return (
    <ErrorBoundary>
      <>{isFirst ? '' : ', and '}</>
      {
        actionItemData.value === '1' &&
        actionItemData.status &&
        <>
          <>
            {ruleSentencePhrases.action.transitionTo}
          </>
          <strong> {actionItemData.status.label}</strong>
        </>
      }
      {
        actionItemData.value === '2' &&
        <>
          then <strong>add this form </strong>
          <>{`to the issue${actionItemData.doNotDuplicate ? ' if it is not already attached' : ''}`}</>
          {
            !!actionItemData.external &&
            <>, and set it to <strong>External</strong></>
          }
        </>
      }
      {
        actionItemData.value === '3' &&
        <>
          <>then </>
          {
            !hasIsNotSubmittedCondition &&
            <> unless this form is attached, </>
          }
          <strong>prevent the status change</strong>
        </>
      }
      {
        isLast &&
        <>.</>
      }
    </ErrorBoundary>
  );
};

interface AutomationRuleItemProps {
  store: ManageAutomationRulesStore;
  ruleMicroStoreId: string;
  updateIsEditOnCallback: (newStatus: boolean) => void;
  configureApis: ConfigureApis;
  jwtToken?: string;
}

export const AutomationRuleItem = observer(({
  store,
  ruleMicroStoreId,
  updateIsEditOnCallback,
  configureApis,
  jwtToken,
}: AutomationRuleItemProps) => {
  const microStore: AutomationRuleMicroStore = store[ruleMicroStoreId];

  // Destructure microStore values
  const {
    isViewDroplistOpen,
    riTypeInfo: {
      issueTypeId,
      requestTypeId,
      label: riTypeName,
    },
    trigger,
    conditions,
    action: actionItemData,
  } = microStore;

  const hasIsNotSubmittedCondition = conditions.some(
    condition => condition.value === RuleConditionTypes.isNotSubmitted,
  );

  const conditionItems = conditions.map((conditionItem, index) => (
    <AutomationRuleConditionInfo
      key={conditionItem.value}
      conditionItemData={conditionItem}
      isFirst={index === 0}
      isLast={index === conditions.length - 1}
    />
  ));

  const actionItem = actionItemData ? (
    <AutomationRuleActionInfo
      key={actionItemData.value}
      hasIsNotSubmittedCondition={hasIsNotSubmittedCondition}
      actionItemData={actionItemData}
      isFirst={true}
      isLast={true}
    />
  ) : '';

  const appliesToAllTypes = (
    typeof issueTypeId === 'undefined' &&
    typeof requestTypeId === 'undefined'
  );

  const callHandleAutomationRuleOnClick = () => {
    logic.handleAutomationRuleOnClick(store, ruleMicroStoreId, updateIsEditOnCallback);
  };

  // Droplist event handlers
  const preventDefaultWrapper = (e) => {
    // This is to catch the droplistOnChange event so it doesn't trigger click eventHandlers on parent nodes
    e.preventDefault();
    e.stopPropagation();
  };

  const handleDroplistOnChange = () => {
    microStore.updateIsViewDroplistOpen(false);
  };

  const callHandleRuleItemDroplistOnClick = () => {
    logic.handleRuleItemDroplistOnClick(microStore, isViewDroplistOpen);
  };

  const callHandleDeleteRuleOnClick = () => {
    logic.handleDeleteRuleOnclick(
      store,
      microStore,
      configureApis,
      updateIsEditOnCallback,
      jwtToken,
    );
  };

  return (
    <div
      className="pf-builder__automation-rule-item"
      tabIndex={-1}
      onClick={callHandleAutomationRuleOnClick}
    >
      {logic.calcRuleIcon((trigger && trigger.value) || 'not-specified')}

      <div className="pf-builder__automation-rule-item-text">
        <AutomationRuleTypeInfo
          requestTypeId={requestTypeId}
          issueTypeId={issueTypeId}
          riTypeName={riTypeName}
        />
        <AutomationRuleTriggerInfo
          appliesToAllTypes={appliesToAllTypes}
          trigger={trigger}
        />
        {conditionItems}
        {actionItem}
      </div>

      <div className="pf-builder__automation-rule-item-droplist-wrapper" onClick={preventDefaultWrapper}>
        <Droplist
          appearance="default"
          position="right top"
          isOpen={isViewDroplistOpen}
          onOpenChange={handleDroplistOnChange}
          trigger={<AtlaskitBtn iconBefore={<EditorMoreIcon label="Automation Rule Actions" />} />}
          onClick={callHandleRuleItemDroplistOnClick}
        >
          <Item onClick={callHandleAutomationRuleOnClick}>Edit</Item>
          <Item onClick={callHandleDeleteRuleOnClick}>Delete</Item>
        </Droplist>
      </div>
    </div>
  );
});

interface ViewAutomationRulesProps {
  store: ManageAutomationRulesStore;
  ruleMicroStoreIds: string[];
  updateIsEditOnCallback: (newStatus: boolean) => void;
  configureApis: ConfigureApis;
  canShowAppcues: boolean;
  jwtToken?: string;
}

export const ViewAutomationRules = observer(({
  store,
  ruleMicroStoreIds,
  updateIsEditOnCallback,
  configureApis,
  canShowAppcues,
  jwtToken,
}: ViewAutomationRulesProps) => {
  const callHandleAddRuleOnClick = () => {
    logic.handleAddRuleOnclick(store, updateIsEditOnCallback);
  };

  const handleExamplesBtnOnClick = () => {
    callAppcuesShow('-LaO7xirQv4sFj7AmDNE');
  };

  return (
    <ErrorBoundary>
      <div className="pf-builder__view-automation-rules">
        <AutomationRuleHeading
          showExamplesBtn={canShowAppcues}
          examplesBtnOnClick={handleExamplesBtnOnClick}
          addRuleOnClick={callHandleAddRuleOnClick}
        />
        <div className="pf-builder__automation-rules-content">
          {
            ruleMicroStoreIds.length ? (
              logic.generateExistingRules(
                store,
                ruleMicroStoreIds,
                updateIsEditOnCallback,
                configureApis,
                jwtToken,
              )
            ) : (
                <>No rules exist for this form.</>
              )
          }
        </div>
      </div>
    </ErrorBoundary>
  );
});

interface DesignRuleTriggerProps {
  microStore: AutomationRuleMicroStore;
  triggerOptions: StoreRuleTrigger[];
  statusOptions: AtlaskitBasicSelectOptionsListItem[];
  validationErrors: ValidationError[];
}

export const DesignRuleTrigger = observer(({
  microStore,
  triggerOptions,
  statusOptions,
  validationErrors,
}: DesignRuleTriggerProps) => {
  const [selectedTriggerOption] = triggerOptions.filter(triggerOption => (
    microStore.trigger &&
    triggerOption.value === microStore.trigger.value
  ));

  const selectedTriggerStatus = (
    microStore.trigger &&
    microStore.trigger.value === TriggerTypes.transition &&
    microStore.trigger.status
  );

  const triggerValidationMsgs: string[] = logic.calcValidationMsgs(
    validationErrors,
    [ValidationErrorTypes.noTrigger],
  );

  const triggerValidationState: AtlaskitSelectValidationState = triggerValidationMsgs.length ? 'error' : 'default';

  const triggerStatusValidationMsgs: string[] = logic.calcValidationMsgs(
    validationErrors,
    [
      ValidationErrorTypes.noTriggerStatus,
      ValidationErrorTypes.invalidTriggerStatus,
    ],
  );

  const triggerStatusValidationState: AtlaskitSelectValidationState = triggerStatusValidationMsgs.length ? 'error' : 'default';


  const handleTriggerOnChange = (e: StoreRuleTrigger) => {
    if (selectedTriggerOption && selectedTriggerOption.value === e.value) {
      // PF-857 Nothing changed so no further action is required
      return;
    }

    microStore.updateTrigger(e);

    // Clear applied conditions as some may no longer be valid
    microStore.updateConditions([]);

    // Can be removed once an action is not tightly coupled to a trigger
    if (e.value === TriggerTypes.submit) {
      microStore.updateAction({
        label: automationRuleActionNamesLookup[1],
        value: '1',
        status: undefined,
      });

      return;
    }

    if (e.value === TriggerTypes.transition) {
      microStore.updateAction({
        label: automationRuleActionNamesLookup[2],
        value: '2',
        addForm: undefined,
        author: '',
        doNotDuplicate: false,
      });
    }

    if (e.value === TriggerTypes.workflowValidator) {
      microStore.updateAction({
        label: automationRuleActionNamesLookup[3],
        value: '3',
      });
    }
  };

  const handleTriggerStatusOnChange = (e) => {
    const newTriggerValue = {
      ...selectedTriggerOption,
      status: e,
    };

    microStore.updateTrigger(newTriggerValue);
  };

  return (
    <ErrorBoundary>
      <Select
        value={selectedTriggerOption}
        options={triggerOptions}
        onChange={handleTriggerOnChange}
        validationState={triggerValidationState}
      />
      {
        !!triggerValidationMsgs.length &&
        <ErrorMessage>{triggerValidationMsgs}</ErrorMessage>
      }
      {
        selectedTriggerOption &&
        selectedTriggerOption.value === TriggerTypes.transition &&
        <div className="pf-builder__design-rule-trigger-status-wrapper">
          <Select
            placeholder="Select status"
            value={selectedTriggerStatus}
            options={statusOptions}
            onChange={handleTriggerStatusOnChange}
            validationState={triggerStatusValidationState}
          />
          {
            !!triggerStatusValidationMsgs.length &&
            <ErrorMessage>{triggerStatusValidationMsgs}</ErrorMessage>
          }
        </div>
      }
      {
        selectedTriggerOption &&
        selectedTriggerOption.value === TriggerTypes.workflowValidator &&
        <WorkflowValidatorInfo />
      }
    </ErrorBoundary>
  );

});

interface DesignRuleConditionItemProps {
  microStore: AutomationRuleMicroStore;
  allConditions: StoreRuleCondition[];
  chosenCondition: StoreRuleCondition;
  statusOptions: AtlaskitBasicSelectOptionsListItem[];
  validationErrors: ValidationError[];
  isFirst?: boolean;
}

export const DesignRuleConditionItem = observer(({
  microStore,
  allConditions,
  chosenCondition,
  statusOptions,
  validationErrors,
  isFirst,
}: DesignRuleConditionItemProps) => {
  const conditionValidationMsgs: string[] = logic.calcValidationMsgs(
    validationErrors,
    [ValidationErrorTypes.incompleteCondition],
  );

  const conditionValidationState: AtlaskitSelectValidationState = conditionValidationMsgs.length ? 'error' : 'default';

  const conditionStatusValidationMsgs: string[] = logic.calcValidationMsgs(
    validationErrors,
    [
      ValidationErrorTypes.noConditionStatus,
      ValidationErrorTypes.invalidConditionStatus,
    ],
  );

  const conditionStatusValidationState: AtlaskitSelectValidationState = conditionStatusValidationMsgs.length ? 'error' : 'default';


  const callHandleConditionTypeOnChange = (e) => {
    logic.handleConditionTypeOnChange(microStore, e, chosenCondition);
  };

  const handleConditionStatusOnChange = (e) => {
    const newCondition = {
      ...chosenCondition,
      status: e,
    };

    logic.handleConditionTypeOnChange(microStore, newCondition, chosenCondition);
  };

  const callHandleRemoveRuleConditionOnClick = () => {
    logic.handleRemoveRuleConditionOnClick(microStore, chosenCondition);
  };

  return (
    <ErrorBoundary>
      {
        !isFirst &&
        <p className="pf-builder__section-paragraph --no-margin">And...</p>
      }
      <div className="pf-builder__design-rule-condition-item-wrapper">
        <div className="pf-builder__design-rule-condition-item">
          <Select
            value={chosenCondition}
            options={allConditions}
            onChange={callHandleConditionTypeOnChange}
            validationState={conditionValidationState}
          />
          {
            !!conditionValidationMsgs.length &&
            <ErrorMessage>{conditionValidationMsgs}</ErrorMessage>
          }
          {
            (
              chosenCondition.value === RuleConditionTypes.matchingIssueStatus ||
              chosenCondition.value === RuleConditionTypes.previousIssueStatus ||
              chosenCondition.value === RuleConditionTypes.fromStatus ||
              chosenCondition.value === RuleConditionTypes.toStatus
            ) &&
            <>
              <Select
                className="pf-builder__automation-rule-condition-status-droplist"
                placeholder="Select status"
                options={statusOptions}
                value={chosenCondition.status}
                onChange={handleConditionStatusOnChange}
                validationState={conditionStatusValidationState}
              />
              {
                !!conditionStatusValidationMsgs.length &&
                <ErrorMessage>{conditionStatusValidationMsgs}</ErrorMessage>
              }
            </>
          }
        </div>
        <AtlaskitBtn
          iconBefore={<TrashIcon label="Remove Condition" />}
          onClick={callHandleRemoveRuleConditionOnClick}
        />
      </div>
    </ErrorBoundary>
  );
});

export const MissingWhenPlaceholder = () => (
  <div className="pf-builder__automation-rule-item --basic">
    <span>Please select a When option first.</span>
  </div>
);

interface DesignRuleActionProps {
  microStore: AutomationRuleMicroStore;
  refData: StoreRefData;
  statusOptions: AtlaskitBasicSelectOptionsListItem[];
  validationErrors: ValidationError[];
  action?: StoreRuleAction;
  trigger?: StoreRuleTrigger;
}

export const DesignRuleAction = observer(({
  microStore,
  refData,
  statusOptions,
  validationErrors,
  action,
  trigger,
}: DesignRuleActionProps) => {

  const selectedStatus = (action && action.value === '1' && action.status) ? ({
    label: action.status.label,
    value: action.status.value,
  }) : undefined;

  const hasIsNotSubmittedCondition = microStore.conditions.some(
    condition => condition.value === RuleConditionTypes.isNotSubmitted,
  );

  const initialDoNotDuplicateValue = (
    microStore.action &&
      microStore.action.value === '2' &&
      !!microStore.action.doNotDuplicate ? doNotDuplicateOptions[0] : doNotDuplicateOptions[1]
  ) || doNotDuplicateOptions[1];

  const initialInternalExternalValue = (
    microStore.action &&
      microStore.action.value === '2' &&
      !!microStore.action.external ? internalExternalOptions[1] : internalExternalOptions[0]
  ) || internalExternalOptions[0];

  const actionValidationMsgs: string[] = logic.calcValidationMsgs(
    validationErrors,
    [ValidationErrorTypes.noAction],
  );

  const actionStatusValidationMsgs: string[] = logic.calcValidationMsgs(
    validationErrors,
    [
      ValidationErrorTypes.noActionStatus,
      ValidationErrorTypes.invalidActionStatus,
    ],
  );

  const actionStatusValidationState: AtlaskitSelectValidationState = actionStatusValidationMsgs.length ? 'error' : 'default';

  const handleTransitionStatusOnChange = (e) => {
    if (action && action.value === '1') {
      const newAction = {
        ...action,
        status: e,
      } as StoreRuleActionTransition;

      microStore.updateAction(newAction);
    }
  };

  const handleDoNotDuplicateOnChange = (e) => {
    if (action && action.value === '2') {
      const newAction = {
        ...action,
        doNotDuplicate: !!e.value,
      } as StoreRuleActionAddForm;

      microStore.updateAction(newAction);
    }
  };

  const handleInternalExternalChange = (e) => {
    if (action && action.value === '2') {
      const newAction = {
        ...action,
        external: e.value === true,
      } as StoreRuleActionAddForm;

      microStore.updateAction(newAction);
    }
  };

  return (
    <ErrorBoundary>
      {
        trigger &&
        trigger.value === TriggerTypes.submit &&
        <>
          <div className="pf-builder__automation-rule-item --basic">
            change the issue status to
          </div>
          <Select
            value={selectedStatus}
            options={statusOptions}
            onChange={handleTransitionStatusOnChange}
            validationState={actionStatusValidationState}
          />
          {
            !!actionStatusValidationMsgs.length &&
            <ErrorMessage>{actionStatusValidationMsgs}</ErrorMessage>
          }
        </>
      }
      {
        trigger &&
        trigger.value === TriggerTypes.transition &&
        <>
          <div className="pf-builder__automation-rule-item --basic">
            add this form to the issue
          </div>
          <Select
            value={initialDoNotDuplicateValue}
            options={doNotDuplicateOptions}
            onChange={handleDoNotDuplicateOnChange}
          />
          { (refData && refData.requestTypes && refData.requestTypes.length > 0) &&
            <Select className="pf-builder__automation-rule-condition-int-ext-droplist"
              value={initialInternalExternalValue}
              options={internalExternalOptions}
              onChange={handleInternalExternalChange}
            />
          }
        </>
      }
      {
        trigger &&
        trigger.value === TriggerTypes.workflowValidator &&
        <div className="pf-builder__automation-rule-item --basic">
          <>prevent the status change</>
          {
            !hasIsNotSubmittedCondition &&
            <>, unless this form is attached</>
          }
          <span>.</span>
        </div>
      }
      {
        (!trigger || !TriggerTypes[trigger.value]) &&
        <MissingWhenPlaceholder />
      }
      {
        !!actionValidationMsgs.length &&
        <ErrorMessage>{actionValidationMsgs}</ErrorMessage>
      }
    </ErrorBoundary>
  );
});

interface DesignAutomationRuleProps {
  store: ManageAutomationRulesStore;
  refData: StoreRefData;
  activeRuleMicroStoreId: string;
  triggerOptions: StoreRuleTrigger[];
  configureApis: ConfigureApis;
  updateIsEditOnCallback: (newStatus: boolean) => void;
  jwtToken?: string;
}

export const DesignAutomationRule = observer(({
  store,
  refData,
  activeRuleMicroStoreId,
  triggerOptions,
  configureApis,
  updateIsEditOnCallback,
  jwtToken,
}: DesignAutomationRuleProps) => {
  const microStore: AutomationRuleMicroStore = store[activeRuleMicroStoreId];
  const {
    isSaving,
    hasUnsavedChanges,
    validationErrors,
    riTypeInfo: {
      statusOptions,
      issueTypeId,
      requestTypeId,
    },
  } = microStore;

  const combinedRiTypeOptions = logic.combineTypeOptions(refData.allTypes, refData.issueTypes, refData.requestTypes);
  const allConditionTypes = Object.keys(RuleConditionTypes);

  const availableConditions = logic.generateAvailableConditionOptions(
    microStore,
    statusOptions,
    allConditionTypes,
    automationRuleConditionLabelLookup,
    microStore.trigger,
    issueTypeId,
    requestTypeId,
  );

  const preventNewConditions = (
    !availableConditions.length ||
    microStore.conditions.length === allConditionTypes.length ||
    microStore.conditions.some(condition => condition.value === '')
  );

  const handleRiTypeOnChange = (e: StoreAutomationRuleRiTypeInfo) => {
    microStore.updateRiTypeInfo(e);

    // Clear any previously configured conditions
    microStore.updateConditions([]);
  };

  const handleAddConditionOnClick = () => {
    const newConditionItem: StoreRuleCondition = {
      label: '',
      value: '',
    };

    microStore.updateConditions([
      ...microStore.conditions,
      newConditionItem,
    ]);
  };

  const callHandleSaveRuleOnClick = () => {
    logic.handleSaveRuleOnClick(
      store,
      microStore,
      configureApis,
      updateIsEditOnCallback,
      jwtToken,
    );
  };

  const callHandleCancelRuleOnClick = () => {
    logic.handleCancelRuleOnClick(store, microStore, updateIsEditOnCallback);
  };

  const isNotWorkflowValidatorTrigger = (
    microStore.trigger &&
    microStore.trigger.value !== TriggerTypes.workflowValidator
  );

  return (
    <ErrorBoundary>
      <div className="pf-builder__design-automation-rule">
        <AutomationRuleHeading />

        <div className="pf-builder__automation-rules-content">
          <div className="pf-builder__automation-rules-section">
            <h3>When...</h3>
            <DesignRuleTrigger
              microStore={microStore}
              statusOptions={microStore.riTypeInfo.statusOptions}
              triggerOptions={triggerOptions}
              validationErrors={validationErrors}
            />
          </div>

          {
            isNotWorkflowValidatorTrigger &&
            <div className="pf-builder__automation-rules-section">
              <h3>For...</h3>
              <Select
                maxMenuHeight={142}
                value={microStore.riTypeInfo}
                options={combinedRiTypeOptions}
                onChange={handleRiTypeOnChange}
              />
            </div>
          }

          <div className="pf-builder__automation-rules-section">
            <h3>If...</h3>
            {
              logic.generateDesignRuleConditionItems(
                microStore,
                microStore.riTypeInfo,
                refData.statuses,
                microStore.conditions,
                availableConditions,
                validationErrors,
              )
            }
            {
              !microStore.trigger &&
              <MissingWhenPlaceholder />
            }
            {
              !preventNewConditions &&
              <AtlaskitBtn onClick={handleAddConditionOnClick}>
                Add Condition
              </AtlaskitBtn>
            }
          </div>

          <div className="pf-builder__automation-rules-section">
            <h3>Then...</h3>
            <DesignRuleAction
              microStore={microStore}
              refData={refData}
              statusOptions={microStore.riTypeInfo.statusOptions}
              validationErrors={validationErrors}
              action={microStore.action}
              trigger={microStore.trigger}
            />
          </div>
        </div>

        <div className="pf-builder__automation-rules-control-panel">
          <AtlaskitBtnGroup>
            <AtlaskitBtn
              onClick={callHandleSaveRuleOnClick}
              appearance="primary"
              isDisabled={isSaving || !hasUnsavedChanges}
              isLoading={isSaving}
            >
              {`Save${hasUnsavedChanges ? '' : 'd'}`}
            </AtlaskitBtn>
            <AtlaskitBtn onClick={callHandleCancelRuleOnClick}>Cancel</AtlaskitBtn>
          </AtlaskitBtnGroup>
        </div>
      </div>
    </ErrorBoundary>
  );
});
