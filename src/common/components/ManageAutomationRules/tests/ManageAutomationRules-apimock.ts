import {
  AutomationRule,
  ReferenceData,
} from '../../../../modulesFB/FormBuilderApp/components/Configure/Configure-models';

export const mockAutomationRulesListSubmit: AutomationRule[] = [
  {
    id: 'i10001-2',
    trigger: {
      type: 'submit',
    },
    conditions: {
      formId: 4,
      status: '10009',
      issueTypeId: '10001',
      otherFormsSubmitted: true,
    },
    action: {
      id: 1,
      to: '4',
    },
  },
  {
    id: 'r876-2',
    trigger: {
      type: 'submit',
    },
    conditions: {
      status: '10002',
      requestTypeId: '876',
    },
    action: {
      id: 1,
      to: '10013',
    },
  },
  {
    id: 'p-5',
    trigger: {
      type: 'transition',
    },
    conditions: {
      formId: 4,
      status: '10013',
      status2: '10007',
    },
    action: {
      id: 2,
      doNotDuplicate: true,
      author: '',
      formId: 4,
    },
  },
  {
    id: 'p-8',
    trigger: {
      type: 'workflowValidator',
    },
    conditions: {},
    action: {
      id: 3,
      formId: 4,
      toStatus: '10013',
      fromStatus: '10007',
      isNotSubmitted: true,
    },
  },
];

export const mockReferenceDataGeneral: ReferenceData = {
  statuses: [
    {
      name: 'Work in progress',
      id: '10013',
    },
    {
      name: 'Waiting for support',
      id: '10002',
    },
    {
      name: 'Planning',
      id: '10009',
    },
    {
      name: 'Reopened',
      id: '4',
    },
    {
      name: 'Waiting for approval',
      id: '10007',
    },
  ],
  issueTypes: [
    {
      id: '10001',
      name: 'Task',
      iconUrl: 'https://robertc.atlassian.net/secure/viewavatar?size=xsmall&amp;avatarId=10517&amp;avatarType=issuetype',
      statuses: ['10009', '10013', '10007'],
      subtask: false,
    },
    {
      id: '10002',
      name: 'Service Request',
      iconUrl: 'https://robertc.atlassian.net/secure/viewavatar?size=xsmall&amp;avatarId=10513&amp;avatarType=issuetype',
      statuses: ['10009', '10013'],
      subtask: false,
    },
    {
      id: '10007',
      name: 'Sub-task',
      iconUrl: 'https://robertc.atlassian.net/secure/viewavatar?size=xsmall&amp;avatarId=10518&amp;avatarType=issuetype',
      statuses: ['10009', '10013'],
      subtask: true,
    },
    {
      id: '10003',
      name: 'Service Request with Approvals',
      iconUrl: 'https://robertc.atlassian.net/secure/viewavatar?size=xsmall&amp;avatarId=10514&amp;avatarType=issuetype',
      statuses: [],
      subtask: false,
    },
  ],
  requestTypes: [
    {
      id: '29',
      name: 'Request new hardware',
      issueTypeId: '10002',
      iconUrl: 'https://robertc.atlassian.net/secure/viewavatar?avatarType=SD_REQTYPE&amp;avatarId=10475&amp;size=large',
    },
    {
      id: '876',
      name: 'Request new software',
      issueTypeId: '10002',
      iconUrl: 'https://robertc.atlassian.net/secure/viewavatar?avatarType=SD_REQTYPE&amp;avatarId=10479&amp;size=large',
    },
    {
      id: '27',
      name: 'Set up a phone line redirect',
      issueTypeId: '10002',
      iconUrl: 'https://robertc.atlassian.net/secure/viewavatar?avatarType=SD_REQTYPE&amp;avatarId=10477&amp;size=large',
    },
    {
      id: '34',
      name: 'Upgrade or change a server',
      issueTypeId: '10001',
      iconUrl: 'https://robertc.atlassian.net/secure/viewavatar?avatarType=SD_REQTYPE&amp;avatarId=10510&amp;size=large',
    },
  ],
};
