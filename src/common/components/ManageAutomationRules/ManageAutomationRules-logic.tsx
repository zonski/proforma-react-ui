import * as React from 'react';
import { toJS } from 'mobx';
// Atlaskit
import SendIcon from '@atlaskit/icon/glyph/send';
import BitbucketCompareIcon from '@atlaskit/icon/glyph/bitbucket/compare';
import BitbucketBuildsIcon from '@atlaskit/icon/glyph/bitbucket/builds';
import DecisionIcon from '@atlaskit/icon/glyph/decision';
// Utils
import { sendDebugMsg } from '../../utils/errorHandlingUtils';
import { CustomNoticeData } from '../../utils/proFormaAPUtils';
import { displayNoticeToUser } from '../../../modulesFB/NoticeApp/components/Notice/Notice-logic';
// Models
import {
  AtlaskitBasicSelectOptionsListItem,
  AutomationRuleMicroStore,
  LookupAutomationRuleConditionLabel,
  ManageAutomationRulesStore,
  PreventTransitionUniqueActionProps,
  RuleConditionTypes,
  StoreAutomationRuleRiTypeInfo,
  StoreRefData,
  StoreRuleAction,
  StoreRuleCondition,
  StoreRuleTrigger,
  TriggerTypes,
  ValidationError,
  ValidationErrorTypes,
  ValidAutomationRule,
  ValidAutomationRuleAction,
  ValidRuleCondition,
} from './ManageAutomationRules-models';

import {
  ActionId,
  AutomationRule,
  AutomationRuleAction,
  AutomationRuleActionTransition,
  AutomationRuleConditions,
  ConfigureApis,
  ReferenceData,
} from '../../../modulesFB/FormBuilderApp/components/Configure/Configure-models';
// Store Classes
import { AutomationRuleLiveMicroStore } from './ManageAutomationRules-stores';
// Simple Functional Components
import { AutomationRuleItem, DesignRuleConditionItem, } from './ManageAutomationRules-sfcs';
// Constants
import { automationRuleTriggerLabelLookup, } from './ManageAutomationRules-constants';

import { ConfirmNoticetypes, } from '../../../modulesFB/NoticeApp/components/Notice/Notice-models';

// Logic
export const createEmptyRuleMicroStore = (
  refData: StoreRefData,
  projectFormName: string,
  projectFormId: string,
) => {
  const tempRuleItem = {
    id: '',
    trigger: {
      type: '',
    },
    conditions: {},
    action: {} as AutomationRuleActionTransition,
  };

  return new AutomationRuleLiveMicroStore(
    tempRuleItem,
    refData,
    projectFormName,
    projectFormId,
  );
};

export const createRuleMicroStores = (
  store: ManageAutomationRulesStore,
  rulesList: AutomationRule[],
  refData: StoreRefData,
  projectFormName: string,
  projectFormId: string,
) => {
  return rulesList.map((rule: AutomationRule) => {
    store[rule.id] = new AutomationRuleLiveMicroStore(
      rule,
      refData,
      projectFormName,
      projectFormId,
    );
    return rule.id;
  });
};

/**
 * Used for converting reference data into easier to consume structures by Atlaskit Select components
 */
export const structureRefData = (referenceData: ReferenceData): StoreRefData => {

  // filter the statuses down to the ones referenced in issues type (PF-932)
  let statusIds: string[] = [];
  referenceData.issueTypes.forEach(issueType => {
    if (issueType.statuses) {
      statusIds.push(...issueType.statuses);
    }
  });

  let statuses = statusIds
      .filter((statusId, index, list) => list.indexOf(statusId) === index)
      .map(statusId => {
        let status = referenceData.statuses.find(status => status.id === statusId);
        if (!status) { throw `Status not found: ${status}`; }
        return status;
      });

  const structuredStatuses: AtlaskitBasicSelectOptionsListItem[] = statuses.map(
    statusOption => ({
      label: statusOption.name || `Unlabeled Status (id: ${statusOption.id})`,
      value: statusOption.id,
    }),
  );

  const structuredIssueTypes: StoreAutomationRuleRiTypeInfo[] = referenceData.issueTypes.map(
    (rawIssueType) => {
      const structuredStatusOptions = rawIssueType.statuses.map(
        (statusId) => {
          const [statusOption] = structuredStatuses.filter(statusObj => statusObj.value === statusId);

          if (!statusOption) {
            sendDebugMsg({
              level: 'error',
              msg: `A provided issueType statusId: ${statusId} did not have a matching reference in the statuses list.`,
              error: new Error('No matching issue status data found for provided statusId, confirm data integrity.'),
            });

            return {
              label: 'Unavailable',
              value: statusId,
            };
          }

          // StatusId match found so return the statusOption
          return statusOption;
        },
      );

      return {
        label: rawIssueType.name,
        value: rawIssueType.id,
        statusOptions: structuredStatusOptions,
        issueTypeId: rawIssueType.id,
        ...rawIssueType.iconUrl && { riTypeIconUrl: rawIssueType.iconUrl },
      } as StoreAutomationRuleRiTypeInfo;
    },
  );

  const structuredRequestTypes: StoreAutomationRuleRiTypeInfo[] = referenceData.requestTypes.map(
    (rawRequestType) => {
      const [associatedIssueType] = structuredIssueTypes.filter(issueType => issueType.value === rawRequestType.issueTypeId);
      let statusOptions = associatedIssueType.statusOptions;

      if (!associatedIssueType) {
        sendDebugMsg({
          level: 'error',
          msg: `The requestType.issueTypeId: ${rawRequestType.issueTypeId}, did not match an existing issueType.`,
          error: new Error('No matching associated issueType found for the issueTypeId provided, confirm data integrity.'),
        });

        statusOptions = [];
      }

      // Associated issueType found so share the statusOptions from it
      return {
        statusOptions,
        label: rawRequestType.name,
        value: rawRequestType.id,
        requestTypeId: rawRequestType.id,
        ...rawRequestType.iconUrl && { riTypeIconUrl: rawRequestType.iconUrl },
      } as StoreAutomationRuleRiTypeInfo;
    },
  );

  const allTypes: StoreAutomationRuleRiTypeInfo = {
    label: 'All issue / request types',
    value: 'allTypes',
    statusOptions: structuredStatuses,
  };

  return {
    allTypes,
    issueTypes: structuredIssueTypes,
    requestTypes: structuredRequestTypes,
    statuses: structuredStatuses,
  };
};

/**
 * Updates the store with rule data for the active project form
 * @param {object} store - Manages component state
 */
export const updateManageAutmationRulesStoreData = (
  store: ManageAutomationRulesStore,
  rulesList: AutomationRule[],
  referenceData: ReferenceData,
  projectFormName: string,
  projectFormId: string,
) => {
  // Update the reference data in the store
  const structuredReferenceData = structureRefData(referenceData);
  store.updateRefData(structuredReferenceData);

  // Generate the triggerOptions in the store
  const newTriggerOptions: StoreRuleTrigger[] = Object.keys(TriggerTypes).map(triggerType => ({
    label: automationRuleTriggerLabelLookup[triggerType],
    value: TriggerTypes[triggerType],
  }));
  store.updateTriggerOptions(newTriggerOptions);

  // Spin up rulesMicroStores combining data from rulesData.rulesList with structuredReferenceData
  const ruleMicroStoreIds = createRuleMicroStores(
    store,
    rulesList,
    structuredReferenceData,
    projectFormName,
    projectFormId,
  );

  // Update the list of loaded rulesMicroStoreIds
  store.updateRuleMicroStoreIds(ruleMicroStoreIds);

  // Update Raw Data Collections
  store.updateRawAutomationRules(rulesList);
  store.updateRawReferenceData(referenceData);

  // Disable the loading spinner so rules are shown
  store.updateIsLoadingRules(false);
};

export const calcValidationMsgs = (
  validationErrors: ValidationError[],
  suitableErrorTypes: ValidationErrorTypes[],
) => validationErrors.reduce(
  (errorMsgs: string[], currentError) => {
    if (suitableErrorTypes.some(errorType => errorType === currentError.errorType)) {
      return [
        ...errorMsgs,
        currentError.errorMsg,
      ];
    }

    return errorMsgs;
  },
  [],
);

export const validateRuleTrigger = (
  statusOptions: AtlaskitBasicSelectOptionsListItem[],
  trigger?: StoreRuleTrigger,
): ValidationError[] => {
  let validationErrors: ValidationError[] = [];

  const noTriggerError: ValidationError = {
    errorType: ValidationErrorTypes.noTrigger,
    errorMsg: 'Rule triger is required but was not provided.',
  };

  const noTriggerStatusError: ValidationError = {
    errorType: ValidationErrorTypes.noTriggerStatus,
    errorMsg: 'Required status for the rule trigger was missing.',
  };

  const invalidTriggerStatusError: ValidationError = {
    errorType: ValidationErrorTypes.invalidTriggerStatus,
    errorMsg: 'Specified status was not available for the chosen issue / request type.',
  };

  if (typeof trigger === 'undefined') {
    validationErrors = [
      ...validationErrors,
      noTriggerError,
    ];
  }

  const noStatus = (
    trigger &&
    trigger.value === 'transition' &&
    !trigger.status
  );

  if (noStatus) {
    validationErrors = [
      ...validationErrors,
      noTriggerStatusError,
    ];
  }

  const statusValue = (
    !!trigger &&
    trigger.value === 'transition' &&
    !!trigger.status &&
    trigger.status.value
  );

  const invalidStatus = (
    statusValue &&
    !statusOptions.some(statusOption => statusOption.value === statusValue)
  );

  if (invalidStatus) {
    validationErrors = [
      ...validationErrors,
      invalidTriggerStatusError,
    ];
  }

  return validationErrors;
};

export const validateRuleConditions = (
  conditions: StoreRuleCondition[],
  statusOptions: AtlaskitBasicSelectOptionsListItem[],
): ValidationError[] => {
  const incompleteConditionError: ValidationError = {
    errorType: ValidationErrorTypes.incompleteCondition,
    errorMsg: 'Conditions must not be left blank, please remove or complete the blank condition.',
  };

  const noConditionStatusError: ValidationError = {
    errorType: ValidationErrorTypes.noConditionStatus,
    errorMsg: 'Required status is missing from the selected condition.',
  };

  const invalidConditionStatusError: ValidationError = {
    errorType: ValidationErrorTypes.invalidConditionStatus,
    errorMsg: 'Specified status was not available for the chosen issue / request type.',
  };

  return conditions.reduce(
    (errorList: ValidationError[], condition) => {
      // Check for a blank condition
      if (condition.value === '') {
        return [
          ...errorList,
          incompleteConditionError,
        ];
      }

      // Check any condition that uses a status has one selected
      const missingStatus = (
        condition.value === RuleConditionTypes.previousIssueStatus ||
        condition.value === RuleConditionTypes.matchingIssueStatus ||
        condition.value === RuleConditionTypes.fromStatus ||
        condition.value === RuleConditionTypes.toStatus
      ) && !condition.status;

      if (missingStatus) {
        return [
          ...errorList,
          noConditionStatusError,
        ];
      }

      // Check that the selected status value is compatible with the selected issue / request type
      const conditionStatusValue = (
        (
          condition.value === RuleConditionTypes.previousIssueStatus ||
          condition.value === RuleConditionTypes.matchingIssueStatus ||
          condition.value === RuleConditionTypes.fromStatus ||
          condition.value === RuleConditionTypes.toStatus
        ) &&
        !!condition.status &&
        condition.status.value
      );

      const invalidStatus = (
        conditionStatusValue &&
        !statusOptions.some(statusOption => statusOption.value === conditionStatusValue)
      );

      if (invalidStatus) {
        return [
          ...errorList,
          invalidConditionStatusError,
        ];
      }

      return errorList;
    },
    [],
  );
};

export const validateRuleAction = (
  statusOptions: AtlaskitBasicSelectOptionsListItem[],
  action?: StoreRuleAction,
): ValidationError[] => {
  let validationErrors: ValidationError[] = [];

  const noAction: ValidationError = {
    errorType: ValidationErrorTypes.noAction,
    errorMsg: 'Rule action is required but was not provided.',
  };

  const noActionStatusError: ValidationError = {
    errorType: ValidationErrorTypes.noActionStatus,
    errorMsg: 'Required status for the rule action was missing.',
  };

  const invalidActionStatusError: ValidationError = {
    errorType: ValidationErrorTypes.invalidActionStatus,
    errorMsg: 'Specified status was not available for the chosen issue / request type.',
  };

  // Check that an action was specified
  if (!action) {
    validationErrors = [
      ...validationErrors,
      noAction,
    ];
  }

  // Check that a status was selected if a requirement of specified action
  if (action && action.value === '1' && !action.status) {
    validationErrors = [
      ...validationErrors,
      noActionStatusError,
    ];
  }

  // Check that a status was selected if a requirement of specified action
  const actionStatusValue = (
    !!action &&
    action.value === '1' &&
    !!action.status &&
    action.status.value
  );

  const invalidStatus = (
    actionStatusValue &&
    !statusOptions.some(statusOption => statusOption.value === actionStatusValue)
  );

  if (invalidStatus) {
    validationErrors = [
      ...validationErrors,
      invalidActionStatusError,
    ];
  }

  return validationErrors;
};

export const validateAutomationRule = (ruleMicroStore: AutomationRuleMicroStore): ValidAutomationRule | undefined => {
  // Combine validation rules into a single list
  const allValidationErrors = [
    ...validateRuleTrigger(ruleMicroStore.riTypeInfo.statusOptions, ruleMicroStore.trigger),
    ...validateRuleConditions(ruleMicroStore.conditions, ruleMicroStore.riTypeInfo.statusOptions),
    ...validateRuleAction(ruleMicroStore.riTypeInfo.statusOptions, ruleMicroStore.action),
  ];

  if (allValidationErrors.length) {
    ruleMicroStore.updateValidationErrors(allValidationErrors);
    return;
  }

  // No errors found so it is safe to return the validated rule microStore
  return ruleMicroStore as ValidAutomationRule;
};

/**
 * Isolates conditions that need to be inserted under the action property for API compatibility
 */
export const extractPreventTransitionActionProps = (
  conditions: ValidRuleCondition[],
): PreventTransitionUniqueActionProps => conditions.reduce(
  (targetData: PreventTransitionUniqueActionProps, condition) => {
    if (condition.value === RuleConditionTypes.isNotSubmitted) {
      return {
        ...targetData,
        isNotSubmitted: true,
      };
    }

    if (condition.value === RuleConditionTypes.fromStatus) {
      return {
        ...targetData,
        fromStatus: condition.status.value,
      };
    }

    if (condition.value === RuleConditionTypes.toStatus) {
      return {
        ...targetData,
        toStatus: condition.status.value,
      };
    }

    return targetData;
  },
  { isNotSubmitted: false },
);

export const discrimRuleAction = (
  action: ValidAutomationRuleAction,
  formId: number,
  conditions: ValidRuleCondition[],
): AutomationRuleAction => {
  switch (action.value) {
    case '1':
      return {
        id: ActionId.transition,
        to: action.status.value,
      };

    case '2':
      return {
        formId,
        id: ActionId.addform,
        author: action.author,
        doNotDuplicate: action.doNotDuplicate,
        external: action.external,
      };

    case '3':
      return {
        formId,
        id: ActionId.preventTransition,
        ...extractPreventTransitionActionProps(conditions),
      };
  }
};

export const extractRuleConditionsFromList = (
  conditions: ValidRuleCondition[],
): AutomationRuleConditions => conditions.reduce(
  (conditionsObj, condition) => {
    switch (condition.value) {
      case 'allFormsSubmitted':
        return {
          ...conditionsObj,
          otherFormsSubmitted: condition.otherFormsSubmitted,
        };

      case 'matchingIssueStatus':
        return {
          ...conditionsObj,
          status: condition.status.value,
        };

      case 'previousIssueStatus':
        return {
          ...conditionsObj,
          status: condition.status.value,
        };

      default:
        return conditionsObj;
    }
  },
  {},
);

export const convertAutomationRuleForApis = (validAutomationRule?: ValidAutomationRule): AutomationRule | undefined => {
  if (!validAutomationRule) {
    return;
  }

  // We are dealing with a valid automation rule so begin conversion
  const {
    id,
    formId,
    trigger,
    conditions,
    action,
    riTypeInfo,
  } = validAutomationRule;

  const ruleAction = discrimRuleAction(action, formId, conditions);

  const convertedRule: AutomationRule = {
    id,
    trigger: {
      type: trigger.value,
    },
    conditions: {
      ...riTypeInfo.issueTypeId && { issueTypeId: riTypeInfo.issueTypeId },
      ...riTypeInfo.requestTypeId && { requestTypeId: riTypeInfo.requestTypeId },
      ...ruleAction.id === ActionId.transition && { status2: ruleAction.to },
      ...trigger.value === TriggerTypes.submit && { formId },
      ...trigger.value === TriggerTypes.transition && { status2: trigger.status.value },
      ...extractRuleConditionsFromList(conditions),
    },
    action: ruleAction,
  };

  return convertedRule;
};

export const generateExistingRules = (
  store: ManageAutomationRulesStore,
  ruleMicroStoreIds: string[],
  updateIsEditOnCallback: (newStatus: boolean) => void,
  configureApis: ConfigureApis,
  jwtToken?: string,
) => {
  return ruleMicroStoreIds.map(ruleId => (
    <AutomationRuleItem
      key={ruleId}
      store={store}
      ruleMicroStoreId={ruleId}
      updateIsEditOnCallback={updateIsEditOnCallback}
      configureApis={configureApis}
      jwtToken={jwtToken}
    />
  ));
};

export const combineTypeOptions = (
  allTypes: StoreAutomationRuleRiTypeInfo,
  issueTypes: StoreAutomationRuleRiTypeInfo[],
  requestTypes: StoreAutomationRuleRiTypeInfo[],
) => {
  if (issueTypes.length && requestTypes.length) {
    return [
      allTypes,
      {
        label: 'Request Types',
        options: requestTypes,
      },
      {
        label: 'Issue Types',
        options: issueTypes,
      },
    ];
  }

  if (requestTypes.length) {
    return [
      allTypes,
      ...toJS(requestTypes),
    ];
  }

  if (issueTypes.length) {
    return [
      allTypes,
      ...toJS(issueTypes),
    ];
  }

  return [allTypes];
};

export const generateDesignRuleConditionItems = (
  microStore: AutomationRuleMicroStore,
  riTypeInfo: StoreAutomationRuleRiTypeInfo,
  allStatusOptions: AtlaskitBasicSelectOptionsListItem[],
  conditions: StoreRuleCondition[],
  allConditions: StoreRuleCondition[],
  validationErrors: ValidationError[],
) => {
  return conditions.map((condition, index) => {
    const statusOptions = (!riTypeInfo.issueTypeId && !riTypeInfo.requestTypeId) ? allStatusOptions : riTypeInfo.statusOptions;

    return (
      <DesignRuleConditionItem
        key={condition.value}
        microStore={microStore}
        allConditions={allConditions}
        chosenCondition={condition}
        statusOptions={statusOptions}
        validationErrors={validationErrors}
        isFirst={index === 0}
      />
    );
  });
};

/**
 * Determines if certain rule configuration options are compatible with the specified trigger type
 */
export const checkIsTriggerCompatible = (
  conditionType: string,
  trigger?: StoreRuleTrigger,
) => {
  if (!trigger) {
    // No trigger specified so assume all conditions are incompatible
    return false;
  }

  if (trigger.value === TriggerTypes.submit) {
    return (
      conditionType === RuleConditionTypes.matchingIssueStatus ||
      conditionType === RuleConditionTypes.allFormsSubmitted
    );
  }

  if (trigger.value === TriggerTypes.transition) {
    return (
      conditionType === RuleConditionTypes.previousIssueStatus
    );
  }

  if (trigger.value === TriggerTypes.workflowValidator) {
    return (
      conditionType === RuleConditionTypes.isNotSubmitted ||
      conditionType === RuleConditionTypes.fromStatus ||
      conditionType === RuleConditionTypes.toStatus
    );
  }

  // No compatibility issues were found
  return true;
};

export const generateAvailableConditionOptions = (
  microStore: AutomationRuleMicroStore,
  statusOptions: AtlaskitBasicSelectOptionsListItem[],
  allConditionTypes: string[],
  allConditionLabels: LookupAutomationRuleConditionLabel,
  trigger?: StoreRuleTrigger,
  issueTypeId?: string,
  requestTypeId?: string,
) => {
  return allConditionTypes.reduce(
    (unusedConditions: StoreRuleCondition[], conditionType) => {
      const conditionIsUnused = !microStore.conditions.some(condition => condition.value === conditionType);

      const conditionDependsOnStatus = (
        conditionType === RuleConditionTypes.matchingIssueStatus ||
        conditionType === RuleConditionTypes.previousIssueStatus ||
        conditionType === RuleConditionTypes.toStatus ||
        conditionType === RuleConditionTypes.fromStatus
      );

      const missingRequiredStatusOptions = (
        conditionDependsOnStatus &&
        (issueTypeId || requestTypeId) &&
        !statusOptions.length
      );

      if (missingRequiredStatusOptions) {
        // skip over conditions that depend on choosing an issue status if no options are available
        return unusedConditions;
      }

      const triggerIsCompatible = checkIsTriggerCompatible(conditionType, trigger);

      if (!triggerIsCompatible) {
        return unusedConditions;
      }

      // Configures default values for available, unused conditions
      if (conditionIsUnused) {
        unusedConditions.push({
          value: RuleConditionTypes[conditionType],
          label: allConditionLabels[conditionType],
          ...conditionType === RuleConditionTypes.allFormsSubmitted && { otherFormsSubmitted: true },
          ...conditionType === RuleConditionTypes.isNotSubmitted && { isNotSubmitted: false },
          ...conditionDependsOnStatus && {
            status: undefined,
          },
        });
      }

      return unusedConditions;
    },
    [],
  );
};

/**
 * Returns the icon associated with the provided triggerType or a generic placeholder if unhandled
 */
export const calcRuleIcon = (triggerType: string) => {
  switch (triggerType) {
    case TriggerTypes.submit:
      return <SendIcon label="Rule Type: Submit" size="xlarge" />;

    case TriggerTypes.transition:
      return <BitbucketCompareIcon label="Rule Type: Transition" size="xlarge" />;

    case TriggerTypes.workflowValidator:
      return <BitbucketBuildsIcon label="Rule Type: Workflow Validator" size="xlarge" />;

    default:
      return <DecisionIcon label="Rule Type: Default" size="xlarge" />;
  }
};

// Event Handlers

/**
 * Prepares the store for adding a new automation rule before switching into edit mode
 * @param {object} store - Manages component state
 */
export const handleAddRuleOnclick = (
  store: ManageAutomationRulesStore,
  updateIsEditOnCallback: (newStatus: boolean) => void,
) => {
  // Reset the tempRuleMicroStore used during the creation of a new rule
  store.updateTempRuleMicroStore(
    createEmptyRuleMicroStore(
      store.refData,
      store.projectFormName,
      store.projectFormId,
    )
  );

  // Assign the tempRuleMicroStore as the activeRuleId
  store.updateActiveRuleId('tempRuleMicroStore');

  // Switch into edit mode
  updateIsEditOnCallback(true);
};

export const handleSaveRuleOnClick = (
  store: ManageAutomationRulesStore,
  ruleMicroStore: AutomationRuleMicroStore,
  configureApis: ConfigureApis,
  updateIsEditOnCallback: (newStatus: boolean) => void,
  jwtToken?: string,
) => {
  // Flag the rule as in the process of being saved
  ruleMicroStore.updateIsSaving(true);

  // Validate rule data
  const validAutomationRule = validateAutomationRule(ruleMicroStore);

  // No valid automation rule to save so exit early
  if (!validAutomationRule) {
    ruleMicroStore.updateIsSaving(false);
    return;
  }

  // Convert to API compatible structure
  const convertedAutomationRule = convertAutomationRuleForApis(validAutomationRule);

  // Take the creation path if this is a new automation rule
  if (store.activeRuleId === 'tempRuleMicroStore' && convertedAutomationRule) {
    configureApis.postAutomationRule(convertedAutomationRule, jwtToken)
      .then((response) => {
        if (response.id) {
          ruleMicroStore.updateIsSaving(false);
          store[response.id] = ruleMicroStore;

          const newRuleMicroStoreIds = [
            ...store.ruleMicroStoreIds,
            response.id,
          ];

          // Update the list of loaded rulesMicroStoreIds
          store.updateRuleMicroStoreIds(newRuleMicroStoreIds);

          // Switch over to use the new activeRuleId
          store.updateActiveRuleId(response.id);

          // Update internal rule id reference
          ruleMicroStore.updateRuleId(response.id);

          // Reset validation errors
          ruleMicroStore.updateValidationErrors([]);

          // Clear hasUnsavedChanges check
          ruleMicroStore.updateHasUnsavedChanges(false);

          // Update the converted automation rule to use the new id provided by the backend
          convertedAutomationRule.id = response.id;

          // Insert the new raw rule data into the rawAutomationRules list (used when cancelling with unsaved changes)
          const newRawAutomationRules = [
            ...store.rawAutomationRules,
            convertedAutomationRule,
          ];

          store.updateRawAutomationRules(newRawAutomationRules);

          // Switch back to view mode
          updateIsEditOnCallback(false);
        }
      })
      .catch((error) => {
        console.error(error);
      });

    return;
  }

  // Dealing with an existing rule that needs updating
  if (convertedAutomationRule) {
    configureApis.putAutomationRule(convertedAutomationRule, jwtToken)
      .then((response) => {
        ruleMicroStore.updateIsSaving(false);

        // Reset validation errors
        ruleMicroStore.updateValidationErrors([]);

        // Clear hasUnsavedChanges check
        ruleMicroStore.updateHasUnsavedChanges(false);

        // Update existing rawAutomationRule data (used when cancelling with unsaved changes)
        const newRawAutomationRules = store.rawAutomationRules.map((rule) => {
          if (rule.id === convertedAutomationRule.id) {
            return convertedAutomationRule;
          }

          return rule;
        });

        store.updateRawAutomationRules(newRawAutomationRules);

        // Switch back to view mode
        updateIsEditOnCallback(false);
      })
      .catch((error) => {
        console.error(error);
      });
  }
};

export const handleCancelRuleOnClick = async (
  store: ManageAutomationRulesStore,
  ruleMicroStore: AutomationRuleMicroStore,
  updateIsEditOnCallback: (newStatus: boolean) => void,
) => {
  if (!ruleMicroStore.hasUnsavedChanges) {
    // Reset validation errors
    ruleMicroStore.updateValidationErrors([]);

    // Switch back to view mode
    updateIsEditOnCallback(false);
    return;
  }

  // Unsaved changes detected to confirm if they should be discarded
  const discardChangesmsg = 'All unsaved changes will be lost if you click "Confirm".';
  const customData: CustomNoticeData = {
    noticeType: ConfirmNoticetypes.automationRuleUnsavedChanges,
    title: 'Confirm Discarding Changes',
    msg: discardChangesmsg,
    confirmationName: 'Confirm',
  };

  if (ruleMicroStore.hasUnsavedChanges && await displayNoticeToUser(customData)) {
    const rawRule = store.rawAutomationRules.filter(rule => rule.id === ruleMicroStore.id);

    // Replaces the current AutomationRuleLiveMicroStore with a new instance based off the orginal data
    createRuleMicroStores(store, rawRule, store.refData, store.projectFormName, store.projectFormId);

    // Reset validation errors
    ruleMicroStore.updateValidationErrors([]);

    // Clear hasUnsavedChanges check
    ruleMicroStore.updateHasUnsavedChanges(false);

    // Switch back to view mode
    updateIsEditOnCallback(false);
  }
};

export const handleDeleteRuleOnclick = async (
  store: ManageAutomationRulesStore,
  microStore: AutomationRuleMicroStore,
  configureApis: ConfigureApis,
  updateIsEditOnCallback: (newStatus: boolean) => void,
  jwtToken?: string,
) => {
  const deleteRuleMsg = 'Please confirm you would like to delete this automation rule. This action cannot be reversed.';
  const customData: CustomNoticeData = {
    noticeType: ConfirmNoticetypes.automationRuleDelete,
    title: 'Confirm Deleting Automation Rule',
    msg: deleteRuleMsg,
    confirmationName: 'Delete',
  };

  try {
    if (await displayNoticeToUser(customData)) {
      // Delete from backend
      await configureApis.deleteAutomationRule(microStore.id, jwtToken);

      // Update automationRuleMicroStoreIds
      const newRuleMicroStoreIdsList = store.ruleMicroStoreIds.filter(rule => rule !== microStore.id);
      store.updateRuleMicroStoreIds(newRuleMicroStoreIdsList);

      // Discard unused microStore
      delete store[microStore.id];

      // Switch back to view mode
      store.updateActiveRuleId('tempRuleMicroStore');
      updateIsEditOnCallback(false);
    }
  } catch (error) {
    console.error(error);
  }
};

/**
 * Switches over to edit mode for the existing automation rule that was clicked
 */
export const handleAutomationRuleOnClick = (
  store: ManageAutomationRulesStore,
  newActiveRuleMicroStoreId: string,
  updateIsEditOnCallback: (newStatus: boolean) => void,
) => {
  store.updateActiveRuleId(newActiveRuleMicroStoreId);
  updateIsEditOnCallback(true);
};

export const handleConditionTypeOnChange = (
  microStore: AutomationRuleMicroStore,
  newCondition: StoreRuleCondition,
  currentCondition: StoreRuleCondition,
) => {
  const noStatusChange = (
    (currentCondition.value === 'matchingIssueStatus' || currentCondition.value === 'previousIssueStatus') &&
    (newCondition.value === 'matchingIssueStatus' || newCondition.value === 'previousIssueStatus') &&
    currentCondition.status === newCondition.status
  );

  const conditionExists = microStore.conditions.some(condition => (
    condition.value === newCondition.value &&
    noStatusChange
  ));

  if (conditionExists) {
    return;
  }

  const newConditionsList = [
    ...toJS(microStore.conditions).filter(condition => condition.value !== currentCondition.value),
    newCondition,
  ];

  microStore.updateConditions(newConditionsList);
};

export const handleRemoveRuleConditionOnClick = (microStore: AutomationRuleMicroStore, removedCondition) => {
  const newConditionsList = microStore.conditions.filter(condition => condition.value !== removedCondition.value);
  microStore.updateConditions(newConditionsList);
};

export const handleRuleItemDroplistOnClick = (
  microStore: AutomationRuleMicroStore,
  currentStatus: boolean,
) => {
  microStore.updateIsViewDroplistOpen(!currentStatus);
};
