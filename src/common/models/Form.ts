export interface Form {
  id: number;
  key: string;
  name: string;
  project: string;
  updated: {
    friendly: string
  };
  requesttypes: RequestType[];
  editUrl: string;
}

export interface RequestType {
  id: string;
  name: string;
  portal: boolean;
  hidden: boolean;
  newIssue: boolean;
}
