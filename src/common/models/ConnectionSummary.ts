export interface ConnectionSummary {
  id: number;
  name: string;
  source: string;
  status: string;
}

export enum ConnectionStatus {
  unknown,
  ok = 'ok',
  failing = 'failing'
}
