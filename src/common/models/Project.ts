export interface Project {
  key: string;
  name: string;
  projectTypeKey: string;
  projectTypeName: string;
  smallAvatarUrl: string;
}
