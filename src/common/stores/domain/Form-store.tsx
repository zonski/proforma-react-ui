import { action, observable } from 'mobx';
import { FormApi } from '../../apis/FormApi';
import { Form, RequestType } from '../../models/Form';

export class FormStore {

  id: number;

  @observable name: string = '';
  @observable key: string = '';
  @observable projectName: string = '';
  @observable lastUpdatedFriendly: string = '';
  @observable requestTypes: RequestType[] = [];
  @observable editUrl: string = '';

  @observable deleting: boolean = false;
  @observable deleted: boolean = false;

  constructor(private formApi: FormApi, id: number) {
    this.id = id
  }

  update(details: Form) {
    this.key = details.key;
    this.name = details.name;
    this.projectName = details.project;
    this.lastUpdatedFriendly = details.updated.friendly;
    this.requestTypes = details.requesttypes;
    this.editUrl = details.editUrl;
  }

  @action deleteForm() {
    this.deleting = true;
    return this.formApi.deleteForm(this.key, this.id)
      .then(() => {
        this.deleting = false;
        this.deleted = true;
      });
  }
}