import { action, computed, observable } from 'mobx';
import { PagedResults } from '../../models/PagedResults';

export abstract class SearchStore<ItemType> {

  @observable loading = true;

  @observable pageSize = 10;

  @observable start = 0;
  @observable total?;
  @observable items: ItemType[] = [];
  @observable nextCursor?;
  @observable prevCursor?;

  @observable sortKey?: string;
  @observable sortOrder?: string;

  @computed get hasNextPage() {
    return !!this.nextCursor;
  }

  @computed get hasPrevPage() {
    return !!this.prevCursor;
  }

  @computed get end() {
    return this.start + this.items.length;
  }

  abstract doSearch(pageSize: number, cursor?: string, sortKey?: string, sortOrder?: string): Promise<PagedResults<ItemType>>;

  @action startSearch() {
    this.clearSearch();
    this.search();
  }

  @action refresh() {
    this.search();
  }

  @action setPageSize(pageSize: number) {
    this.pageSize = pageSize;
    this.clearSearch();
    this.search();
  }

  @action nextPage() {
    if (this.hasNextPage) {
      this.search(this.nextCursor);
    }
  }

  @action prevPage() {
    if (this.hasPrevPage) {
      this.search(this.prevCursor);
    }
  }

  @action sortBy(sortDetails) {
    this.clearSearch();
    this.sortKey = sortDetails ? sortDetails.key : null;
    this.sortOrder = sortDetails ? sortDetails.sortOrder : null;
    this.search();
  }

  private clearSearch() {
    this.prevCursor = null;
    this.nextCursor = null;
  }

  private search(cursor?: string) {
    this.loading = true;
    this.items.length = 0;
    this.doSearch(this.pageSize, cursor, this.sortKey, this.sortOrder).then(results => {
      this.start = results.start;
      this.total = results.total;
      this.prevCursor = results.cursor.prev;
      this.nextCursor = results.cursor.next;
      results.results.forEach(item => {
        this.items.push(item);
      });
      this.loading = false;
    });
  }
}