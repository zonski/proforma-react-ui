import { action, observable } from "mobx";
import { AdminApi } from '../../apis/AdminApi';

export class AdminSettingsDomainStore {

  @observable enabled = true;
  @observable loading = true;

  constructor(private apis: { adminApi: AdminApi }) {}

  @action loadEnabledState() {
    this.loading = true;
    this.apis.adminApi.isIssueFormsEnabled().then(b => {
      this.enabled = b;
      this.loading = false;
    });
  }

  @action toggleEnabled() {
    this.loading = true;
    this.apis.adminApi.setIssueFormsEnabled(!this.enabled).then(b => {
      this.enabled = b;
      this.loading = false;
    });
  }

}
