import { observable } from 'mobx';
import { ConnectionStatus, ConnectionSummary } from '../../models/ConnectionSummary';

export class ConnectionSummaryStore {

  id: number;
  @observable name: string;
  @observable source: string;
  @observable status: ConnectionStatus;

  constructor(connectionSummary: ConnectionSummary) {
    this.id = connectionSummary.id;
    this.name = connectionSummary.name;
    this.source = connectionSummary.source;
    this.status = ConnectionStatus[connectionSummary.status] || ConnectionStatus.unknown;
  }

}

