import { AdminSettingsDomainStore } from './AdminSettingsDomain-store';
import { ConnectionDomainStore } from './ConnectionDomain-store';
import { FormDomainStore } from './FormDomain-store';
import { ProjectDomainStore } from './ProjectDomain-store';

export function createDomainStores(apis) {
  return {
    adminSettingsDomainStore: new AdminSettingsDomainStore(apis),
    connectionDomainStore: new ConnectionDomainStore(apis),
    projectDomainStore: new ProjectDomainStore(apis),
    formDomainStore: new FormDomainStore(apis)
  };
}

