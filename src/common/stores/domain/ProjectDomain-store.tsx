import { action, observable } from 'mobx';
import { ProjectStore } from './Project-store';
import { ProjectApi } from '../../apis/ProjectApi';
import { SearchStore } from './Search-store';
import { PagedResults } from '../../models/PagedResults';
import { Project } from '../../models/Project';

export class ProjectDomainStore {

  @observable projectStores: ProjectStore[] = [];
  @observable enablingAll: boolean = false;

  constructor(private apis: {
    projectApi: ProjectApi
  }) {
  }

  @action setAllProjectsEnabled(enabled: boolean) {
    this.enablingAll = true;
    this.apis.projectApi.setAllProjectsEnabled(enabled).then(() => {
      this.projectStores.forEach(
        p => p.enabledState = enabled ? 'enabled' : 'disabled');
      this.enablingAll = false;
    });
  }

  @action startAdminSearch(): ProjectAdminSearchStore {
    let searchStore = new ProjectAdminSearchStore({
      projectApi: this.apis.projectApi,
      projectDomainStore: this
    });
    searchStore.startSearch();
    return searchStore;
  }

  @action getProjectStore(key): ProjectStore {
    let projectStore = this.projectStores.find(p => p.key === key);
    if (!projectStore) {
      projectStore = new ProjectStore(this.apis.projectApi, key);
      this.projectStores.push(projectStore);
    }

    return projectStore;
  }
}

export class ProjectAdminSearchStore extends SearchStore<ProjectStore> {

  constructor(private context: {
    projectApi: ProjectApi,
    projectDomainStore: ProjectDomainStore
  }) {
    super();
  }

  doSearch(pageSize: number, cursor?: string, sortKey?: string, sortOrder?: string): Promise<PagedResults<ProjectStore>> {
    return new Promise<PagedResults<ProjectStore>>((resolve, reject) => {
      this.context.projectApi.search(pageSize, cursor)
        .then((response: PagedResults<Project>) => {

          // for each project, create a ProjectStore
          let projectStores = response.results.map(details => {
            let projectStore = this.context.projectDomainStore.getProjectStore(details.key);
            projectStore.update(details);
            projectStore.loadEnabledState();
            return projectStore;
          });

          resolve(Object.assign({}, response, {results: projectStores}));
        });
    });
  }

}
