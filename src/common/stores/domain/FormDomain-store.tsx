import { action, observable } from 'mobx';
import { FormStore } from './Form-store';
import { FormApi } from '../../apis/FormApi';
import { SearchStore } from './Search-store';
import { PagedResults } from '../../models/PagedResults';
import { Form } from '../../models/Form';

export class FormDomainStore {

  @observable formStores: FormStore[] = [];

  constructor(private apis: { formApi: FormApi }) {}

  @action startSearch(projectKey?: string): FormSearchStore {
    let searchStore = new FormSearchStore({
      formApi: this.apis.formApi,
      formDomainStore: this,
      projectKey: projectKey
    });
    searchStore.startSearch();
    return searchStore;
  }

  @action getFormStore(id): FormStore {
    let formStore = this.formStores.find(p => p.id === id);
    if (!formStore) {
      formStore = new FormStore(this.apis.formApi, id);
      this.formStores.push(formStore);
    }
    return formStore;
  }
}

export class FormSearchStore extends SearchStore<FormStore> {

  constructor(private context: {
    formApi: FormApi,
    formDomainStore: FormDomainStore,
    projectKey?: string
  }) {
    super();
  }

  doSearch(pageSize: number, cursor?: string, sortKey?: string, sortOrder?: string): Promise<PagedResults<FormStore>> {
    return new Promise<PagedResults<FormStore>>((resolve, reject) => {
      this.context.formApi.search(pageSize, this.context.projectKey, cursor, sortKey, sortOrder)
        .then((response: PagedResults<Form>) => {

          // for each form, create a FormStore
          let formStores = response.results.map(details => {
            let formStore = this.context.formDomainStore.getFormStore(details.id);
            formStore.update(details);
            return formStore;
          });

          resolve(Object.assign({}, response, {results: formStores}));
        });
    });
  }

}

