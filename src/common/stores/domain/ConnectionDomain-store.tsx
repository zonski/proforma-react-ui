import { action, computed, observable } from 'mobx';
import { AdminApi } from '../../apis/AdminApi';
import { ConnectionSummary } from '../../models/ConnectionSummary';
import { ConnectionSummaryStore } from './ConnectionSummary-store';

export class ConnectionDomainStore {

  @observable connectionSummaryStores: ConnectionSummaryStore[] = [];

  constructor(private apis: { adminApi: AdminApi }) {}


  @computed
  get hasConnections() {
    return this.connectionSummaryStores.length > 0;
  }


  @action
  setConnectionSummaries(connectionSummaries: ConnectionSummary[]): ConnectionDomainStore {
    this.connectionSummaryStores = connectionSummaries.map(
      connectionSummary => new ConnectionSummaryStore(connectionSummary)
    );
    return this;
  }

}
