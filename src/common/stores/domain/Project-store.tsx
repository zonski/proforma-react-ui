import { action, observable } from 'mobx';
import { ProjectApi } from '../../apis/ProjectApi';
import { Project } from '../../models/Project';

export class ProjectStore {

  key: string;
  @observable name;
  @observable projectTypeKey;
  @observable projectTypeName;
  @observable smallAvatarUrl;
  @observable enabledState: 'enabled' | 'disabled' | 'loading' | 'saving' | undefined;

  constructor(private projectApi: ProjectApi, key: string) { this.key = key }

  update(details: Project) {
    this.name = details.name;
    this.projectTypeKey = details.projectTypeKey;
    this.projectTypeName = details.projectTypeName;
    this.smallAvatarUrl = details.smallAvatarUrl;
  }

  @action loadEnabledState() {
    this.enabledState = 'loading';
    this.projectApi.isProjectEnabled(this.key).then(result => {
      this.enabledState = result ? 'enabled' : 'disabled';
    });
  }

  @action toggleEnabled() {
    let newState = !(this.enabledState == 'enabled');
    this.enabledState = 'saving';
    this.projectApi.setProjectEnabled(this.key, newState).then(result => {
      this.enabledState = newState ? 'enabled' : 'disabled';
    });
  }
}

