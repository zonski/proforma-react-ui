import React from 'react';
import ReactDOM from 'react-dom';

import { createDomainStores } from '../../common/stores/domain/index';
import { createLiveApis } from '../../common/apis/live/index';
import AdminForms from './AdminForms';
import { FormApi } from '../../common/apis/FormApi';
import { FormDomainStore } from '../../common/stores/domain/FormDomain-store';
import { Provider } from 'mobx-react';

interface Apis {
  formApi: FormApi
}

interface Stores {
  formDomainStore: FormDomainStore
}

export function createAdminFormsModule(apis: Apis, stores: Stores) {
  return (
    <Provider apis={apis} stores={stores}>
      <AdminForms/>
    </Provider>
  );
}

export function loadAdminFormsModule(elementId, context, apis?: Apis, stores?: Stores) {
  apis = apis ? apis : createLiveApis(context);
  stores = stores ? stores : createDomainStores(apis);
  let targetNode = document.getElementById(elementId);
  ReactDOM.render(createAdminFormsModule(apis, stores), targetNode);
  return () => {
    if (targetNode !== null) {
      ReactDOM.unmountComponentAtNode(targetNode)
    }
  };
}

