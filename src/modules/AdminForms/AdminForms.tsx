import * as React from 'react';
import { inject, observer } from 'mobx-react';
import '../../common/styles/CommonModuleStyles.css';
import { PageHeading } from '../../common/components/heading';
import { DynamicTableStateless } from '@atlaskit/dynamic-table';
import Button, { ButtonGroup } from '@atlaskit/button';
import WatchFilledIcon from '@atlaskit/icon/glyph/watch-filled';
import EditorAddIcon from '@atlaskit/icon/glyph/editor/add';
import ListForms from '../../common/components/ListForms/ListForms';

@inject('stores')
@observer
export default class AdminForms extends React.Component {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <PageHeading actions={(<HeadingActions/>)}>
          ProForma Forms
        </PageHeading>
        <section>
          <ListForms showProjectColumn={true}/>
        </section>
      </div>
    );
  }
}


const HeadingActions = inject('apis')((props) => (
  <ButtonGroup>

    <Button href={props.apis.browserApi.createUrl('admin-template-library')}
            target="_top"
            iconBefore={<WatchFilledIcon label="View Template Library" size="small"/>}>
      Template Library F{props.apis.browserApi.getFlag('SpreadsheetDownload')}
    </Button>

    <Button href={props.apis.browserApi.createUrl('admin-create-form')}
            target="_top"
            iconBefore={<EditorAddIcon label="Create a new form" size="small"/>}>
      Create Form
    </Button>

  </ButtonGroup>
));
