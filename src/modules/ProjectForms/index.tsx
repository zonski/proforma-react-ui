import React from 'react';
import ReactDOM from 'react-dom';

import { createDomainStores } from '../../common/stores/domain/index';
import { createLiveApis } from '../../common/apis/live/index';
import ProjectForms from './ProjectForms';
import { FormApi } from '../../common/apis/FormApi';
import { FormDomainStore } from '../../common/stores/domain/FormDomain-store';
import { Provider } from 'mobx-react';
import { BrowserApi } from '../../common/apis/BrowserApi';

interface Apis {
  formApi: FormApi,
  browserApi: BrowserApi
}

interface Stores {
  formDomainStore: FormDomainStore
}

export function createProjectFormsModule(apis: Apis, stores: Stores) {
  return (
    <Provider apis={apis} stores={stores}>
      <ProjectForms/>
    </Provider>
  );
}

export function loadProjectFormsModule(elementId, context, apis?: Apis, stores?: Stores) {
  apis = apis ? apis : createLiveApis(context);
  stores = stores ? stores : createDomainStores(apis);

  let targetNode = document.getElementById(elementId);
  ReactDOM.render(createProjectFormsModule(apis, stores), targetNode);
  return () => {
    if (targetNode !== null) {
      ReactDOM.unmountComponentAtNode(targetNode)
    }
  };
}

