import * as React from 'react';
import { inject, observer } from 'mobx-react';
import '../../common/styles/CommonModuleStyles.css';
import { PageHeading } from '../../common/components/heading';
import { DynamicTableStateless } from '@atlaskit/dynamic-table';
import Button, { ButtonGroup } from '@atlaskit/button';
import WatchFilledIcon from '@atlaskit/icon/glyph/watch-filled';
import EditorAddIcon from '@atlaskit/icon/glyph/editor/add';
import ListForms from '../../common/components/ListForms/ListForms';

@inject('stores', 'apis')
@observer
export default class ProjectForms extends React.Component {

  private projectKey?: string;

  constructor(props) {
    super(props);
    this.projectKey = props.apis.browserApi.getProjectKey();
  }

  render() {
    return (
      <div>
        <PageHeading actions={(<HeadingActions projectKey={this.projectKey}/>)}>
          ProForma Forms
        </PageHeading>
        <section>
          <ListForms projectKey={this.projectKey} showProjectColumn={false}/>
        </section>
      </div>
    );
  }
}

const HeadingActions = inject('apis')((props) => (
  <ButtonGroup>

    <Button href={props.apis.browserApi.createUrl(`project-template-library?project.key=${props.projectKey}`)}
            target="_top"
            iconBefore={<WatchFilledIcon label="View Template Library" size="small"/>}>
      Template Library
    </Button>

    <Button href={props.apis.browserApi.createUrl(`project-create-form?project.key=${props.projectKey}`)}
            target="_top"
            iconBefore={<EditorAddIcon label="Create a new form" size="small"/>}>
      Create Form
    </Button>

  </ButtonGroup>
));
