import * as React from 'react';
import { observer } from 'mobx-react';
import ListProjects from './ListProjects/ListProjects';
import EnableAllProjects from './EnableAllProjects/EnableAllProjects';
import '../../../common/styles/CommonModuleStyles.css';

@observer
export default class ConfigureProjects extends React.Component<{}> {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <div>
          <h3>
            Enable ProForma for Jira Projects
          </h3>
          <p>
            ProForma can be enabled and disabled on individual projects, or on all projects in your Jira instance.
            Enabling ProForma on a project:
          </p>
          <ul>
            <li>Displays a Forms section on each issue.</li>
            <li>Allows users to add and fill out forms on those issues.</li>
            <li>Allows project administrators to design and administer custom forms.</li>
          </ul>
          <p>
            Disabling ProForma does not delete any data. Forms and data will be restored when ProForma is re-enabled on
            the project.
          </p>
        </div>
        <EnableAllProjects/>
        <ListProjects/>
      </div>
    );
  }
}
