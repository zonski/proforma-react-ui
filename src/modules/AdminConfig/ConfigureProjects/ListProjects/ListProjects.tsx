import * as React from 'react';
import { inject, observer } from 'mobx-react';
import { ToggleStateless } from '@atlaskit/toggle';
import Spinner from '@atlaskit/spinner';
import { PaginatedTable } from '../../../../common/components/PaginatedTable';
import { observable } from 'mobx';

@inject("stores")
@observer
export default class ListProjects extends React.Component {

  @observable searchStore;

  constructor(props) {
    super(props);
    this.searchStore = props.stores.projectDomainStore.startAdminSearch();
  }

  onPrev() {
    this.searchStore.prevPage();
  }

  onNext() {
    this.searchStore.nextPage();
  }

  render() {
    return (
      <div>
        <PaginatedTable
          store={this.searchStore}
          columns={tableHeaders}
          renderRow={createRow}
        />
      </div>
    );
  }
}

const tableHeaders = {
  cells: [
    {
      key: 'icon',
      content: '',
      isSortable: false,
      width: 10
    },
    {
      key: 'projectKey',
      content: 'Project Key',
      isSortable: false,
      width: 10
    },
    {
      key: 'name',
      content: 'Name',
      isSortable: false,
    },
    {
      key: 'projectTypeName',
      content: 'Project Type',
      isSortable: false,
    },
    {
      key: 'enabled',
      content: 'Enabled',
      isSortable: false,
    },
  ],
};

function createRow(project) {
  return ({
      key: `row-${project.key}`,
      cells: [
        {
          key: 'icon',
          content: (<img style={{height: '2.4em'}} src={project.smallAvatarUrl} />)
        },
        {
          key: 'projectKey',
          content: project.key
        },
        {
          key: 'name',
          content: project.name
        },
        {
          key: 'projectTypeName',
          content: project.projectTypeName
        },
        {
          key: 'enabled',
          content: (<EnableToggle project={project} />)
        }
      ]
    });
}

const EnableToggle = observer(({project}) => {
  switch (project.enabledState) {
    case 'enabled':
    case 'disabled':
      return (<ToggleStateless isChecked={project.enabledState === 'enabled'}
                               onChange={() => project.toggleEnabled()}/>);
    default:
      return (<Spinner size="small"/>);
  }
});