import * as React from 'react';
import { inject, observer } from 'mobx-react';
import Button, { ButtonGroup } from '@atlaskit/button';
import Spinner from '@atlaskit/spinner';
import { ProjectDomainStore } from '../../../../common/stores/domain/ProjectDomain-store';

@inject("stores")
@observer
export default class EnableAllProjects extends React.Component {

  private store: ProjectDomainStore;

  constructor(props) {
    super(props);
    this.store = props.stores.projectDomainStore;
  }

  enableAll() {
    this.store.setAllProjectsEnabled(true);
  }

  disableAll() {
    this.store.setAllProjectsEnabled(false);
  }

  render() {
    const {
      enablingAll,
    } = this.store;

    return (
      <div>
        <section>
          <ButtonGroup>
            <Button appearance={'primary'} onClick={()=> this.enableAll()} isDisabled={enablingAll}>Enable all</Button>
            <Button appearance={'primary'} onClick={()=> this.disableAll()} isDisabled={enablingAll}>Disable all</Button>
          </ButtonGroup>
          { enablingAll ? (<Spinner/>) : '' }
        </section>
      </div>
    );
  }
}
