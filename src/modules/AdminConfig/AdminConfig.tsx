import * as React from 'react';
import { observer } from 'mobx-react';
import '../../common/styles/CommonModuleStyles.css';
import ConfigureProjects from './ConfigureProjects/ConfigureProjects';
import ConfigureIssueForms from './ConfigureIssueForms/ConfigureIssueForms';
import { PageHeading } from '../../common/components/heading';

@observer
export default class AdminConfig extends React.Component<{}> {

  constructor(props) {
    super(props);
  }

  render() {
    return (
      <div>
        <PageHeading>ProForma Configuration</PageHeading>
        <section>
          <ConfigureIssueForms/>
        </section>
        <section>
          <ConfigureProjects/>
        </section>
      </div>
    );
  }
}
