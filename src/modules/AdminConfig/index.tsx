import React from 'react';
import ReactDOM from 'react-dom';

import { ProjectApi } from '../../common/apis/ProjectApi';
import { ProjectDomainStore } from '../../common/stores/domain/ProjectDomain-store';
import { createDomainStores } from '../../common/stores/domain/index';
import { createLiveApis } from '../../common/apis/live/index';
import AdminConfig from './AdminConfig';
import { AdminApi } from "../../common/apis/AdminApi";
import { Provider } from 'mobx-react';
import { AdminSettingsDomainStore } from '../../common/stores/domain/AdminSettingsDomain-store';

interface Apis {
  adminApi: AdminApi,
  projectApi: ProjectApi
}

interface Stores {
  adminSettingsDomainStore: AdminSettingsDomainStore,
  projectDomainStore: ProjectDomainStore
}

export function createAdminConfigModule(apis: Apis, stores: Stores) {
  return (
    <Provider apis={apis} stores={stores}>
      <AdminConfig/>
    </Provider>
  );
}

export function loadAdminConfigModule(elementId, context, apis?: Apis, stores?: Stores) {
  apis = apis ? apis : createLiveApis(context);
  stores = stores ? stores : createDomainStores(apis);
  let targetNode = document.getElementById(elementId);
  ReactDOM.render(createAdminConfigModule(apis, stores), targetNode);
  return () => {
    if (targetNode !== null) {
      ReactDOM.unmountComponentAtNode(targetNode)
    }
  };
}

