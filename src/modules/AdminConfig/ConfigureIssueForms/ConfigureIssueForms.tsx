import * as React from 'react';
import { inject, observer } from 'mobx-react';
import Spinner from '@atlaskit/spinner';
import { ToggleStateless } from '@atlaskit/toggle';
import { AdminSettingsDomainStore } from '../../../common/stores/domain/AdminSettingsDomain-store';

@inject("stores")
@observer
export default class ConfigureIssueForms extends React.Component {

  store: AdminSettingsDomainStore;

  constructor(props) {
    super(props);
    this.store = props.stores.adminSettingsDomainStore;
    this.store.loadEnabledState();
  }

  toggleEnabled() {
    this.store.toggleEnabled();
  }

  render() {
    const pStyle = {
      "lineHeight": "30px"
    };

    return (
      <div>
        <h3>Issue Forms</h3>
        <p>
          If enabled, users will be able to select <i>Issue forms</i> from the Jira navigation bar. This will allow the user to create new Jira issues using ProForma forms.
        </p>
        <div style={pStyle}>
          Enable Issue Forms: {this.renderToggle()}
        </div>
      </div>
    );
  }

  private renderToggle() {
    if(this.store.loading) {
      return (
        <Spinner size="small" />
      );
    } else {
      return (
        <ToggleStateless isChecked={this.store.enabled} onChange={() => this.toggleEnabled()} />
      );
    }
  }
}
