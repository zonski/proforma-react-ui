import * as React from 'react';
import { observer } from 'mobx-react';
import { DemoStore } from './Demo-store';

@observer
export default class Demo extends React.Component<{store: DemoStore}> {

  private store: DemoStore;

  constructor(props) {
    super(props);
    this.store = props.store;
  }

  render() {
    return (
      <div>
        <h1>Demo Module</h1>
        <p>
          A demo module just for fun
        </p>
      </div>
    );
  }
}
