import React from 'react';
import ReactDOM from 'react-dom';

import Demo from './Demo';
import { DemoStore } from './Demo-store';

export interface Apis {
}

export interface DomainStores {
}

export function createDemoModule(apis: Apis, domainStores: DomainStores) {
  let store = new DemoStore(apis, domainStores);
  return (
    <Demo store={store}/>
  );
}

export function loadDemoModule(apis: Apis, domainStores: DomainStores, elementId) {
  ReactDOM.render(createDemoModule(apis, domainStores), document.getElementById(elementId));
}

