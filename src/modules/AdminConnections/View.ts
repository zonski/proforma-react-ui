export enum View {
  Loading,
  ConnectionList,
  AddConnection,
  EditConnection
}
