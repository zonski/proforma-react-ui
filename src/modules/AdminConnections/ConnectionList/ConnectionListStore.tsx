import { computed } from "mobx";
import { AdminApi } from "../../../common/apis/AdminApi";
import { ConnectionDomainStore } from '../../../common/stores/domain/ConnectionDomain-store';

export enum View {
  EmptyState,
  ConnectionTable
}

export class ConnectionListStore{

  connectionDomainStore: ConnectionDomainStore;

  constructor(
    private apis: {
      adminApi: AdminApi
    },
    private domainStores: {
      connectionDomainStore: ConnectionDomainStore
    }) {
    this.connectionDomainStore = domainStores.connectionDomainStore;
  }

  @computed
  get view(): View {
    if (this.connectionDomainStore.hasConnections) {
      return View.ConnectionTable;
    } else {
      return View.EmptyState;
    }
  }

}
