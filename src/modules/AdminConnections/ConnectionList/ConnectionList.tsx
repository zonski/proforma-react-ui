import Button, { ButtonGroup } from '@atlaskit/button';
import DynamicTable from '@atlaskit/dynamic-table';
import Lozenge from '@atlaskit/lozenge';
import { ToggleStateless } from '@atlaskit/toggle';
import { computed } from 'mobx';
import { observer } from 'mobx-react';
import * as React from 'react';
import { ConnectionStatus } from '../../../common/models/ConnectionSummary';
import { ConnectionEmptyState } from './ConnectionEmptyState';
import { ConnectionListStore, View } from './ConnectionListStore';
import { PageHeading } from '../../../common/components/heading';

@observer
export default class ConnectionList extends React.Component<{store: ConnectionListStore}> {

  private store: ConnectionListStore;

  constructor(props) {
    super(props);
    this.store = props.store;
  }

  private addConnection = (e: React.MouseEvent<HTMLButtonElement>) => {
    // TODO: implement add connection UI
  };

  tableHeaders = {
    cells: [
      {
        key: 'name',
        content: 'Name',
        isSortable: true,
        width: 20
      },
      {
        key: 'source',
        content: 'Source',
        isSortable: true,
        width: 50
      },
      {
        key: 'status',
        content: 'Status',
        isSortable: true,
        width: 8
      },
      {
        key: 'actions',
        content: '',
        isSortable: false,
        width: 19
      }
    ],
  };

  @computed
  get rows() {
    return this.store.connectionDomainStore.connectionSummaryStores.map(
      connectionSummary => (
        {
          key: `row-${connectionSummary.id}`,
            cells: [
          {
            key: 'name',
            content: connectionSummary.name,
          },
          {
            key: 'source',
            content: connectionSummary.source,
          },
          {
            key: 'status',
            content: <React.Fragment>
              {connectionSummary.status == ConnectionStatus.ok && (
                <Lozenge appearance="success">OK</Lozenge>
              )}
              {connectionSummary.status == ConnectionStatus.failing && (
                <Lozenge appearance="removed">Failing</Lozenge>
              )}
            </React.Fragment>,
          },
          {
            key: 'actions',
            content: <ButtonGroup>
              <Button spacing="compact">Edit</Button>
              <Button spacing="compact">Test</Button>
              <Button spacing="compact">Delete</Button>
            </ButtonGroup>,
          }
        ]
        }
      )
    )
  }


  render() {
    const { view } = this.store;

    return (
      <div>
        <PageHeading>ProForma Data Connections</PageHeading>
        <section>
          {view == View.EmptyState && (
            <ConnectionEmptyState addConnection={this.addConnection} />
          )}

          {view == View.ConnectionTable && (
            <DynamicTable
              head={this.tableHeaders}
              rows={this.rows}
              rowsPerPage={10}
              defaultPage={1}
              loadingSpinnerSize="large"
              isLoading={false}
              isFixedSize
              defaultSortKey="name"
              defaultSortOrder="ASC"
              onSort={() => console.log('onSort')}
              onSetPage={() => console.log('onSetPage')}
            />
          )}
        </section>
      </div>
    );
  }

}
