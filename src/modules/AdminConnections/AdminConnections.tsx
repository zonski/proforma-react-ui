import Spinner from '@atlaskit/spinner';
import { observer } from 'mobx-react';
import * as React from 'react';
import '../../common/styles/CommonModuleStyles.css';
import { AdminConnectionsStore } from './AdminConnectionsStore';
import ConnectionList from './ConnectionList/ConnectionList';
import { View } from './View';

@observer
export default class AdminConnections extends React.Component<{ store: AdminConnectionsStore }> {

  private store: AdminConnectionsStore;

  constructor(props) {
    super(props);
    this.store = props.store;
  }


  render() {
    const { view } = this.store;

    switch (view) {
      case View.Loading:
        return (
          <h3><Spinner/> Loading...</h3>
        );
      case View.ConnectionList:
        return (
          <ConnectionList store={this.store.connectionListStore} />
        );
      default:
        return null;
    }
  }
}
