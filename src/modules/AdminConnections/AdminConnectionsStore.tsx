import { observable } from 'mobx';
import { AdminApi } from "../../common/apis/AdminApi";
import { ConnectionDomainStore } from '../../common/stores/domain/ConnectionDomain-store';
import { ConnectionListStore } from './ConnectionList/ConnectionListStore';
import { View } from './View';

export class AdminConnectionsStore {

  @observable
  view: View;

  adminApi: AdminApi;
  connectionDomainStore: ConnectionDomainStore;

  connectionListStore: ConnectionListStore;

  constructor(
    private apis: {
      adminApi: AdminApi
    },
    private domainStores: {
      connectionDomainStore: ConnectionDomainStore
    }) {


    this.view = View.Loading;

    this.connectionDomainStore = domainStores.connectionDomainStore;
    this.adminApi = apis.adminApi;

    this.connectionListStore = new ConnectionListStore(apis, domainStores);

    this.start();
  }


  start() {
    // Load the list of connection summaries from the backend when starting up
    this.adminApi.getConnectionSummaries().then(
      connectionSummaries => {
        this.connectionDomainStore.setConnectionSummaries(connectionSummaries);

        // Now that the data has been loaded, show the list of connections
        this.view = View.ConnectionList;
      }
    )
  }


}
