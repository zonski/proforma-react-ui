import React from 'react';
import ReactDOM from 'react-dom';
import { AdminApi } from "../../common/apis/AdminApi";
import { createLiveApis } from '../../common/apis/live/index';

import { ProjectApi } from '../../common/apis/ProjectApi';
import { ConnectionDomainStore } from '../../common/stores/domain/ConnectionDomain-store';
import { createDomainStores } from '../../common/stores/domain/index';
import AdminConnections from './AdminConnections';
import { AdminConnectionsStore } from './AdminConnectionsStore';

export interface AdminConnectionsApis {
  adminApi: AdminApi,
  projectApi: ProjectApi
}

export interface AdminConnectionsDomainStores {
  connectionDomainStore: ConnectionDomainStore
}

export function createAdminConnectionsModule(apis: AdminConnectionsApis, domainStores: AdminConnectionsDomainStores) {
  let store = new AdminConnectionsStore(apis, domainStores);
  return (
    <AdminConnections store={store}/>
  );
}

export function loadAdminConnectionsModule(elementId, context, apis?: AdminConnectionsApis, domainStores?: AdminConnectionsDomainStores) {
  apis = apis ? apis : createLiveApis(context);
  domainStores = domainStores ? domainStores : createDomainStores(apis);
  let targetNode = document.getElementById(elementId);
  ReactDOM.render(createAdminConnectionsModule(apis, domainStores), targetNode);
  return () => {
    if (targetNode !== null) {
      ReactDOM.unmountComponentAtNode(targetNode)
    }
  };
}

