import React, { Component } from 'react';
import { PageHeading } from '../../../common/components/heading';

export const TypographyDemo = () => (
  <div className="demo-section">
    <h2>PageHeading</h2>
    <section>
      <PageHeading>An example page heading</PageHeading>
    </section>
  </div>
);
