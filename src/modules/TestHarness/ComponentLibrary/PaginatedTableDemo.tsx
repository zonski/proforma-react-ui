import React, { Component } from 'react';
import { Grid, GridColumn } from '@atlaskit/page';
import { PagedResults } from '../../../common/models/PagedResults';
import { MockSearchHelper } from '../../../common/apis/mock/MockSearchHelper';
import { PaginatedTable } from '../../../common/components/PaginatedTable';
import { SearchStore } from '../../../common/stores/domain/Search-store';

export class PaginatedTableDemo extends Component<{}, {}> {

  render() {
    return (
      <div className="demo-section">
        <h2>PaginatedTable</h2>
        <section>
          <PaginatedTable
            store={new ExamplePaginatedTableStore()}
            columns={tableHeaders}
            renderRow={createRow}
          />
        </section>
      </div>
    )
  }
}

class ExamplePaginatedTableStore extends SearchStore<any> {

  private data = createData();
  private mockSearchHelper;

  constructor() {
    super();
    this.mockSearchHelper = new MockSearchHelper();
    this.startSearch();
  }

  doSearch(pageSize: number, cursor?: string, sortKey?: string, sortOrder?: string): Promise<PagedResults<any>> {
    return this.mockSearchHelper.search(() => this.data, pageSize, cursor);
  }
}

const tableHeaders = {
  cells: [
    {
      key: 'id',
      content: 'ID',
      isSortable: false,
      width: 10
    },
    {
      key: 'name',
      content: 'Name',
      isSortable: false,
    },
    {
      key: 'value1',
      content: 'Value 1',
      isSortable: false,
    },
    {
      key: 'value2',
      content: 'Value 2',
      isSortable: false,
    }
  ],
};

function createRow(item) {
  return ({
    key: `row-${item.id}`,
    cells: [
      {
        key: 'id',
        content: item.id
      },
      {
        key: 'name',
        content: item.name
      },
      {
        key: 'value1',
        content: item.value1
      },
      {
        key: 'value2',
        content: item.value2
      }
    ]
  });
}

const createData = () => {
  let data: any[] = [];
  for (let i = 1; i < 96; i++) {
    data.push({
      id: i, name: `Item ${i}`, value1: `Value 1 for item ${i}`, value2: `Value 2 for item ${i}`
    });
  }
  return data;
};
