import React, { Component } from 'react';
import { Grid, GridColumn } from '@atlaskit/page';
import { TypographyDemo } from './TypographyDemo';
import { PaginatedTableDemo } from './PaginatedTableDemo';
import { IconDemo } from './IconDemo';

export class ComponentLibrary extends Component<{}, {}> {

  render() {
    return (
      <div>
        <h1>Proforma UI Component Library</h1>
        <section>
          <p>
            These are the common components used in the Proforma UI. Try to use either
            raw <a href="https://atlaskit.atlassian.com/packages">AtlasKit Components</a> or the components below before
            creating new ones or adding custom styling. If you do need to do some custom styling, check whether this
            should be a new common component.
          </p>
        </section>
        <IconDemo/>
        <TypographyDemo/>
        <PaginatedTableDemo/>
      </div>
    )
  }
}
