import React, { Component } from 'react';
import {
  BlankFormIcon,
  FinanceIcon,
  HRIcon,
  ITIcon,
  LegalIcon,
  MarketingIcon,
  OperationsIcon,
  ProcurementIcon,
  ProformaLogoIcon,
  ProjectManagementIcon,
  RiskAndHazardIcon
} from '../../../common/components/icons';
import styled from 'styled-components';


const IconGrid = styled.div`
  display: flex;
	flex-wrap: wrap;
`;

export const IconDemo = () => (
  <div className="demo-section">
    <h2>Icons</h2>
    <section>
      <IconGrid>
        <Icon name={'ProformaLogoIcon'} icon={<ProformaLogoIcon/>}/>
        <Icon name={'BlankFormIcon'} icon={<BlankFormIcon/>}/>
        <Icon name={'FinanceIcon'} icon={<FinanceIcon/>}/>
        <Icon name={'HRIcon'} icon={<HRIcon/>}/>
        <Icon name={'ITIcon'} icon={<ITIcon/>}/>
        <Icon name={'OperationsIcon'} icon={<OperationsIcon/>}/>
        <Icon name={'LegalIcon'} icon={<LegalIcon/>}/>
        <Icon name={'MarketingIcon'} icon={<MarketingIcon/>}/>
        <Icon name={'ProcurementIcon'} icon={<ProcurementIcon/>}/>
        <Icon name={'ProjectManagementIcon'} icon={<ProjectManagementIcon/>}/>
        <Icon name={'RiskAndHazardIcon'} icon={<RiskAndHazardIcon/>}/>
      </IconGrid>
    </section>
  </div>
);


const IconCell = styled.div`
  margin: 5px;
  padding: 5px;
  border: 1px solid lightgrey;
  text-align: center;
  > * {
    padding: 5px;
  } 
`;

export const Icon = ({name, icon}) => (
  <IconCell >
    <div>{name}</div>
    <div>{icon}</div>
  </IconCell>
);