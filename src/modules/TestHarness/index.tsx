import React from 'react';
import ReactDOM from 'react-dom';
import { TestHarness } from './TestHarness';

export function loadTestHarness(elementId) {
  ReactDOM.render(<TestHarness />, document.getElementById(elementId));
}

