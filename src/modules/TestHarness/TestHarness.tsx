// import '@atlaskit/css-reset';
import Page, { Grid, GridColumn } from '@atlaskit/page';
import React, { Component } from 'react';
import { BrowserRouter as Router, Link, NavLink, Redirect, Route, Switch, withRouter } from 'react-router-dom'
import styled from 'styled-components';
import { createMockApis } from '../../common/apis/mock/index';
import { createDomainStores } from '../../common/stores/domain/index';
import { createAdminConfigModule } from '../AdminConfig/index';
import { createAdminConnectionsModule } from '../AdminConnections';
import { createAdminFormsModule } from '../AdminForms/index';
import { createProjectFormsModule } from '../ProjectForms/index';
import { createDemoModule } from '../Demo/index';
import { ComponentLibrary } from './ComponentLibrary/ComponentLibrary';
import './TestHarness-styles.css';
import { action, observable } from 'mobx';
import { MockData } from '../../common/apis/mock/data/MockData';
import { loadTypicalMockData } from '../../common/apis/mock/data/typical';
import { observer } from 'mobx-react';


const modules = [
  {
    name: 'Demo',
    route: 'demo',
    create: createDemoModule,
    description: 'A demo module for testing'
  },
  {
    name: 'AdminConfig',
    route: 'admin-config',
    create: createAdminConfigModule,
    description: 'The Admin Configuration page (i.e. enable projects, etc)'
  },
  {
    name: 'AdminConnections',
    route: 'admin-connections',
    create: createAdminConnectionsModule,
    description: 'The Admin data connection configuration page'
  },
  {
    name: 'AdminForms',
    route: 'admin-forms',
    create: createAdminFormsModule,
    description: 'The Admin page for configuring Forms'
  },
  {
    name: 'ProjectForms',
    route: 'project-forms',
    create: createProjectFormsModule,
    description: 'The Project page for configuring Forms',
    projectPage: true
  },
];

@observer
export class TestHarness extends React.Component {

  @observable harness: {
    dataSetType: string,
    loading: boolean,
    mockData: MockData,
    updateMockData: (type: string) => void,
    updateProjectKey: (projectKey: string) => void
};

  apis;
  stores;

  constructor(props) {
    super(props);

    this.harness = {
      mockData: new MockData(),
      dataSetType: 'typical',
      loading: false,
      updateMockData: (type: string) => { this.updateMockData(type) },
      updateProjectKey: (projectKey: string) => { this.updateProjectKey(projectKey) }
    };

    this.apis = createMockApis(this.harness.mockData);
    this.stores = createDomainStores(this.apis);

    this.updateMockData('typical');
  }

  @action updateMockData(type: string) {
    console.log('Loading mock data: ' + type);
    this.harness.dataSetType = type;
    this.harness.loading = true;
    this.harness.mockData.clear();
    switch (type) {

      case 'typical':
        loadTypicalMockData(this.harness.mockData);
        break;

      case 'empty':
        break;
    }
    if (this.harness.mockData.projects.length) {
        this.updateProjectKey(this.harness.mockData.projects[0].key);
    }
    setTimeout(() => {
      this.harness.loading = false;
    }, 0);
  }

  @action updateProjectKey(projectKey: string) {
    console.log('Setting main project key to: ' + projectKey);
    this.harness.loading = true;
    this.apis.browserApi.projectKey = projectKey;
    setTimeout(() => {
      this.harness.loading = false;
    }, 0);
  }

  render() {
    return (
      <Router>
        <div>
          <NavBar />
          <ModuleWrapper harness={this.harness}
                         apis={this.apis}
                         stores={this.stores}/>
        </div>
      </Router>
    );
  }
}

const NavBar = () => {
  return (
    <div className="topnav">
      <Page>
        <Grid>
          <span className="brand">ProForma UI Test Harness</span>
          <NavLink activeClassName="active" to="/modules">Core Modules</NavLink>
          <NavLink activeClassName="active" to="/components">UI Components</NavLink>
        </Grid>
      </Page>
    </div>
  );
};

const ModuleWrapper = withRouter(observer(({ harness, apis, stores }) => {
  return (
    <Page>
      <Grid>
        <GridColumn medium={12}>
          <Switch>
            <Route exact path="/" render={() => (<Redirect to="/modules" />)} />
            <Route path='/components' component={ComponentLibrary} />
            <Route exact path='/modules' render={() => <IndexPage/>} />
            {modules.map(m => (
              <Route key={m.route} path={'/modules/' + m.route}
                     render={() => (
                       <ModuleContent harness={harness}
                                      apis={apis}
                                      stores={stores}
                                      module={m} />
                     )} />
            ))}
            <Route component={withRouter(NoMatch)} />
          </Switch>
        </GridColumn>
      </Grid>
    </Page>
  );
}));

const ModuleToolbar = styled.div`
  background-color: #eee;
  border: 1px solid lightgrey;
  border-radius: 5px;
  padding: 10px;
  margin: 0 -20px 40px -20px;  
  display: flex;
  justify-content: space-between;
`;


const ModuleContent = withRouter(observer((props) => {
  const {
    harness,
    apis,
    stores,
    module
  } = props;

  return (
    <div>
      <ModuleToolbar>
        <b>{module.name}</b>

        <div>
          <span>Mock data: </span>
          <select value={harness.dataSetType} onChange={(e) => harness.updateMockData(e.target.value)}>
            <option value="typical">Typical</option>
            <option value="empty">Empty</option>
          </select>
        </div>

        {module.projectPage &&
        <div>
          <span>Project: </span>
          <select value={apis.browserApi.projectKey} onChange={(e) => harness.updateProjectKey(e.target.value)}>
            { harness.mockData.projects.map( p => (<option key={p.key} value={p.key}>{p.name}</option>))}
          </select>
        </div>}

        <div>
          <Link to={'/modules'}>Close</Link>
        </div>

      </ModuleToolbar>

      {harness.loading && <div>Loading...</div>}

      {
        !harness.loading &&
        <div>
          {module.create(apis, stores)}
        </div>
      }

    </div>
  );
}));

const IndexPage = (props) => {
  return (
    <div>
      <h1>ProForma Core Modules</h1>
      <section>
        <table>
          <thead>
            <tr>
              <th>Module</th>
              <th>Route</th>
              <th>Description</th>
            </tr>
          </thead>
          <tbody>
            {modules.map(m => (
              <tr key={m.route}>
                <td><Link to={'/modules/' + m.route}>{m.name}</Link></td>
                <td>/module/{m.route}</td>
                <td>{m.description}</td>
              </tr>
            ))}
          </tbody>
        </table>
      </section>
    </div>
  );
};

const NoMatch = (props) => {
  return (
    <div>
      <h1>Module Doesn't Exist</h1>
      <section>
        <b>{props.location.pathname}</b> was not found
      </section>
    </div>
  );
};