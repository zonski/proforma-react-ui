import styled, { css } from 'styled-components';

import { colors } from '@atlaskit/theme';

export const SettingsPanel = styled.div`
  width: 100%;
  max-width: 600px;
  margin-top: 1.4rem;
  padding: 0 1.4rem 1.4rem;
  box-sizing: border-box;
`;

export const H700 = styled.h1`
  color: ${colors.N800};
  font-size: 2.4rem;
  font-weight: 500;
  line-height: 2.8rem;
  letter-spacing: -0.01rem;
  margin-top: 4rem;
`;

export const H600 = styled.h2`
  color: ${colors.N800};
  font-size: 2rem;
  line-height: 2.4remm;
  font-weight: 500;
  letter-spacing: -0.008rem;
  margin-top: 2.8rem;
`;

export const H500 = styled.h3`
  color: ${colors.N800};
  font-size: 1.6rem;
  line-height: 2rem;
  font-weight: 600;
  letter-spacing: -0.006rem;
  margin-top: 2.4rem;
`;

export const H400 = styled.h4`
  color: ${colors.N800};
  font-size: 1.4rem;
  line-height: 1.6rem;
  font-weight: 600;
  letter-spacing: -0.003rem;
  margin-top: 1.6rem;
`;

interface SectionParagraphProps {
  noMargin?: boolean;
  autoWidth?: boolean;
}

export const SectionParagraph = styled.p`
    font-size: 1.4rem;
    font-weight: 400;
    line-height: 2rem;
    letter-spacing: -0.005rem;
    color: ${colors.N800};
    margin: ${(props: SectionParagraphProps) => props.noMargin ? '0' : '1.2rem 0 0'};
    width: ${(props: SectionParagraphProps) => props.autoWidth ? 'auto' : '100%'};
`;

interface ParagraphLinkProps {
  btnStyle?: boolean;
  paragraphStyle?: boolean;
  isStart?: boolean;
  isEnd?: boolean;
  isActive?: boolean;
  isDisabled?: boolean;
}

export const ParagraphLink = styled.a`
  font-size: 1.4rem;
  font-weight: 400;
  margin: 0 0.4rem;
  line-height: 2rem;
  letter-spacing: -0.005rem;
  text-decoration: none;
  color: ${colors.B400};
  cursor: pointer;

  &:hover {
    text-decoration: underline;
  }

  /* Configurable Props */
  ${(props: ParagraphLinkProps) => props.btnStyle && css`
    display: flex;
    align-items: center;
    height: 100%;
    background: none;
    border: none;
    padding: 0.4rem;
  `}

  ${(props: ParagraphLinkProps) => props.paragraphStyle && css`
    display: flex;
    align-items: center;
    background: none;
    border: none;
    padding: 0.4rem;
  `}

  ${(props: ParagraphLinkProps) => props.isStart && css`
    margin-left: 0;
  `}

  ${(props: ParagraphLinkProps) => props.isEnd && css`
    margin-right: 0;
  `}

  ${(props: ParagraphLinkProps) => props.isActive && css`
    font-weight: 700;
    color: ${colors.N800};
    cursor: default;

    &:hover {
      text-decoration: none;
    }
  `}

  ${(props: ParagraphLinkProps) => props.isDisabled && css`
    font-weight: 400;
    color: ${colors.N40};
    cursor: not-allowed;

    &:hover {
      text-decoration: none;
    }
  `}
`;

export const AtlaskitBtnGroup = styled.div`
  display: flex;

  > div,
  > a,
  > button {
    margin-left: 0.4rem;

    &:first-child {
      margin: 0;
    }
  }
`;
