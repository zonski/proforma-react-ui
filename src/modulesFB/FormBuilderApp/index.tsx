import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react';

// Utils
import { checkCanIgnoreDoNotTrack } from '../../common/utils/analyticsUtils';

// Models
import { ServerSettings } from '../../common/apis/live';
import { FormBuilderAppStore } from './FormBuilderApp-models';

// Stores
import { FormBuilderAppLiveStore } from './FormBuilderApp-store';

// Module
import FormBuilderApp from './FormBuilderApp';
import { setupGlobalErrorHandling } from '../../common/utils/errorHandlingUtils';

export const createFormBuilderModule = (formBuilderAppStore) => {
  return (
    <Provider formBuilderAppStore={formBuilderAppStore}>
      <FormBuilderApp />
    </Provider>
  );
};

export const loadFormBuilderModule = (
  elementId: string,
  serverSettings: ServerSettings,
  moduleStore?: FormBuilderAppStore,
) => {
  // Setup any global Utils with dependencies on static serverSettings
  setupGlobalErrorHandling(
    serverSettings.sen,
    serverSettings.debugLvl,
    serverSettings.analyticsEnabled,
  );

  const formBuilderAppStore = (
    moduleStore ||
    new FormBuilderAppLiveStore(serverSettings, !checkCanIgnoreDoNotTrack())
  );

  const targetNode = document.getElementById(elementId);

  ReactDOM.render(
    createFormBuilderModule(formBuilderAppStore),
    targetNode,
  );

  return () => {
    if (targetNode !== null) {
      ReactDOM.unmountComponentAtNode(targetNode)
    }
  };
};
