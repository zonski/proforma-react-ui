// Models
import { PermissionLevel } from '../../common/apis/live/index';
import { ConfigureStore } from './components/Configure/Configure-models';

// TODO extend smaller purpose specific interfaces to result in ths interface
export interface FormBuilderAppStore {
  apiBaseUrl: string;
  buildUrl: string;
  closeUrl: string;
  licenseLevel: string;
  locale: string;
  permissionLevel: PermissionLevel;
  projectFormId: string;
  projectKey: string;
  tz: string;
  doNotTrack: boolean;
  segmentWriteKey?: string;
  token?: string;

  // Page Stores
  configureStore: ConfigureStore;

  // Methods
  updateToken: () => Promise<void>;
}
