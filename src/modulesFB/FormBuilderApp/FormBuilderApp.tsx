// NPM Modules
import * as React from 'react';
import { inject, observer } from 'mobx-react';

// Atlaskit Components
import Tabs from '@atlaskit/tabs';
import { default as AtlaskitBtn } from '@atlaskit/button';

// Error Handling
import ErrorBoundary from '../../common/components/ErrorBoundary/ErrorBoundary';

// Models
import { FormBuilderAppStore } from './FormBuilderApp-models';

// Pages
import Configure from './components/Configure/Configure';
// DISABLED PLACEHOLDER FOR WHEN BUILD PAGE DEV BEGINS
// import FormBuilder from './components/FormBuilder/FormBuilder';

// Styles
import './replace-with-styled-components.css';
import {
  FormBuilderAppWrapper,
  ActionsBar,
} from './FormBuilderApp-styles';

export interface FormBuilderAppProps {
  formBuilderAppStore?: FormBuilderAppStore;
}

export interface TabsList {
  label: 'Settings' | 'Build';
  content?: React.ReactNode;
  href?: string;
}

@inject('formBuilderAppStore')
@observer
export default class FormBuilderApp extends React.Component<FormBuilderAppProps, {}> {
  tokenTimer?: NodeJS.Timer;

  constructor(props) {
    super(props);

    if (!this.props.formBuilderAppStore) {
      return;
    }

    // Destructuring store props
    const {
      formBuilderAppStore,
      formBuilderAppStore: {
        token,
      },
    } = this.props;

    // Set a timer every 15 minutes to update the current token as this page is read-only
    if (token) {
      this.tokenTimer = setInterval(() => formBuilderAppStore.updateToken(), 900000);
    }
  }

  componentWillUnmount() {
    // Destroy the timer
    if (this.tokenTimer) {
      clearInterval(this.tokenTimer);
    }
  }

  handleTabOnSelect = (e) => {
    if (e && typeof e.href !== 'undefined') {
      window.top.location.href = e.href;
    }
  }

  render() {
    if (!this.props.formBuilderAppStore) {
      return;
    }

    const {
      buildUrl,
      closeUrl,
      configureStore,
      doNotTrack,
      segmentWriteKey,
      token,
    } = this.props.formBuilderAppStore;

    const tabsList = [
      {
        label: 'Settings',
        content: (
          <Configure
            configureStore={configureStore}
            doNotTrack={doNotTrack}
            segmentWriteKey={segmentWriteKey}
            jwtToken={token}
          />
        ),
      },
      {
        label: 'Build',
        // Replace the href with content and FormBuilder component when developing form builder page
        href: buildUrl,
      },
    ];

    return (
      <ErrorBoundary>
        <FormBuilderAppWrapper id="content" className="pf-builder__app">
          <Tabs tabs={tabsList} onSelect={this.handleTabOnSelect} />
          <ActionsBar>
            <AtlaskitBtn
              href={closeUrl}
              target="_top"
              appearance="link"
            >
              Close
            </AtlaskitBtn>
          </ActionsBar>
        </FormBuilderAppWrapper>
      </ErrorBoundary>
    );
  }
}
