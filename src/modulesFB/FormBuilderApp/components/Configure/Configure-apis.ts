// Error Handling
import { throwNewApiError } from '../../../../common/utils/errorHandlingUtils';

// Models
import {
  ConfigureApis,
  FormSettingsData,
  FetchApiSettings,
  AutomationRule,
  ReferenceData,
  AutomationRuleNoId,
} from './Configure-models';

export class ConfigureLiveApis implements ConfigureApis {
  private apiBaseUrl: string;
  private projectFormId: string;
  private projectKey: string;

  private fetchApi = async ({ method, path, token, body }: FetchApiSettings): Promise<any> => {
    const init: RequestInit = {
      method,
      credentials: 'same-origin',
      headers: {
        ...(token && { Authorization: `Bearer ${token}` }),
        'Content-Type': 'application/json',
      },
      ...(body && { body: JSON.stringify(body) }),
    };

    try {
      const response = await fetch(this.apiBaseUrl + path, init);

      if (response.ok) {
        return response.json();
      }

      // Response was not ok so dive into the catch block to handle what went wrong
      throw response;
    } catch (error) {
      const contentType = error.headers ? error.headers.get('content-type') : '';
      const errorDetails = /^application\/json/.test(contentType.toLowerCase()) && (await error.json());
      return throwNewApiError(errorDetails, error);
    }
  }

  constructor(apiBaseUrl: string, projectFormId: string, projectKey: string) {
    this.apiBaseUrl = apiBaseUrl;
    this.projectFormId = projectFormId;
    this.projectKey = projectKey;
  }

  public getFormSettings = (token?: string): Promise<FormSettingsData> =>
    this.fetchApi({
      token,
      method: 'GET',
      path: `/api/1/projects/${this.projectKey}/forms/${this.projectFormId}/configuration`,
    })

  public getAutomationRules = (token?: string): Promise<AutomationRule[]> =>
    this.fetchApi({
      token,
      method: 'GET',
      path: `/api/1/projects/${this.projectKey}/forms/${this.projectFormId}/automation`,
    })

  public getReferenceData = (token?: string): Promise<ReferenceData> =>
    this.fetchApi({
      token,
      method: 'GET',
      path: `/api/1/projects/${this.projectKey}/forms/reference`,
    })

  public postFormSettings = (formSettings: FormSettingsData, token?: string): Promise<any> =>
    this.fetchApi({
      token,
      method: 'POST',
      path: `/api/1/projects/${this.projectKey}/forms/${this.projectFormId}/configuration`,
      body: formSettings,
    })

  public postAutomationRule = (
    newRuleData: AutomationRuleNoId,
    token?: string,
  ): Promise<AutomationRule> =>
    this.fetchApi({
      token,
      method: 'POST',
      path: `/api/1/projects/${this.projectKey}/automation`,
      body: newRuleData,
    })

  public putAutomationRule = (ruleData: AutomationRule, token?: string): Promise<any> =>
    this.fetchApi({
      token,
      method: 'PUT',
      path: `/api/1/projects/${this.projectKey}/automation/${ruleData.id}`,
      body: ruleData,
    })

  public deleteAutomationRule = (ruleId: string, token?: string): Promise<any> =>
    this.fetchApi({
      token,
      method: 'DELETE',
      path: `/api/1/projects/${this.projectKey}/automation/${ruleId}`,
    })
}
