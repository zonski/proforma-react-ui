import { observable, action } from 'mobx';

// Models
import { FormBuilderAppStore } from '../../FormBuilderApp-models';

import {
  ManageAutomationRulesStore,
} from '../../../../common/components/ManageAutomationRules/ManageAutomationRules-models';

import {
  ConfigureApis,
  ConfigureStore,
  AutomationRule,
  ReferenceData,
  FormSettingsData,
} from './Configure-models';

// APIs
import { ConfigureLiveApis } from './Configure-apis';

// Logic
import { loadSharedConfigureData } from './Configure-logic';
import { DebugObj } from '../../../../common/utils/errorHandlingUtils';

export class ConfigureLiveStore implements ConfigureStore {
  @observable
  public isLoadingData: boolean;

  @observable
  public referenceData: ReferenceData;

  @observable
  public automationRules: AutomationRule[];

  @observable
  public formSettingsData: FormSettingsData;

  @observable
  public isEditOn: boolean;

  public configureApis: ConfigureApis;

  // Component Stores
  @observable
  public manageAutomationRulesStore: ManageAutomationRulesStore;

  constructor(
    formBuilderAppStore: FormBuilderAppStore,
    apiBaseUrl: string,
    projectFormId: string,
    projectKey: string,
  ) {
    this.isEditOn = false;
    this.isLoadingData = true;
    this.automationRules = [];

    this.referenceData = {
      statuses: [],
      issueTypes: [],
      requestTypes: [],
    };

    this.formSettingsData = {
      name: '',
      portalRequestTypeIds: [],
      issueRequestTypeIds: [],
      newIssueIssueTypeIds: [],
      newIssueRequestTypeIds: [],
      portalLock: false,
    };

    // Spin up an instance of the configureApis collection for use by child components
    this.configureApis = new ConfigureLiveApis(apiBaseUrl, projectFormId, projectKey);

    // Create a placeholder ManageAutomationRulesStore while waiting for the loadSharedConfigureData fn to complete
    this.manageAutomationRulesStore = {} as ManageAutomationRulesStore;

    loadSharedConfigureData(
      this,
      this.configureApis,
      projectFormId,
      formBuilderAppStore.token,
    );
  }

  @action
  public updateIsEditOnCallback = (newStatus) => {
    this.isEditOn = newStatus;
  }

  @action
  public updateIsLoadingData = (newStatus) => {
    this.isLoadingData = newStatus;
  }

  @action
  public updateReferenceData = (newReferenceData) => {
    this.referenceData = newReferenceData;
  }

  @action
  public updateAutomationRules = (newAutomationRules) => {
    this.automationRules = newAutomationRules;
  }

  @action
  public updateFormSettingsData = (newFormSettingsData) => {
    this.formSettingsData = newFormSettingsData;
  }

  @action
  public updateManageAutomationRulesStore = (newStoreData: ManageAutomationRulesStore) => {
    this.manageAutomationRulesStore = newStoreData;
  }
}
