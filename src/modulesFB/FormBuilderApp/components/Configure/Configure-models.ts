// Models
import { ManageAutomationRulesStore } from '../../../../common/components/ManageAutomationRules/ManageAutomationRules-models';

// Form Settings
export interface FormSettingsData {
  name: string;
  portalRequestTypeIds: number[];
  issueRequestTypeIds: number[];
  newIssueIssueTypeIds: number[];
  newIssueRequestTypeIds: number[];
  portalLock: boolean;
}

// Reference Data
interface ReferenceDataStatusItem {
  id: string;
  name?: string;
}

interface ReferenceDataTypeItem {
  id: string;
  name: string;
  iconUrl?: string;
}

interface ReferenceDataIssueTypesItem extends ReferenceDataTypeItem {
  statuses: string[];
  subtask: boolean;
}

interface ReferenceDataRequestTypeItem extends ReferenceDataTypeItem {
  issueTypeId: string;
}

export interface ReferenceData {
  statuses: ReferenceDataStatusItem[];
  issueTypes: ReferenceDataIssueTypesItem[];
  requestTypes: ReferenceDataRequestTypeItem[];
}

// Automation Rules
interface AutomationRuleTrigger {
  type: string;
}

export interface AutomationRuleConditions {
  formId?: number;
  status?: string;
  status2?: string;
  requestTypeId?: string;
  issueTypeId?: string;
  otherFormsSubmitted?: boolean;
}

export enum ActionId {
  transition = 1,
  addform = 2,
  preventTransition = 3,
}

export interface AutomationRuleActionTransition {
  id: ActionId.transition;
  to: string;
}

export interface AutomationRuleActionAddForm {
  id: ActionId.addform;
  author: string;
  formId: number;
  doNotDuplicate: boolean;
  external?: boolean;
}

export interface AutomationRuleActionPreventTransition {
  id: ActionId.preventTransition;
  formId: number;
  isNotSubmitted: boolean;
  toStatus?: string;
  fromStatus?: string;
}

export type AutomationRuleAction = (
  AutomationRuleActionTransition |
  AutomationRuleActionAddForm |
  AutomationRuleActionPreventTransition
);

export interface AutomationRuleNoId {
  trigger: AutomationRuleTrigger;
  conditions: AutomationRuleConditions;
  action: AutomationRuleAction;
}

export interface AutomationRule extends AutomationRuleNoId {
  id: string;
}

// Stores and Helpers
export interface FetchApiSettings {
  method: string;
  path: string;
  body?: object;
  token?: string;
}

export interface ConfigureApis {
  getFormSettings: (token?: string) => Promise<FormSettingsData>;
  getReferenceData: (token?: string) => Promise<ReferenceData>;
  getAutomationRules: (token?: string) => Promise<AutomationRule[]>;

  postFormSettings: (formSettingsData: FormSettingsData, token?: string) => Promise<any>;
  postAutomationRule: (automation: AutomationRuleNoId, token?: string) => Promise<AutomationRule>;

  putAutomationRule: (automation: AutomationRule, token?: string) => Promise<any>;

  deleteAutomationRule: (deleteId: string, token?: string) => Promise<any>;
}

export interface ConfigureStore {
  isEditOn: boolean;
  isLoadingData: boolean;
  referenceData: ReferenceData;
  automationRules: AutomationRule[];
  formSettingsData: FormSettingsData;
  configureApis: ConfigureApis;

  // Component Stores
  manageAutomationRulesStore: ManageAutomationRulesStore;

  updateIsEditOnCallback: (newStatus: boolean) => void;
  updateIsLoadingData: (newStatus: boolean) => void;
  updateReferenceData: (newReferenceData: ReferenceData) => void;
  updateAutomationRules: (newAutomationRules: AutomationRule[]) => void;
  updateFormSettingsData: (newFormSettingsData: FormSettingsData) => void;
  updateManageAutomationRulesStore: (newStoreData: ManageAutomationRulesStore) => void;
}
