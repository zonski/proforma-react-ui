// Utils
import { sendDebugMsg } from '../../../../common/utils/errorHandlingUtils';

// Models
import { ConfigureStore, ConfigureApis } from './Configure-models';

// Async
import { fetchConfigureData } from './Configure-async';
import {
  ManageAutomationRulesLiveStore,
} from '../../../../common/components/ManageAutomationRules/ManageAutomationRules-stores';

export const loadSharedConfigureData = async (
  configureStore: ConfigureStore,
  configureApis: ConfigureApis,
  projectFormId: string,
  jwtToken?: string,
) => {
  try {
    const {
      formSettings,
      automationRules,
      referenceData,
    } = await fetchConfigureData(configureApis, jwtToken);

    const newFormSettingsData = {
      ...configureStore.formSettingsData,
      ...formSettings,
    };
    // Update configureStore with raw data
    configureStore.updateReferenceData(referenceData);
    configureStore.updateAutomationRules(automationRules);
    configureStore.updateFormSettingsData(newFormSettingsData);

    // Update child stores
    const newManageRulesStore = new ManageAutomationRulesLiveStore(
      projectFormId,
      newFormSettingsData.name,
      referenceData,
      automationRules,
    );
    configureStore.updateManageAutomationRulesStore(newManageRulesStore);

    // Disable the loading spinner so rules are shown
    configureStore.updateIsLoadingData(false);
  } catch (error) {
    sendDebugMsg({
      error,
      level: 'error',
      msg: 'Unable to fetch the automation rules',
    });
  }
};
