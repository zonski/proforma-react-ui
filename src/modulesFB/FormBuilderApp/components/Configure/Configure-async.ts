// Models
import { ConfigureApis } from './Configure-models';

export const fetchConfigureData = async (
  configureApis: ConfigureApis,
  jwtToken?: string,
) => {
  try {
    const referenceDataPromise = configureApis.getReferenceData(jwtToken);
    const formSettingsPromise = configureApis.getFormSettings(jwtToken);
    const automationRulesPromise = configureApis.getAutomationRules(jwtToken);

    const [referenceData, formSettings, automationRules] = await Promise.all([
      referenceDataPromise,
      formSettingsPromise,
      automationRulesPromise,
    ]);

    return { referenceData, formSettings, automationRules };
  } catch (error) {
    throw error;
  }
};
