import * as React from 'react';
import { inject, observer } from 'mobx-react';
import styled from 'styled-components';
// Error Handling
import ErrorBoundary from '../../../../common/components/ErrorBoundary/ErrorBoundary';
// Models
import { ConfigureStore } from './Configure-models';
// Components
import LoadingSpinner from '../../../../common/components/LoadingSpinner/LoadingSpinner';
import ManageAutomationRules from '../../../../common/components/ManageAutomationRules/ManageAutomationRules';
import FormSettings from '../../../../common/components/FormSettings/FormSettings';

const ConfigureItemsWrapper = styled.div`
  display: flex;
  flex-direction: column;
  align-items: center;
  width: 100%;
  margin-bottom: 22px;
  background-color: white;
`;

// Props
export interface ConfigureProps {
  configureStore: ConfigureStore;
  doNotTrack: boolean;
  segmentWriteKey?: string;
  jwtToken?: string;
}

@inject('configureStore')
@observer
export default class Configure extends React.Component<ConfigureProps, {}> {
  render() {
    const {
      configureStore,
      configureStore: {
        isLoadingData,
        formSettingsData,
        referenceData,
        configureApis,
        manageAutomationRulesStore,
      },
      doNotTrack,
      segmentWriteKey,
      jwtToken,
    } = this.props;

    return (
      <ErrorBoundary>
        <ConfigureItemsWrapper>
          {
            isLoadingData &&
            <LoadingSpinner message="Loading form configuration data..." />
          }
          {
            !isLoadingData &&
            <>
              {
                !configureStore.isEditOn &&
                <FormSettings
                  configureApis={configureApis}
                  configureStore={configureStore}
                  formSettingsInfo={formSettingsData}
                  refData={referenceData}
                  jwtToken={jwtToken}
                />
              }
              <ManageAutomationRules
                configureApis={configureApis}
                manageAutomationRulesStore={manageAutomationRulesStore}
                isEditOn={configureStore.isEditOn}
                updateIsEditOnCallback={configureStore.updateIsEditOnCallback}
                doNotTrack={doNotTrack}
                segmentWriteKey={segmentWriteKey}
                jwtToken={jwtToken}
              />
            </>
          }
        </ConfigureItemsWrapper>
      </ErrorBoundary>
    );
  }
}
