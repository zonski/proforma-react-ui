import styled, { injectGlobal } from 'styled-components';
import { colors } from '@atlaskit/theme';

injectGlobal`
  #proforma-react-ui-app {
    height: 100%;
  }
`;

export const FormBuilderAppWrapper = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: space-between;
  height: 100%;
  width: 100%;
  overflow-x: auto;
  overflow-y: auto;
`;

export const ActionsBar = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  width: 100%;
  padding: 0.4rem;
  box-sizing: border-box;
  border-top: solid 1px ${colors.N60};
  background-color: ${colors.N30};
  position: fixed;
  bottom: 0;
`;
