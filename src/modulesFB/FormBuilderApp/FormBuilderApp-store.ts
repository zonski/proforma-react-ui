import { observable } from 'mobx';

// Globals
import { licenseLevelLookup } from '../../common/utils/global-constants';
import { throwNewApiError } from '../../common/utils/errorHandlingUtils';

// Models
import {
  PermissionLevel,
  ServerSettings,
} from '../../common/apis/live';

import { FormBuilderAppStore } from './FormBuilderApp-models';
import { ConfigureStore } from './components/Configure/Configure-models';

// Page Stores
import { ConfigureLiveStore } from './components/Configure/Configure-store';

export class FormBuilderAppLiveStore implements FormBuilderAppStore {
  @observable
  public apiBaseUrl: string;

  @observable
  public buildUrl: string;

  @observable
  public closeUrl: string;

  @observable
  public licenseLevel: string;

  @observable
  public locale: string;

  @observable
  public permissionLevel: PermissionLevel;

  @observable
  public projectFormId: string;

  @observable
  public projectKey: string;

  @observable
  public tz: string;

  @observable
  public token?: string;

  public doNotTrack: boolean;
  public segmentWriteKey?: string;

  // Page Stores
  @observable
  configureStore: ConfigureStore;

  constructor(serverSettings: ServerSettings, doNotTrackStatus: boolean) {
    this.apiBaseUrl = serverSettings.apiBaseUrl || window.document.location.origin;
    this.buildUrl = serverSettings.buildUrl || '';
    this.closeUrl = serverSettings.closeUrl || '';
    this.licenseLevel = serverSettings.licenseLevel || licenseLevelLookup.unlicensed;
    this.locale = serverSettings.locale;
    this.permissionLevel = serverSettings.permissionLevel || {};
    this.projectFormId = serverSettings.projectFormId || '';
    this.projectKey = serverSettings.projectKey || '';
    this.doNotTrack = doNotTrackStatus;
    this.tz = serverSettings.tz;

    this.segmentWriteKey = serverSettings.segmentWriteKey;
    this.token = serverSettings.token;

    // Store init
    this.configureStore = new ConfigureLiveStore(
      this,
      this.apiBaseUrl,
      this.projectFormId,
      this.projectKey,
    );
  }

  updateToken = async () => {
    const route = '/api/1/token/refresh';
    const preFlight = {
      method: 'POST',
      headers: {
        ...(this.token && { Authorization: `Bearer ${this.token}` }),
        ...(this.tz && { 'X-Timezone': this.tz }),
        ...(this.locale && { 'X-Locale': this.locale }),
        'Content-Type': 'application/json',
      },
    };

    try {
      const response = await fetch(this.apiBaseUrl + route, preFlight);

      if (response.ok) {
        const jsonData = await response.json();

        // Update Token value in the App Store
        this.token = jsonData.token;

        return;
      }

      // Response was not ok
      throw response;
    } catch (error) {
      const contentType = error.headers ? error.headers.get('content-type') : '';
      const errorDetails =
        /^application\/json/.test(contentType.toLowerCase()) && (await error.json());
      return throwNewApiError(errorDetails, error);
    }
  }
}
