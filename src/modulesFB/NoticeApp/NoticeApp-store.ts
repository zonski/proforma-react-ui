import { observable } from 'mobx';

// Globals
import { licenseLevelLookup } from '../../common/utils/global-constants';
import { throwNewApiError } from '../../common/utils/errorHandlingUtils';

// Models
import {
  PermissionLevel,
  ServerSettings,
} from '../../common/apis/live';

import { NoticeAppStore } from './NoticeApp-models';
import { NoticeStore } from './components/Notice/Notice-models';

// Page Stores
import { NoticeLiveStore } from './components/Notice/Notice-store';

export class NoticeAppLiveStore implements NoticeAppStore {
  @observable
  public apiBaseUrl: string;

  @observable
  public licenseLevel: string;

  @observable
  public locale: string;

  @observable
  public permissionLevel: PermissionLevel;

  @observable
  public tz: string;

  @observable
  public token?: string;

  // Page Stores
  @observable
  noticeLiveStore: NoticeStore;

  constructor(serverSettings: ServerSettings) {
    this.apiBaseUrl = serverSettings.apiBaseUrl || window.document.location.origin;
    this.licenseLevel = serverSettings.licenseLevel || licenseLevelLookup.unlicensed;
    this.locale = serverSettings.locale;
    this.permissionLevel = serverSettings.permissionLevel || {};
    this.token = serverSettings.token;
    this.tz = serverSettings.tz;

    // Store init
    this.noticeLiveStore = new NoticeLiveStore();
  }

  updateToken = async () => {
    const route = '/api/1/token/refresh';
    const preFlight = {
      method: 'POST',
      headers: {
        ...(this.token && { Authorization: `Bearer ${this.token}` }),
        ...(this.tz && { 'X-Timezone': this.tz }),
        ...(this.locale && { 'X-Locale': this.locale }),
        'Content-Type': 'application/json',
      },
    };

    try {
      const response = await fetch(this.apiBaseUrl + route, preFlight);

      if (response.ok) {
        const jsonData = await response.json();

        // Update Token value in the App Store
        this.token = jsonData.token;

        return;
      }

      // Response was not ok
      throw response;
    } catch (error) {
      const contentType = error.headers ? error.headers.get('content-type') : '';
      const errorDetails =
        /^application\/json/.test(contentType.toLowerCase()) && (await error.json());
      return throwNewApiError(errorDetails, error);
    }
  }
}
