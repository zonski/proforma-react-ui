// NPM Modules
import * as React from 'react';
import { inject, observer } from 'mobx-react';

// Error Handling
import ErrorBoundary from '../../common/components/ErrorBoundary/ErrorBoundary';

// Models
import { NoticeAppStore } from './NoticeApp-models';

// Dedicated Components
import Notice from './components/Notice/Notice';

// Styles
import {
  NoticeAppWrapper,
} from './NoticeApp-styles';

export interface NoticeAppProps {
  noticeAppStore?: NoticeAppStore;
}

@inject('noticeAppStore')
@observer
export default class NoticeApp extends React.Component<NoticeAppProps, {}> {
  tokenTimer?: NodeJS.Timer;

  constructor(props) {
    super(props);

    if (!this.props.noticeAppStore) {
      return;
    }

    // Destructuring store props
    const {
      noticeAppStore,
      noticeAppStore: {
        token,
      },
    } = this.props;

    // Set a timer every 15 minutes to update the current token as this page is read-only
    if (token) {
      this.tokenTimer = setInterval(() => noticeAppStore.updateToken(), 900000);
    }
  }

  componentWillUnmount() {
    // Destroy the timer
    if (this.tokenTimer) {
      clearInterval(this.tokenTimer);
    }
  }

  render() {
    if (!this.props.noticeAppStore) {
      return;
    }

    const { noticeLiveStore } = this.props.noticeAppStore;

    return (
      <ErrorBoundary>
        <NoticeAppWrapper id="content" className="pf-builder__app">
          <Notice noticeLiveStore={noticeLiveStore} />
        </NoticeAppWrapper>
      </ErrorBoundary>
    );
  }
}
