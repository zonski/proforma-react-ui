// Models
import { NoticeStore } from './components/Notice/Notice-models';
import { PermissionLevel } from '../../common/apis/live';

// TODO extend smaller purpose specific interfaces to result in ths interface
export interface NoticeAppStore {
  apiBaseUrl: string;
  licenseLevel: string;
  locale: string;
  permissionLevel: PermissionLevel;
  tz: string;
  token?: string;

  // Page Stores
  noticeLiveStore: NoticeStore;

  // Methods
  updateToken: () => Promise<void>;
}
