// Simple Functional Components
import * as React from 'react';

// Atlaskit Components
import AtlaskitBtn from '@atlaskit/button';

// Atlaskit Icons
import CrossIcon from '@atlaskit/icon/glyph/cross';

// Error Boundary
import ErrorBoundary from '../../../../common/components/ErrorBoundary/ErrorBoundary';

// Models
import { AlertStatusTypes } from './Notice-models';

// Styles
import { DialogWrapper, DialogHeader, DialogBody, DialogFooter } from './Notice-styles';

// Logic
import * as logic from './Notice-logic';
import { SectionParagraph, AtlaskitBtnGroup, ParagraphLink } from '../../../shared-styles';

/**
 * Displays an Atlaskit Modal enabling the user to confirm or cancel a specific action
 */

export interface GeneralDialogHeadingProps {
  handleCrossOnClick: () => void;
  title: string;
}

export const GeneralDialogHeading = ({
  handleCrossOnClick,
  title,
}: GeneralDialogHeadingProps) => (
    <DialogHeader>
      <h2>{title}</h2>
      <AtlaskitBtn
        appearance="subtle"
        iconBefore={<CrossIcon label="Close" />}
        onClick={handleCrossOnClick}
      />
    </DialogHeader>
  );

export interface GeneralConfirmDialogProps {
  confirmationName: string;
  msg: string;
  noticeType: string;
  title: string;
}

export const GeneralConfirmDialog = ({
  confirmationName,
  msg,
  noticeType,
  title,
}: GeneralConfirmDialogProps) => {
  const callHandleConfirmationOnClick = () => {
    logic.handleConfirmationOnClick(noticeType);
  };

  const callHandleCancelOnClick = () => {
    logic.handleCancelOnClick(noticeType);
  };

  return (
    <ErrorBoundary>
      <DialogWrapper>
        <GeneralDialogHeading
          handleCrossOnClick={callHandleCancelOnClick}
          title={title}
        />

        <DialogBody>
          <p>{msg}</p>
        </DialogBody>

        <DialogFooter>
          <AtlaskitBtnGroup>
            <AtlaskitBtn appearance="primary" onClick={callHandleConfirmationOnClick}>
              {confirmationName}
            </AtlaskitBtn>
            <AtlaskitBtn onClick={callHandleCancelOnClick}>
              Cancel
            </AtlaskitBtn>
          </AtlaskitBtnGroup>
        </DialogFooter>
      </DialogWrapper>
    </ErrorBoundary>
  );
};

/**
 * Displays an Atlaskit Modal alerting the user about an event that has occurred
 */
export interface GeneralAlertDialogProps {
  alertStatusType: string;
  msg: string;
  noticeType: string;
  title: string;
}

export const GeneralAlertDialog = ({
  alertStatusType,
  msg,
  noticeType,
  title,
}: GeneralAlertDialogProps) => {
  const callHandleCloseOnClick = () => {
    logic.handleCloseOnClick(noticeType);
  };

  return (
    <ErrorBoundary>
      <DialogWrapper alertStatusType={alertStatusType}>
        <GeneralDialogHeading
          handleCrossOnClick={callHandleCloseOnClick}
          title={title}
        />

        <DialogBody>
          <p>{msg}</p>
          {
            alertStatusType === AlertStatusTypes.error &&
            <SectionParagraph>
              <span>Please refer to our</span>
              <ParagraphLink
                target="_blank"
                rel="noopener noreferrer"
                href="https://thinktilt.atlassian.net/wiki/x/GQFCEg"
              >
                Error Explanations
              </ParagraphLink>
              <span>page for more details or contact</span>
              <ParagraphLink
                isEnd={true}
                target="_blank"
                rel="noopener noreferrer"
                href="https://status.thinktilt.com/"
              >
                ThinkTilt Support
              </ParagraphLink>
              <span>.</span>
            </SectionParagraph>
          }
        </DialogBody>

        <DialogFooter>
          <AtlaskitBtnGroup>
            <AtlaskitBtn onClick={callHandleCloseOnClick}>
              Cancel
            </AtlaskitBtn>
          </AtlaskitBtnGroup>
        </DialogFooter>
      </DialogWrapper>
    </ErrorBoundary>
  );
};
