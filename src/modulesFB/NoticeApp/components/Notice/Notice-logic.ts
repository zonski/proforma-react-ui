// Utils
import { sendDebugMsg } from '../../../../common/utils/errorHandlingUtils';

import {
  CustomNoticeData,
  DialogClosePayload,
  DialogCreatePayload,
  proFormaAPUtils,
  ProFormaAPUtilsFnNames,
  ProFormaAPOnceEventOptions,
} from '../../../../common/utils/proFormaAPUtils';

// Models
import {
  AlertNoticeTypes,
  NoticeStore,
  NoticeBodyTypes,
  ConfirmNoticetypes,
} from './Notice-models';

/**
 * Determines what type of notification component needs to be displayed to a user based on the noticetype provided
 */
export const assignNewNoticeBody = (
  noticeLiveStore: NoticeStore,
  noticeType: string,
  msg?: string,
  title?: string,
  alertStatusType?: string,
  confirmationName?: string,
) => {
  const isConfirmNotice = !!ConfirmNoticetypes[noticeType];
  const isAlertNotice = !!AlertNoticeTypes[noticeType];

  if (isConfirmNotice) {
    noticeLiveStore.updateNoticeBodyType(NoticeBodyTypes.confirm);
    noticeLiveStore.updateNoticeType(noticeType);

    if (typeof title !== 'undefined') {
      noticeLiveStore.updateTitle(title);
    }

    if (typeof msg !== 'undefined') {
      noticeLiveStore.updateMsg(msg);
    }

    if (typeof confirmationName !== 'undefined') {
      noticeLiveStore.updateConfirmationName(confirmationName);
    }

    // Disable spinner to show updated notice
    return noticeLiveStore.updateIsLoading(false);
  }

  if (isAlertNotice) {
    noticeLiveStore.updateNoticeBodyType(NoticeBodyTypes.alert);
    noticeLiveStore.updateNoticeType(noticeType);

    if (typeof title !== 'undefined') {
      noticeLiveStore.updateTitle(title);
    }

    if (typeof msg !== 'undefined') {
      noticeLiveStore.updateMsg(msg);
    }

    if (typeof alertStatusType !== 'undefined') {
      noticeLiveStore.updateConfirmationName(alertStatusType);
    }

    // Disable spinner to show updated notice
    return noticeLiveStore.updateIsLoading(false);
  }

  // No match found so auto cancel the dialog
  sendDebugMsg({
    level: 'error',
    msg: 'The noticeType provided was either undefined or for an unhandled type.',
    error: new Error(`No match was found for the noticeType: ${noticeType}.`),
  });

  const payload: DialogClosePayload = {
    payloadType: 'close',
    noticeType,
    status: false,
  };

  proFormaAPUtils({
    fnName: ProFormaAPUtilsFnNames.close,
    payload,
  });

  return noticeLiveStore.updateIsLoading(false);
};

/**
 * Captures the Positive action in a confirmation modal and sends it back via the 'close' event with data
 * @param {string} noticeType - Identifies which notification the confirmation came from
 */
export const handleConfirmationOnClick = (noticeType) => {
  const payload: DialogClosePayload = {
    payloadType: 'close',
    noticeType,
    status: true,
  };

  proFormaAPUtils({
    fnName: ProFormaAPUtilsFnNames.close,
    payload,
  });
};

/**
 * Captures the Negative action in a confirmation modal and sends it back via the 'close' event with data
 * @param {string} noticeType - Identifies which notification was canceled
 */
export const handleCancelOnClick = (noticeType) => {
  const payload: DialogClosePayload = {
    payloadType: 'close',
    noticeType,
    status: false,
  };

  proFormaAPUtils({
    fnName: ProFormaAPUtilsFnNames.close,
    payload,
  });
};

/**
 * Captures the Close action within an alert modal and sends it back via the 'close' event with data
 * @param {string} noticeType - Identifies which notification was canceled
 */
export const handleCloseOnClick = (noticeType) => {
  const payload: DialogClosePayload = {
    payloadType: 'close',
    noticeType,
    status: false,
  };

  proFormaAPUtils({
    fnName: ProFormaAPUtilsFnNames.close,
    payload,
  });
};

// Externally used helpers

/**
 * Wraps the async task of displaying notices with proFormaAP in a Promise that always resolves
 * @param customData - Contains some of the following properties
 * @returns - Resolves with the confirmData.status (true for accept, false for deny type responses)
 */
export const displayNoticeToUser = (customData: CustomNoticeData) => {
  return new Promise((resolve) => {
    const payload: DialogCreatePayload = {
      payloadType: 'create',
      customData,
      key: 'noticeV2',
      chrome: false,
      size: 'large',
      closeOnEscape: false,
    };

    proFormaAPUtils({
      fnName: ProFormaAPUtilsFnNames.create,
      payload,
    });

    proFormaAPUtils({
      fnName: ProFormaAPUtilsFnNames.once,
      event: ProFormaAPOnceEventOptions.onClose,
      callback: (confirmData) => {
        if (confirmData.noticeType === customData.noticeType) {
          resolve(confirmData.status);
        }
      },
    });
  });
};
