import { DebugObj } from '../../../../common/utils/errorHandlingUtils';

export enum AlertNoticeTypes {
  submitIssueFormAlert = 'submitIssueFormAlert',
}

export enum ConfirmNoticetypes {
  automationRuleUnsavedChanges = 'automationRuleUnsavedChanges',
  automationRuleDelete = 'automationRuleDelete',
}

export enum AlertStatusTypes {
  error = 'error',
}

export enum NoticeBodyTypes {
  alert = 'alert',
  confirm = 'confirm',
  none = '',
}

// Stores and Helpers

export interface NoticeStore {
  isLoading: boolean;
  noticeBodyType: string;
  title: string;
  msg: string;
  noticeType: string;
  confirmationName: string;
  alertStatusType: string;

  updateIsLoading: (newStatus: boolean) => void;
  updateNoticeBodyType: (newNoticeBodyType: string) => void;
  updateTitle: (newTitle: string) => void;
  updateMsg: (newMsg: string) => void;
  updateNoticeType: (newNoticeType: string) => void;
  updateConfirmationName: (newConfirmationName: string) => void;
  updateAlertStatusType: (newAlertStatusType: string) => void;
}
