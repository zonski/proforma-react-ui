import styled from 'styled-components';

// Atlaskit
import { colors } from '@atlaskit/theme';
import { AlertStatusTypes } from './Notice-models';

export const NoticeWrapper = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: center;
  color: ${colors.N800};
  border-radius: 0.5rem;
`;

export interface DialogWrapperProps {
  alertStatusType?: string;
}

export const DialogWrapper = styled.div`
  display: flex;
  flex-direction: column;
  width: 60rem;
  padding: 2rem;
  background-color: ${colors.N0};
  border: ${(props: DialogWrapperProps) => (props.alertStatusType === AlertStatusTypes.error) && `0.1rem solid ${colors.R400}`};
`;

export const DialogHeader = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: space-between;
  align-items: center;
  height: 4rem;
`;

export const DialogBody = styled.div`
  min-height: 8rem;
  max-height: 30rem;
  padding-bottom: 1.2rem;
  font-size: 1.4rem;
  border-bottom: 0.1rem solid ${colors.N40};
  overflow-y: auto;
  overflow-x: hidden;
`;

export const DialogFooter = styled.div`
  display: flex;
  flex-direction: row;
  justify-content: flex-end;
  align-items: center;
  margin-top: 2rem;
`;
