import { observable, action } from 'mobx';

// Utils
import {
  CustomNoticeData, proFormaAPUtils, ProFormaAPUtilsFnNames,
} from '../../../../common/utils/proFormaAPUtils';

// Models
import { NoticeStore, NoticeBodyTypes } from './Notice-models';

// Logic
import { assignNewNoticeBody } from './Notice-logic';
import { DebugObj } from '../../../../common/utils/errorHandlingUtils';

export class NoticeLiveStore implements NoticeStore {
  @observable
  public isLoading: boolean;

  @observable
  public noticeBodyType: string;

  @observable
  public title: string;

  @observable
  public msg: string;

  @observable
  public noticeType: string;

  @observable
  public confirmationName: string;

  @observable
  public alertStatusType: string;

  constructor() {
    this.isLoading = true;
    this.noticeBodyType = NoticeBodyTypes.none;
    this.title = '';
    this.msg = '';
    this.noticeType = '';
    this.confirmationName = 'Confirm';
    this.alertStatusType = '';

    proFormaAPUtils({
      fnName: ProFormaAPUtilsFnNames.getCustomData,
      callback: (customData: CustomNoticeData) => {
        assignNewNoticeBody(
          this,
          customData.noticeType,
          customData.msg,
          customData.title,
          customData.alertStatusType,
          customData.confirmationName,
        );
      },
    });
  }

  @action
  public updateIsLoading = (newStatus) => {
    this.isLoading = newStatus;
  }

  @action
  public updateNoticeBodyType = (newNoticeBodyType: string) => {
    this.noticeBodyType = newNoticeBodyType;
  }

  @action
  public updateTitle = (newTitle: string) => {
    this.title = newTitle;
  }

  @action
  public updateMsg = (newMsg: string) => {
    this.msg = newMsg;
  }

  @action
  public updateNoticeType = (newNoticeType: string) => {
    this.noticeType = newNoticeType;
  }

  @action
  public updateConfirmationName = (newConfirmationName: string) => {
    this.confirmationName = newConfirmationName;
  }

  @action
  public updateAlertStatusType = (newAlertStatusType: string) => {
    this.alertStatusType = newAlertStatusType;
  }
}
