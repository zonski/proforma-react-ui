// NPM Modules
import * as React from 'react';
import { inject, observer } from 'mobx-react';

// Error Boundary
import ErrorBoundary from '../../../../common/components/ErrorBoundary/ErrorBoundary';

// Models
import { NoticeBodyTypes, NoticeStore } from './Notice-models';

// Styles
import { NoticeWrapper } from './Notice-styles';

// Functional Components
import {
  GeneralConfirmDialog,
  GeneralAlertDialog,
} from './Notice-sfcs';

// Common Components
import LoadingSpinner from '../../../../common/components/LoadingSpinner/LoadingSpinner';

export interface NoticeProps {
  noticeLiveStore: NoticeStore;
}

class Notice extends React.Component<NoticeProps> {
  render() {
    // Destructure store items to reduce boilerplate
    const {
      isLoading,
      noticeBodyType,
      title,
      msg,
      noticeType,
      confirmationName,
      alertStatusType,
    } = this.props.noticeLiveStore;

    return (
      <ErrorBoundary>
        <NoticeWrapper>
          {
            isLoading &&
            <LoadingSpinner message="Loading..." />
          }
          {
            noticeBodyType === NoticeBodyTypes.confirm &&
            <GeneralConfirmDialog
              title={title}
              msg={msg}
              confirmationName={confirmationName}
              noticeType={noticeType}
            />
          }
          {
            noticeBodyType === NoticeBodyTypes.alert &&
            <GeneralAlertDialog
              title={title}
              msg={msg}
              noticeType={noticeType}
              alertStatusType={alertStatusType}
            />
          }
        </NoticeWrapper>
      </ErrorBoundary>
    );
  }
}

export default inject('noticeLiveStore')(observer(Notice));
