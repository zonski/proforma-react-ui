describe('Given', () => {
  describe('When', () => {
    test('Then', () => {

    });
  });
});

// // Unit Tests for Functions used in the Notice component
// import {
//   noticeTypeLookup,
//   checkIsAlertNotice,
//   checkIsConfirmNotice,
// } from '../Notice';

// /*
//     noticeTypeLookup - Object Property Confirmation
// */
// describe('Given the noticeTypeLookup is defined', () => {
//   describe('When keys are inspected', () => {
//     test('Then all noticeType keys will end in a known suffix', () => {
//       const allTypesHaveKnownSuffix = !Object.keys(noticeTypeLookup).some(noticeType => (
//         noticeType.toLowerCase().slice(-7) !== 'confirm'
//         && noticeType.toLowerCase().slice(-5) !== 'alert'
//       ));

//       expect(allTypesHaveKnownSuffix).toEqual(true);
//     });
//   });
// });

// /*
//     checkIsAlertNotice(noticeType: string)
// */
// describe('Given checkIsAlertNotice is called', () => {
//   describe('When the provided noticeType value does not match a known alert noticeType', () => {
//     test('Then it will return false', () => {
//       const result = checkIsAlertNotice('madeThisUpAlert');
//       expect(result).toEqual(false);
//     });
//   });

//   describe('When the provided noticeType is defined in the noticeTypeLookup object using the alert suffix', () => {
//     test('Then it will return true as it is being handled', () => {
//       const unhandledAlertNoticeType = Object.keys(noticeTypeLookup).some(noticeType => (
//         noticeType.toLowerCase().slice(-5) === 'alert'
//         && !checkIsAlertNotice(noticeTypeLookup[noticeType])
//       ));

//       expect(unhandledAlertNoticeType).toEqual(false);
//     });
//   });
// });

// /*
//     checkIsConfirmNotice(noticeType: string)
// */
// describe('Given checkIsConfirmNotice is called', () => {
//   describe('When the provided noticeType value does not match a known confirm noticeType', () => {
//     test('Then it will return false', () => {
//       const result = checkIsConfirmNotice('madeThisUpConfirm');
//       expect(result).toEqual(false);
//     });
//   });

//   describe('When the provided noticeType is defined in the noticeTypeLookup object using the confirm suffix', () => {
//     test('Then it will return true as it is being handled', () => {
//       const unhandledConfirmNoticeType = Object.keys(noticeTypeLookup).some(noticeType => (
//         noticeType.toLowerCase().slice(-7) === 'confirm'
//         && !checkIsConfirmNotice(noticeTypeLookup[noticeType])
//       ));

//       expect(unhandledConfirmNoticeType).toEqual(false);
//     });
//   });
// });
