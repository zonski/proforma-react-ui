describe('Given', () => {
  describe('When', () => {
    test('Then', () => {

    });
  });
});

// // Integration Tests for React Class Component
// import React from 'react';
// import { Provider } from 'mobx-react';
// import { observable } from 'mobx';

// // Mocks
// import { noticeTypeLookup, statusClassNameLookup } from '../Notice-logic';
// import { GeneralConfirmDialog, GeneralAlertDialog } from '../Notice';
// import * as globalHelpers from '../../../../utils/global-helpers';


// // Component to be tested
// import Notice from '../Notice';

// /*
//     Notice
// */
// describe('Given Notice is mounted', () => {
//   const debugMsgBackup = globalHelpers.debugMsg;

//   beforeEach(() => {
//     // Mocks
//     globalHelpers.debugMsg = jest.fn();

//     global.proFormaAP = {
//       dialog: {
//         close: jest.fn(),
//       },
//     };

//     // Common Scenario Configuration
//     global.scenario = {
//       store: {},
//     };
//   });

//   afterEach(() => {
//     globalHelpers.debugMsg = debugMsgBackup;
//   });

//   describe('When customData.noticeType is undefined', () => {
//     test('Then the customData.msg will NOT be displayed', () => {
//       // Unique Scenario Config
//       const mockCustomData = {
//         title: 'mock',
//         msg: 'not shown',
//         confirmationName: 'mock',
//         noticeType: undefined,
//       };

//       global.proFormaAP.dialog.getCustomData = jest.fn((
//         callback,
//         customData = mockCustomData,
//       ) => callback(customData));

//       // Attempt Mounting
//       const notice = mount((
//         <Provider store={observable(global.scenario.store)}>
//           <Notice />
//         </Provider>
//       ));

//       const hiddenCustomDataMsg = notice.findWhere(x => (
//         x.exists()
//         && (x.type() === GeneralConfirmDialog || x.type() === GeneralAlertDialog)
//         && x.contains(mockCustomData.msg)
//       ));

//       expect(hiddenCustomDataMsg).toHaveLength(0);
//     });
//   });

//   describe('When customData.noticeType is matched to an alert dialog', () => {
//     test('Then the customData.msg will be displayed using the GeneralAlertDialog SFC', () => {
//       // Unique Scenario Config
//       const mockCustomData = {
//         title: 'mock',
//         msg: 'submit issue form alert message',
//         statusClassName: statusClassNameLookup.error,
//         noticeType: noticeTypeLookup.submitIssueFormAlert,
//       };

//       global.proFormaAP.dialog.getCustomData = jest.fn((
//         callback,
//         customData = mockCustomData,
//       ) => callback(customData));

//       // Attempt Mounting
//       const notice = mount((
//         <Provider store={observable(global.scenario.store)}>
//           <Notice />
//         </Provider>
//       ));

//       const matchedNoticeType = notice.findWhere(x => (
//         x.exists()
//         && x.type() === GeneralAlertDialog
//         && x.contains(mockCustomData.msg)
//       ));

//       expect(matchedNoticeType).toHaveLength(1);
//     });
//   });
// });
