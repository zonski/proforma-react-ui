import React from 'react';
import ReactDOM from 'react-dom';
import { Provider } from 'mobx-react';

// Models
import { ServerSettings } from '../../common/apis/live';
import { NoticeAppStore } from './NoticeApp-models';

// Stores
import { NoticeAppLiveStore } from './NoticeApp-store';

// Module
import NoticeApp from './NoticeApp';
import { setupGlobalErrorHandling } from '../../common/utils/errorHandlingUtils';

export const createNoticeModule = (noticeAppStore) => {
  return (
    <Provider noticeAppStore={noticeAppStore}>
      <NoticeApp />
    </Provider>
  );
};

export const loadNoticeModule = (
  elementId: string,
  serverSettings: ServerSettings,
  moduleStore?: NoticeAppStore,
) => {
  // Setup any global Utils with dependencies on static serverSettings
  setupGlobalErrorHandling(
    serverSettings.sen,
    serverSettings.debugLvl,
    serverSettings.analyticsEnabled,
  );

  const noticeAppStore = moduleStore || new NoticeAppLiveStore(serverSettings);
  const targetNode = document.getElementById(elementId);

  ReactDOM.render(
    createNoticeModule(noticeAppStore),
    targetNode,
  );

  return () => {
    if (targetNode !== null) {
      ReactDOM.unmountComponentAtNode(targetNode)
    }
  };
};
