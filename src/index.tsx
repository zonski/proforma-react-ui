import 'react-app-polyfill/ie11';
import 'core-js/modules/es7.array.includes';
import 'core-js/modules/es6.array.find';
import 'core-js/modules/es6.string.ends-with';
import 'core-js/modules/es6.string.starts-with';
import 'core-js/modules/es6.object.keys';
import * as serviceWorker from './serviceWorker';

import reactDOM from 'react-dom';

export * from './modules/TestHarness/index';

export * from './modules/AdminConfig/index';
export * from './modules/AdminForms/index';
export * from './modules/ProjectForms/index';

// Automation TODO: merge into ./modules/
export * from './modulesFB/FormBuilderApp';
export * from './modulesFB/NoticeApp';

export const unloadApp = (appElements) => {
  Array.prototype.forEach.call(appElements, (currentDiv) => {
    reactDOM.unmountComponentAtNode(currentDiv);
  });
};

// If you want your app to work offline and load faster, you can change
// unregister() to register() below. Note this comes with some pitfalls.
// Learn more about service workers: https://bit.ly/CRA-PWA
serviceWorker.unregister();
