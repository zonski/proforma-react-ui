/* config-overrides.js */

// Used to enable support for importing css files into other css files
const postcssImport = require('postcss-import');

// Provides support for things like css variables, complex nesting, etc.
const precss = require('precss');

const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const reactAppRewireBuildDev = require('react-app-rewire-build-dev');
const FileManagerPlugin = require('filemanager-webpack-plugin');
const webpack = require('webpack');

module.exports = function override(config, env) {

  config.output.library = 'proFormaReactUi';

  config.optimization.runtimeChunk = false;

  config.optimization.splitChunks = {
    cacheGroups: {
      vendor: {
        test: /[\\/]node_modules[\\/]/,
        name: 'vendor',
        chunks: 'all',
      },
    },
  };

  config.output.path = __dirname + '/build';
  console.log('Build output path: ' + config.output.path);

  let jsDir = 'static/js';
  config.output.filename = jsDir + '/[name].js';
  config.output.chunkFilename = jsDir + '/[name].js';

  let cssDir = 'static/css';
  config.plugins.forEach(p => {
    if (p instanceof MiniCssExtractPlugin) {
      p.options.filename = cssDir + '/[name].css';
      p.options.chunkFilename = cssDir + '/[name].css';
    }
  });

  // Handle PostCss
  require('react-app-rewire-postcss')(config, {
    // Postcss plugins run in the order listed (postcssImport first)
    plugins: loader => [
      postcssImport({
        root: loader.resourcePath,
      }),
      precss(),
    ],
  });

  config.plugins.push(
      new webpack.DefinePlugin({
          // Give our styled-components a unique name to avoid conflicts
          'process.env.SC_ATTR': '"proforma-styled-components"',
      }),
  );

  let deployToAddOn = process.env.DEPLOY_TO_PROFROMA_ADDON;
  deployToAddOn = deployToAddOn === true || deployToAddOn === 'true';
  if (deployToAddOn) {
    let targetDir = process.env.PROFROMA_ADDON_DIR + '/app/assets/js/proforma-react-ui';
    let publicDir = process.env.PROFROMA_ADDON_DIR + '/public';
    console.log('Copying files to ProForma AddOn at: ' + targetDir);
    config.plugins.push(new FileManagerPlugin({
      onEnd: [
        {
          copy: [
            {
              source: config.output.path + "/static/js/*.js",
              destination: targetDir
            },
            {
              source: config.output.path + "/static/css/*.css",
              destination: targetDir
            },
            {
              source: config.output.path + "/images",
              destination: publicDir + "/images"
            },
            {
              source: "public/atlaskit-css-reset.css",
              destination: targetDir + '/atlaskit-css-reset.css'
            },
            {
              source: __dirname + "/node_modules/@atlaskit/reduced-ui-pack/dist/bundle.css",
              destination: targetDir + '/atlaskit-css-reduced-ui-pack.css'
            }
          ]
        }
      ]
    }));
  }

  let deployToPlugin = process.env.DEPLOY_TO_PROFROMA_PLUGIN;
  deployToPlugin = deployToPlugin === true || deployToPlugin === 'true';
  if (deployToPlugin) {
    let targetDir = process.env.PROFROMA_PLUGIN_DIR + '/common/src/main/resources/js/proforma-react-ui';
    let publicDir = process.env.PROFROMA_PLUGIN_DIR + '/common/src/main/resources/';
    console.log('Copying files to ProForma Plugin at: ' + targetDir);
    config.plugins.push(new FileManagerPlugin({
      onEnd: [
        {
          copy: [
            {
              source: config.output.path + "/static/js/*.js",
              destination: targetDir
            },
            {
              source: config.output.path + "/static/css/*.css",
              destination: targetDir
            },
            {
              source: config.output.path + "/images",
              destination: publicDir + "/images"
            },
            {
              source: __dirname + "/node_modules/@atlaskit/css-reset/dist/bundle.css",
              destination: publicDir + '/css/atlaskit-css-reset.css'
            }
          ]
        }
      ]
    }));
  }

  if (deployToAddOn && env === 'development') {
    // this forces writing the dev build to the file system so it can be copied to the Add On project
    const options = {
      outputPath: './build'
    };
    return reactAppRewireBuildDev(config, env, options);
  } else {
    return config;
  }
};
